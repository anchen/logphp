<?php
ini_set('magic_quotes_gpc'    , 0);
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase' , 0);
ini_set("session.gc_maxlifetime", 1800); // 30mins
ini_set('memory_limit', '1024M');

//tidyUpSessions(); // has to be prior to session_start
session_start();

/*
// ----------------------------------------------------------------------------------------------------
// - Display Errors
// ----------------------------------------------------------------------------------------------------
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

// ----------------------------------------------------------------------------------------------------
// - Error Reporting
// ----------------------------------------------------------------------------------------------------
error_reporting(-1);

// ----------------------------------------------------------------------------------------------------
// - Shutdown Handler
// ----------------------------------------------------------------------------------------------------
function ShutdownHandler()
{
    if(@is_array($error = @error_get_last()))
    {
        return(@call_user_func_array('ErrorHandler', $error));
    };

    return(TRUE);
};

register_shutdown_function('ShutdownHandler');

// ----------------------------------------------------------------------------------------------------
// - Error Handler
// ----------------------------------------------------------------------------------------------------
function ErrorHandler($type, $message, $file, $line)
{
    $_ERRORS = Array(
        0x0001 => 'E_ERROR',
        0x0002 => 'E_WARNING',
        0x0004 => 'E_PARSE',
        0x0008 => 'E_NOTICE',
        0x0010 => 'E_CORE_ERROR',
        0x0020 => 'E_CORE_WARNING',
        0x0040 => 'E_COMPILE_ERROR',
        0x0080 => 'E_COMPILE_WARNING',
        0x0100 => 'E_USER_ERROR',
        0x0200 => 'E_USER_WARNING',
        0x0400 => 'E_USER_NOTICE',
        0x0800 => 'E_STRICT',
        0x1000 => 'E_RECOVERABLE_ERROR',
        0x2000 => 'E_DEPRECATED',
        0x4000 => 'E_USER_DEPRECATED'
    );

    if(!@is_string($name = @array_search($type, @array_flip($_ERRORS))))
    {
        $name = 'E_UNKNOWN';
    };

    return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
};

$old_error_handler = set_error_handler("ErrorHandler");
*/


require_once "library/config.php";
require_once "library/database.php";
require_once "library/functions.php";
require_once "library/html.php";
require_once "library/master.php";
require_once "library/language.php";
require_once "library/verbosity.php";

	
// tidyUpSessions
// --------------------------------------------
function tidyUpSessions(){
	/*Goes through the list of sessions that have been active
	*in the past and removes those that have been updated last
	*time over 2 hours ago (probably not logged out correctly)*/
	$now  = time();
	$path = session_save_path();
	foreach(glob($path."/*") as $session){
		if(substr($session, 0, 5)!="sess_") continue;
		if($now-filemtime($path."/".$session) < 2*3600) continue;
		// old sessions are removed
		$id = substr($session, 5);
		rm($path."/".$session);
		rm("sessions/".$id   );
	}
}





// Portal
// ============================================
class Portal extends Master {
	/* A lightweight version of the Master class but 
	* only to manage the logging in and the logging out
	* of the users.*/


	// __construct
	// ----------------------------------------
	public function __construct($configs){
		/* Constructor */
		parent::__construct("index", $configs);
		$this->session["sessId"] = session_id();
		$this->run();
	}

	// run
	// ----------------------------------------
	private function run() {
		/* Main function that is called, implements the
		* login, logout, or do nothing logic */
		if($_SERVER['REQUEST_METHOD']=="POST"){
			//if(isset($this->session["userId"]) && !empty($this->session["userId"]) &&
			//   isset($this->session["theme" ]) && !empty($this->session["theme" ])){
			//	$msg = $this->html->template("vb_redirect");
			//	$this->vb->success($msg);
			//}
			//else {
				$this->runLogin();
			//}
		}
		else {
			if(array_key_exists("do", $this->globals) && $this->globals["do"]=="logout")
				$this->runLogout();
			else {
				$this->html->set("content", $this->html->template("login"), "main");
			}
		}
		print $this->html->template("main", array(), NULL, "index", "main");
	}

	// runLogin
	// ----------------------------------------
	private function runLogin(){
		/* Logging the user into the system */

		if($_SERVER['REQUEST_METHOD']!="POST") return;
		if($this->post["do"]=="none") $this->post["do"] = "log";

		$pass   = md5($this->post["password"]);
		$user   = $this->post["user"];
		$table  = $this->db->readTable("websiteusers", array("*"), array("username"=>$user));

		if($table->count()==0) $this->vb->error(sprintf("User %s does not exist!"                   , $user));
		if($table->count()!=1) $this->vb->error(sprintf("Username %s cannot be uniquely identified!", $user));

		if($table->userpasswd!=$pass) {
			$this->vb->error("Incorrect password!");
			$table->faultyattempts++;
			$table->push();
			return;
		}

		if($table->faultyattempts>4) 
			$this->vb->error("You have tried to login too many times and failed. User is blocked! Please email me to unlock: <b>kim.temming@physik.uni-freiburg.de</b>.");

		if($this->vb->state==3) return;

		// redirection to the sTGC page
		if($this->post["do"]=="stgc"){
			$this->session["username"] = $user;
			registerInSession($this->session);
			header('Location: https://muon-nsw.web.cern.ch/muon-nsw/sTGC.php');
			return;
		}

		if(file_exists("sessions/".$this->session["sessId"])) 
			rmdir("sessions/".$this->session["sessId"]);

		mkdir("sessions/guest/"                   , 0777, true);
		mkdir("sessions/".$this->session["sessId"], 0777, true);

		$this->session["userId"      ] = $table->{$table->pk->key};
		$this->session["username"    ] = $user;
		$this->session["isAdmin"     ] = false;
		$this->session["canEdit"     ] = false;
		$this->session["canMultiEdit"] = false;
		$this->session["setup"       ] = "production";
		$this->session["theme"       ] = $this->post["do"];
		$this->session["fontsize"    ] = "100";

		$canedit = ($this->post["do"]=="elec") ? "isallowedtoelec" : "isallowedtoeditflag";

		if($table->isadminflag         =="T") $this->session["isAdmin"     ] = true;
		if($table->$canedit            =="T") $this->session["canEdit"     ] = true;
		if($table->isallowedtomultiflag=="T") $this->session["canMultiEdit"] = true;

		registerInSession($this->session);

		$this->html->setVars($this->session, "all");
		$msg = $this->html->template("vb_redirect");
		$this->vb->success($msg);
	}

	// runLogout
	// ----------------------------------------
	private function runLogout(){
		/* Logging the user out of the system */
		//rmdir("sessions/".$this->session["sessId"]);
		session_destroy();
		$this->vb->success($this->html->template("vb_logout"));
	}
}


$portal = new Portal(array("configs/setup_all", readlink("configs/setup_default")));


?>
