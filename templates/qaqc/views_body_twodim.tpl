<div class="bbox simple">               
	<div class="border padd6x12 wrap">
		<div class="fleft wthird right topp6">
			Binning X:
		</div>
		<div class="fleft wtwothirds">
			<input type='number' class="invisiblelike half" id='scalingX' name="scalingX" value='20'>
		</div>
		<div class="clear"></div>
		<div class="fleft wthird right topp6">
			Binning Y:
		</div>
		<div class="fleft wtwothirds">
			<input type='number' class="invisiblelike half" id='scalingY' name="scalingY" value='20'>
			<button class="buttonlike flex" type='button' onclick='Draw_TH2()'>Re-Draw</button>
		</div>
	</div>
	<div class="clear"></div>
	<div class="border padd6x12 topm6 center">
		<button class="buttonlike" type='button' onclick='GetJSON2()' >Download JSON</button> 
		<a href='downloads/parser2D.py' target='_blank'><button class="buttonlike" type='button'>Download Script</button></a> 
	</div>
	<div class="border padd6x12 topm6" id="zone1">
		Here will be the drawing of TH2
	</div>
</div>
<!--
<a href='scripts/parser2D.py' class='button'  download><i class='fa fa-downloa'></i>Download script</a>
-->
<script type='text/javascript' src='https://root.cern/js/dev/scripts/JSRootCore.js'></script>
<script>
// Doc:  https://root.cern.ch/js/5.5.1/docu/JSROOT.html#jsroot-api

//--- Creation et dessin  Histogramme TH1 --------------------
var h2;
var json2D;

function Draw_TH2() {

	var binsX = document.getElementById('scalingX').value;
	var binsY = document.getElementById('scalingY').value;
	console.log(binsX);
	
	var valuesX  = $jsonx;
	var valuesY  = $jsony;
	console.log(valuesY);

	var minX = $jsonx_min;
	var minY = $jsony_min;
	
	var maxX = $jsonx_max;
	var maxY = $jsony_max;
	
	var singleX = (maxX-minX)/binsX;
	var singleY = (maxY-minY)/binsY;
	
	var bin_values = [];
            
	for(var i = 0; i < binsX; i++) {
		var temp = []
		for(var j = 0; j < binsY; j++) {
			temp.push(0)
		}
		bin_values.push(temp);
	}
           
	loop1:
	for (var it = 0; it<valuesX.length; it++ ){
		loop2:
		for (var act_binX = 1; act_binX<=binsX; act_binX++){
			if (valuesX[it]<minX+act_binX*singleX){
				loop3:        
				for (var itY = 0; itY<valuesY.length; itY++ ){
					loop4:        
					for (var act_binY = 1; act_binY<=binsY; act_binY++){
						if (valuesY[itY]<minY+act_binY*singleY){
							bin_values[act_binX-1][act_binY-1] = bin_values[act_binX-1][act_binY-1]+1;
							break loop2;
						}
					}
				}
			}
		}
	}
	console.log(bin_values);
                                                                               
	h2 = JSROOT.CreateHistogram('TH2F', parseInt(binsX), parseInt(binsY));
	h2.fTitle = '$name';
	
	h2.fXaxis.fXmin = $jsonx_min;
	h2.fXaxis.fXmax = $jsonx_max;
	h2.fYaxis.fXmin = $jsony_min;
	h2.fYaxis.fXmax = $jsony_max;

	h2.fXaxis.fTitle = '$ctxx';
	h2.fYaxis.fTitle = '$ctxy';

	for (var index = 0; index<valuesX.length; index++){
		h2.Fill(valuesX[index], valuesY[index]);
	}
                                                                   
	var json = JSROOT.toJSON(h2);
	json2D = json;
	console.log(json);

	document.getElementById('zone1').style.width = '1000px';
	document.getElementById('zone1').style.height = '700px';

	JSROOT.redraw('zone1', h2, "colz", function(painter){ var json = JSROOT.toJSON(painter.GetObject()); console.log(json);});

}; 

function GetJSON2() {
	var hiddenElement = document.createElement('a');
	hiddenElement.href = 'data:attachment/text,' + encodeURI(json2D);
	hiddenElement.target = '_blank';
	hiddenElement.download = 'myFile.json';
	hiddenElement.click();
};

Draw_TH2();

</script>
