<div id="main">
	<p>Please login to see the pages:</p>
	<table id="inputFields">
		<tr>
			<td class="label"><label>User:</label></td>
			<td class="input"><input type="text" name="user" pattern='$patternText'></td>
		</tr>
		<tr>
			<td class="label"><label>Password:</label></td>
			<td class="input"><input type="password" name="password"></td>
		</tr>
	</table>
	<table id="submitButton">
		<tr>
			<td colspan="2">
				<button class="logistics" type="button" onclick="submitIt('log')" >go to Logistics DB WebView</button>
			</td>
		</tr>
		<tr>
			<td>
				<button class="central"   type="button" onclick="submitIt('qaqc')">go to CERN Central DB<br />WebView</button>
			</td>
			<td>
				<button class="stgc"      type="button" onclick="submitIt('stgc')">go to sTGC DB<br />WebView</button>
			</td>
		</tr>
	</table>
</div>
<div id="useFf">
	Please use this website with firefox (recent version) only.
</div>
