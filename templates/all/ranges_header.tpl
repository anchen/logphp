<script type="text/javascript" src="jsNew/all/ranges.js"></script>      
<link rel="stylesheet" href="stylesNew/all/ranges.css">
<input type="hidden" name="startRangePlhd1" value="$startRangePlhd1" />
<input type="hidden" name="startRangePlhd2" value="$startRangePlhd2" />
<input type="hidden" name="endRangePlhd1"   value="$endRangePlhd1"   />
<input type="hidden" name="endRangePlhd2"   value="$endRangePlhd2"   />
<input type="hidden" name="items"           value="$myItems"         />
<input type="hidden" name="rangeCount"      value="$rangeCountTotal" />
<input type="hidden" name="faulty"          value="$faulty"          />
$defHead
$customHead
<div class='bbox simple'> 
	<div class='border padd6x12' id="rangesTable"> 
		Selected equipments:
		<table class='greytable'>
			<colgroup>
				$colgroup
			</colgroup>
			<thead>
				<tr>
					$colhead
				</tr>
			</thead>
			<tbody>
				$rowsRangeTable
				<tr class='rangesTotalCount'>
					$infCols<td></td><td></td><td></td><td class="rangesTotalCountCell">$rangeCountTotal</td><td></td><td></td>$addCols
				</tr>
				<tr class='rangesLegend'>
					<td class='rangesLegendArrow'></td>
					<td colspan='$colspan'>
						With selected:
						<select class='buttonlike flex' name='withselected'>
							$optionRmRange
							$optionBreakLink
						</select>
						<button type="button" class='buttonlike flex' onclick="submitIt('go')">Go!</button>
						or 
						<button type="button" class='buttonlike flex' onclick="toggleAllBtn('subRanges[]', 'source')">Select / Unselect All Elements in List</button> 
						<input type="checkbox" id="source" hidden>
						or 
						<button type="button" class='buttonlike flex' onclick="submitIt('clr')">Clear the Table</button>
					</td>
				</tr>
			</tbody>   
		</table>
		$faultyElements
	</div>
</div>
