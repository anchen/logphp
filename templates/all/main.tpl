<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE">
<meta charset="utf-8">
<title>$title</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="stylesNew/menu.css">
<link rel="stylesheet" href="stylesNew/vb.css">
<link rel="stylesheet" href="stylesNew/all.css">
<link rel="stylesheet" href="stylesNew/$theme.css">
<script>
if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>	 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="jsNew/all.js"></script>
<script type="text/javascript" src="jsNew/all_tableExport.js"></script>
<script type="text/javascript" src="jsNew/all_jquery.base64.js"></script>  
<script type="text/javascript" src="jsNew/all_jquery.floatThead.js"></script>
<script type="text/javascript" src="jsNew/all_custom.floatThead.js"></script>
<script type="text/javascript" src="jsNew/all_custom.ordertable.js"></script>
<script type="text/javascript" src="jsNew/all_jquery.tablesorter.js"></script>
<script>
function calibrate(){
	document.getElementById('headerNavi'    ).style.height = document.getElementById('headerNaviLeftUl').style.height;
}
</script>
</head>
<body id='body' class="$setup" style="font-size:$fontsize%;" onload="calibrate();$onload">
<form name="mainform" method="POST" action="page.php" enctype="multipart/form-data">
<input type="hidden" name="sessId" value="$sessId">
<input type="hidden" name="pageId" value="$pageId">
<input type="hidden" name="do"     value=""       >
<input type="hidden" name="get"    value=""       >
<input type="hidden" name="p"      value="$p"     >
<input type="hidden" name="docId"  value="0"      >
<input type="hidden" name="theme"  value=""       >
<input type="hidden" name="dummy"  value=""       id="dummy">
<div id="header">
	<div id="headerTop">
		<div id="headerTopLeft">You are logged in as $username.
			&nbsp;|&nbsp;&nbsp;Select font-size:
			<input name="fontsize" type="radio" value="100" onclick="toggleFontSize()" $checked_fontsize_100> normal
			<input name="fontsize" type="radio" value="80"  onclick="toggleFontSize()" $checked_fontsize_80 > smaller
		</div>
		<div id="headerTopCenter">
			<h1>$title</h1>
		</div> 
		<div id="headerTopRight"> 
			<a href="#" onclick="changeTheme('$imgtheme')">
				<img src="$imgsrc" alt="$imgtitle" id="headerImage">
			</a>
		</div>
		<div class="clear"></div>
	</div>	
	<div id="headerNavi">
		<div id="headerNaviLeft">
			$navigationLeft
		</div>
		<div id="headerNaviRight">
			$navigationRight
		</div>
	</div>
</div>
<div class="clear"></div>
$debug
$messages
$content
</form>
</body>
</html>
