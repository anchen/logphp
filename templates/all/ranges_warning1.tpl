<div class='warning'>
	<span class='warningteaser'>
		You have selected items and/or batches together that have different actual locations (MORE...)
	</span>
	<span class='warningtext'>
		You have selected items and/or batches together that have different actual locations
		saved in the database. You cannot perform a common shipping operation for them, since
		they need a common starting point for their shipping. Either you need to split them
		in two shipping operations (depending on their actual location), or (if there is a
		mistake and these items actually do have the same location) correct the wrong actual
		location first for the concerned part of the selection.
	</span>
</div>
