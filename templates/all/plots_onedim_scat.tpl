<script type='text/javascript'>
google.load('visualization', '1', {packages:['corechart']});
google.setOnLoadCallback(drawScatterChart);

// drawScatterChart
function drawScatterChart() {

	// the data
	var data = new google.visualization.DataTable($json);

	// chart parameters
	var options = {
		title:  '$plotTitle ($plotDate)',
		explorer: { 
			actions: ['dragToZoom', 'rightClickToReset'],
			axis: 'horizontal',
			keepInBounds: true,
			maxZoomIn: 4.0
		},
 		backgroundColor: '#f8f8f8',
		colors: ['#D44E41']
	};

	// prepare chart				                 
	var chart = new google.visualization.ScatterChart(document.getElementById('$canvasId'));

	// add link for png
	google.visualization.events.addListener(chart, 'ready', function () {
		document.getElementById('$png').innerHTML = '<a id="plotLinkPng" class="plotLink" target="_blank" href="' +chart.getImageURI()+'">png</a>';
	});

	// draw the plot
	chart.draw(data, options);
}
</script>
