<label class='invisdesc'>DOCUMENTS:</label>
<table class="greytable smaller">
	<colgroup>
		<col width="4%">
		<col width="55%">
		<col width="12%">
		<col width="20%">
		<col width="9%">
	</colgroup>
	<thead>
		<tr>
			<th><center><button type="button" onclick="submitIt('deldoc')" class="buttonlike flex del" $saveActive>&#8999;</button></center></th>
			<th><b>DOCUMENT</b></th>
			<th><b>TYPE OF DOCUMENT</b></th>
			<th><b>DATE</b></th>
			<th><b>AUTHOR</b></th>
		</tr>
	</thead>
	<tbody>
		$rows
		<tr>
			<td></td>
			<td colspan="4">
				<select id="doctypbutton" class="buttonlike" name="docType" onchange="if(askEventDate()){document.getElementById('uplnewdoc').click()};" $saveActive>   
					<option style="display:none;" value="">Upload new Doc (max. 2MB)</option>
					$options_new
 				</select>
				<input type="file" id="uplnewdoc" name="uplnewdoc" accept="$allowedUplTypes" onchange="if(checkFileSize('uplnewdoc')){submitIt('newdoc');}" $saveActive>
				or 
				<select id="docaddbutton" class="buttonlike" name="docAdd" onchange="if(askEventDate()){submitIt('adddoc')};" $saveActive>
					<option style="display:none;" value="0" selected>Add existing Doc</option>
					$options_existing
				</select> of
				<button type="button" class="buttonlike flex" onclick="if(askMtfId()){submitIt('reload')}" $saveActive> MTF-No. </button>
			</td>
		</tr>
	</tbody>
</table>
