<label class="invisdesc">COMMENTS:</label>
<table class="greytable smaller">
	<colgroup>
		<col width="4%">
		<col width="55%">
		<col width="12%">
		<col width="20%">
		<col width="9%">
	</colgroup>
	<thead>
		<tr>
			<th><center><button type="button" onclick="submitIt('delcom')" class="buttonlike flex del" $saveActive>&#8999;</button></center></th>
			<th><b>COMMENT</b></th>
 			<th><b>TYPE OF COMMENT</b></th>
			<th><b>DATE</b></th>
			<th><b>AUTHOR</b></th>
		</tr>
	</thead>
	<tbody>
		$rows
		<tr>
			<td></td>
			<td colspan="4">
				<select class="buttonlike" name="comType" onchange="if(askEventDate()){askComment()};" $saveActive>
					<option value="-1" selected>Enter New Comment</option>
					$options
				</select>
			</td>
		</tr>
	</tbody>
</table>
