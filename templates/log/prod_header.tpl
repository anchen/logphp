<div class="bbox simple">
	<div class='border padd6x12'>Access Type: Production Plots</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6 light'>
		Please choose either a site or a module, and (!) also a week up to which the data 
		should be included in the plots. Then click on &quot;Generate&quot; to generate the 
		plots (potentially long loading times!):
	</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6 wrap'>
		<div class='column wthird'>Module Type:</div>
		<div class='column wthird'>Site:</div>
		<div class='column wthird fright'>Week before ...</div>
		<div class='clear'></div>
		<div class='column wthird'>
			<select id='filterType' name='filterType' class='invisiblelike $classTypes' onchange='changecolor("filterType");'>
				$options_filterTypes
			</select>
		</div>
		<div class='column wthird'>
			<select id='filterSite' name='filterSite' class='invisiblelike $classSites' onchange='changecolor("filterSite");'>
				$options_filterSites
			</select>
		</div>
		<div class='column wthird fright'>
			<select id='filterWeek' name='filterWeek' class='invisiblelike $classWeeks' onchange='changecolor("filterWeek");'>
				$options_filterWeeks
			</select>
		</div>
	</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6 wrap'>
		<div id="prodTwoThirds" class='column wthird left'>
			<input type='checkbox' name='filterIntegrated' $checked_filterIntegrated value='1'> <label>show numbers integrated up to chosen date</label>
		</div> 
		<div class="column wthird fright">
			<button type="button" class="buttonlike" onclick="submitIt('generate')">Generate</button>
		</div>    
	</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6 light'>
		<a href='#' onclick='event.preventDefault();togglePlots("prodHintInner")'>HINT: How to tell the DB that you finished some specific equipment?</a><br />
		<a href="downloads/ELTX_plots_instructions.pdf" target="_blank">HINT: Instructions for electronics plots...</a>
		<div id='prodHintOuter'>                           
			<div id='prodHintInner'>
				<p>...and reached a status monitored by the Production Overview:</p>
				<ul>
					<li>Select your Equipment in "Display Equipment".</li>
					<li>Change status to the desired status.</li>
					<li>Add the date the status should have changed at "event date" (if it was not today).
					    This field will appear at the top below the fields to select the equipment.</li>
					<li>Click on "Save Changes in Input Fields".</li>
				</ul>
				<p>The "desired status" for the Production Overview Table can be found here:</p>
				<table class="greytable normal">
					$hintRows
				</table>
			</div>
		</div>
	</div> 
</div>    
<!---
	<div id="searchfields" class="in3div">
		<div id='mftsearch'>
			<label><span class='desc'>Module Type: </span></label><br />
			<select id='filterType' name='filterType' class='invisiselectmenu $classTypes' onchange='changecolor("filterType");'>
				$options_filterTypes
			</select>
		</div>
		<div id='eqidsearch'>
			<label><span class='desc'>Site:</span></label><br />
			<select id='filterSite' name='filterSite' class='invisiselectmenu $classSites' onchange='changecolor("filterSite");'>
				$options_filterSites
			</select>
		</div>
		<div id='aliassearch'>
			<label><span class='desc'>Week before ...</span></label><br />
			<select id='filterWeek' name='filterWeek' class='invisiselectmenu $classWeeks' onchange='changecolor("filterWeek");'>
				$options_filterWeeks
			</select>
		</div>
	</div> 
	<div class='clear'></div>                            
	<div id='selProd4' class='left'>
		<input id='intProd' type='checkbox' name='filterIntegrated' $checked_filterIntegrated value='1'><label> show numbers integrated up to chosen date</label>
	</div> 
	<div id="commitbutt" class="in2div">
		<input id='saveCH' class="commitbuttons" name='commitbuttons' type='button' value='Generate' onclick="submitIt('generate')">
	</div>    
	<div class='clear'></div>                            
	<div id='hintProd'>
		<p>Please choose a site or a module and(!) a week.<br />Then click again on "Generate". (Potentially long loading times!)</p>
	</div>
	<p><a href='#' onclick='togglePlots("hint2Prod")'>How to tell the DB that you finished some specific equipment...</a></p>
	<div id='fixHintProd'>                           
		<div id='hint2Prod' style='display:none;'>
			<p>...and reached a status monitored by the Production Overview:</p>
			<ul>
				<li>Select your Equipment in "Display Equipment".</li>
				<li>Change status to the desired status.</li>
				<li>Add the date the status should have changed at "event date" (if it was not today).
				    This field will appear at the top below the fields to select the equipment.</li>
				<li>Click on "Save Changes in Input Fields".</li>
			</ul>
			<p>The "desired status" for the Production Overview Table can be found here:</p>
			<table>
				$hintRows
			</table>
		</div>
	</div>
</div>
-->
