<input type="hidden" name="showTree"        value="$showTree" />
<input type="hidden" name="comText"         value="" />
<input type="hidden" name="comdocEventDate" value="" />
<input type="hidden" name="docMtfId"        value="$docMtfId" />
<input type="hidden" name="writeStatLoc"    value="0" />
<div id="displayequipTable" class="gbox complex">
	<div id="displayequipColumnLeft" class="gbox column">
		<div id="explistContainer">
			<div class="explistControl">
				<a class="buttonlike" onclick="toggle('showTree', 0, 1);submitIt('refresh')">Show / Hide Equipment Tree</a>
			</div>
			$tree
		</div>						
	</div>
	<div id="displayequipColumnCenter" class="bbox column">
		<div id="displayequipRow1" class="heighth">
			<div id="eqinfoRow1Col1" class="bbox column wfifth">
				<div id="eqinfoMtfId" class="hthird">
					<input id='mtfId'        name='mtfId'        class='invisreadonly center' type="text" value="$mtfId" readonly>
				</div>
				<div class="hthird">
					<input id='eqTypeName'   name='eqTypeName'   class='invisreadonly center' type="text" value="$eqTypeName" readonly>
				</div>
				<div class="hthird">
					<input id='alternEqType' name='alternEqType' class='invisreadonly center' type="text" value="$alternEqType" readonly>
				</div>
			</div>
			<div id="eqinfoRow1Col2" class="bbox column wfifth">
				<div class="displayblock hhalf"><label class='invisdesc'>EQ ALIAS</label></div>
				<div class="displayblock hhalf"><input id='otherId' name='otherId' class='invisiblelike full center' type="text" 
								onchange="changecolor('otherId')" value="$otherId" pattern="$patternText"></div>
			</div>
			<div id="eqinfoRow1Col3" class="bbox column wfifth">
				<div class="hhalf"><label class='invisdesc'>EQ SUB BATCH ID</label></div>
				<div class="hhalf"><input id='subBatchId' name='subBatchId' class='invisreadonly center' type="text" value="$subBatchId" readonly></div>
			</div>
			<div id="eqinfoRow1Col4" class="bbox column wfifth">
				<div class="hhalf"><label class='invisdesc'>EQ ID</label></div>
				<div class="hhalf"><input id='eqId' name='eqId' class='invisreadonly center' type="text" value="$eqId" readonly></div>
			</div>
			<div id="eqinfoRow1Col5" class="bbox column wfifth">
				<div class="hhalf"><label class='invisdesc'>( GOOD UNITS / UNITS IN BATCH )</label></div>
				<div class="hhalf">
					<input id='goodQuantity' name='goodQuantity' class='invisiblelike half center' type="text" 
								onchange="changecolor('goodQuantity')" value="$goodQuantity" $statLocDisable pattern="$patternInt"> $quantityUnit / 
					<input id='quantity' name='quantity' class='invisiblelike half center' type="text" 
					            onchange="changecolor('quantity')" value="$quantity" $statLocDisable pattern="$patternInt"> $quantityUnit</div>
			</div>
		</div>
				
		<div id="displayequipRow2" class="heighth">
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>CURRENT STATUS</label></div>
				<div class="hhalf">$statusList</div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc smaller'>STATUS SINCE / $placeholderDateTimeSec (USER)</label></div>
				<div class="hhalf"><input id='statusCreate' name='statusCreate' class='invisreadonly center' type="text" value="$statusCreateTime ($statusCreateUser)" readonly></div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>EQ REGISTERED / $placeholderDate (USER)</label></div>
				<div class="hhalf"><input id='create' name='create' class='invisreadonly center' type="text" value="$createTime ($createUser)" readonly></div>
			</div>
		</div>
		<div id="displayequipRow3" class="heighth">
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>CURRENT LOCATION / SITE:</label></div>
				<div class="hhalf">$locationList</div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>BUILDING OR ROOM:</label></div>
				<div class="hhalf"><input id='locationRoomDesc' name='locationRoomDesc' class='invisiblelike full center' type="text" 
						onchange="if(this.reportValidity()){changecolor('locationRoomDesc');setStatLoc();toggleEventDate()}" value="$locationRoomDesc" pattern="$patternText"></div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc smaller'>LOCATION SINCE / $placeholderDateTimeSec (USER)</label></div>
				<div class="hhalf"><input id='locationCreate' name='locationCreate' class='invisreadonly center' type="text" 
						value="$locationCreateTime ($locationCreateUser)" readonly></div>
			</div>
		</div>
		<div id="displayequipRow4" class="heighth"> 
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>SUPPLIER:</label></div>
				<div class="hhalf"><input id='supplier' name='supplier' class='invisiblelike full center' type="text" 
						onchange="changecolor('supplier')" value="$supplier" pattern="$patternText" $disabled></div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>MANUFACTURING DATE / $placeholderDate:</label></div>
				<div class="hhalf"><input id='manufacturingdate' name='manufacturingdate' class='invisiblelike full center' type="text" 
						onchange="changecolor('manufacturingdate')" value="$manudate" pattern="$patternDate" $disabled></div>
			</div>
			<div class="bbox column wthird">
				<div class="hhalf"><label class='invisdesc'>DELIVERY DATE / $placeholderDate:</label></div>
				<div class="hhalf"><input id='deliverydate' name='deliverydate' class='invisiblelike full center' type="text" 
						onchange="changecolor('deliverydate')" value="$deldate" pattern="$patternDate" $disabled></div>
			</div>
		</div>
		<div id="displayequipRow5" class="heighth"> 
			<div id="displayequipRow5Col1" class="bbox column wquarter">
				<div class="hhalf"><label class='invisdesc'>CABLE LENGTH (METERS):</label></div>
				<div class="hhalf"><input id='cablelength' name='cablelength' class='invisiblelike full center' type="number"
						min='0' max='999.99' step='0.01' placeholder='0.00'
						onchange="changecolor('cablelength')" value="$cablelength" $disabled></div>
			</div>
			<div id="displayequipRow5Col2" class="bbox column wquarter">
				<div class="hhalf"><label class='invisdesc'>ORDERED BY:</label></div>
				<div class="hhalf"><input id='orderedby' name='orderedby' class='invisiblelike full center' type="text" 
						onchange="changecolor('orderedby')" value="$orderedby" pattern="$patternText" $disabled></div>
			</div>
			<div id="displayequipRow5Col3" class="bbox column wquarter">
				<div class="hhalf"><label class='invisdesc'>ORDER DATE / $placeholderDate:</label></div>
				<div class="hhalf"><input id='ordereddate' name='ordereddate' class='invisiblelike full center' type="text" 
						onchange="changecolor('ordereddate')" value="$ordereddate" $disabled></div>
			</div>
			<div id="displayequipRow5Col4" class="bbox column wquarter">
				<div class="hhalf"><label class='invisdesc'>ORDER REFERENCE (EDMS / DAI LINK):</label></div>
				<div class="hhalf"><input id='orderreference' name='orderreference' class='invisiblelike full center' type="url" 
						onchange="changecolor('orderreference')" value="$orderref" $disabled></div>
			</div>
		</div>
		<div id="displayequipRow6" class="heighth">$histtable</div>
		<div id="displayequipRow7" class="heighth">$doctable</div>
		<div id="displayequipRow8" class="heighth">$comtable</div>
		<div id="displayequipRow9" class="heighth">$heritage</div>
	</div>
	<div id="displayequipColumnRight" class="gbox column">
		<div id="displayequipBatch" class="hquarter">
			<div class="column whalf ncf_$confIndic">$ncfdropdown</div>
			<div class="column whalf type_$batchCols">
				<input name='batchIndic' class='invisreadonly batch' type="text" value="$batchIndic" readonly>
			</div>
		</div>
		<div id="displayequipSub"      class="hquarter">$subtable</div>
		<div id="displayequipOrigin"   class="hquarter">$origin</div>
		<div id="displayequipWorkflow" class="hquarter">$workflow</div>
	</div>
	<div class="clear"></div>
</div>
