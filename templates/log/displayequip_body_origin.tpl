This Item or Sub Batch was created by splitting a Group or a Batch 
(EQ ID <a href="#" onclick="set('newEqId', '$pareqid');submitItAs('load', 'eqId')">$pareqid</a>).<br /> 
It is still connected to this former Group or Batch and inherits all attributes concerning order, 
supplier and delivery, as well as all comments, documents, and status-location changes that are
linked to the former Group or Batch.
