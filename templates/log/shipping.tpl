<link rel="stylesheet" href="stylesNew/log/shipping.css">
<div class='bbox simple'>
	<div class='border padd6x12'>Access Type: Shipping View</div>
	<div class='clear'></div>
	<div class='border padd6x12 topm6'>
		Select shipment to view:<br />
		<div class='column whalf left'>
			<label>Shipment-ID:</label>
			<input class='invisiblelike' name='shipId' type='number' value='$shipId' min='1' max='99999' placeholder='#####'>
		</div>
		<div class='column whalf left'>
			<button type="button" class='buttonlike flex' onclick="submitIt('load')"  >Load Shipment</button>
			<button type="button" class='buttonlike flex' onclick="submitIt('export')">Download Shipping View</button>
		</div>
		<div class='clear'></div>
		The last shipment was #$lastShipId from $lastShipFrom to $lastShipTo.
	</div>
</div>
<div class='bbox simple'>
	<div class='border padd6x12'>
		Shipping view:
		<table class='greytable'>
			<colgroup>
				<col width='70px'><col width='23%'><col width='24%'><col width='10%'><col width='36%'>
			</colgroup>	                       
			<thead>
				<tr>
					<th>ID</th><th>MTF</th><th>Alias</th><th>EQ-ID</th><th>Description</th>
				</tr>
			</thead>
			<tbody>
				$shipments
			</tbody>
		</table>
	</div>
</div>
<div class='bbox simple'>
	<div class='border padd6x12'>
		<div class='column wquarter left shippingCol1'>
			<label>sent from:</label>
			<input class='invisiblelike' type='text' value='$shipLoc' readonly>
		</div>
		<div class='column wquarter left shippingCol2'>
			<label>person packing:</label>
			<input class='invisiblelike' type='text' value='$shipBy' readonly>
		</div>
		<div class='column wquarter left shippingCol3'>
			<label>sending date &amp; time:</label>
			<input class='invisiblelike' type='text' value='$shipDate' readonly>
		</div>
		<div class='column wquarter left shippingCol4'>
			<label>service provider:</label>
			<input class='invisiblelike' type='text' value='$shipComp' readonly>
		</div>
		<div class='clear'></div>
		<div class='caption shippingRow2'>
			<label>shipping reference (URL), if leaving CERN:</label>
			<input class='invisiblelike' type='url' value='$shipRef' readonly>
		</div>
	</div>
	<div id='recViewDetails' class='border padd6x12 topm6'>
		<div class='column wthird left shippingCol1'>
			<label class='caption'>sent to:</label>
			<input class='invisiblelike' type='text' value='$receiveLoc' readonly>
		</div>
		<div class='column wthird left shippingCol2'>
			<label class='caption'>person receiving:</label>
			<input class='invisiblelike' type='text' value='$receiveBy' readonly>
		</div>
		<div class='column wthird left shippingCol3'>
			<label class='caption'>receiving date &amp; time:</label>
			<input class='invisiblelike' type='text' value='$receiveDate' readonly>
		</div>
		<div class='clear'></div>
		<div class='caption shippingRow2'>
			<label>shipping reference (URL), if arriving at CERN:</label>
			<input class='invisiblelike' type='text' value='$receiveRef' readonly>
		</div>              	           
	</div>
</div>	                    
<div class='bbox simple'>
	<div class='border padd6x12'>
		Shipping comments: $shipcomm<br />
		<div style='display:$shipdisp'>
			<table class='greytable'>
				<colgroup>
					<col width='53%'><col width='17%'><col width='30%'>
				</colgroup>
				<thead>
					<tr>
						<th>Comment</th><th>Date</th><th>Author</th>
					</tr>
				</thead>
				<tbody>
					$eqcomments
				</tbody>                                
			</table>
		</div>
	</div>
</div>
<div class="bbox simple">
	<div class='border padd6x12 topm6'>
		Shipping papers: $shipdocs<br />
		<div style='display:$docdisp'>
			<table class='greytable'>
				<colgroup>
					<col width='10%'><col width='20%'><col width='2%'>
				</colgroup>
				<thead>
					<tr>
						<th>Type</th><th>Name</th><th>Format</th>
					</tr>
				</thead>
				<tbody>
					$documents
				</tbody>                             
			</table>
		</div>	                       
	</div>
	<div class='border padd6x12 topm6 right'>
		<div class='left'>
			Atttach new shipping paper (optional, max. 2MB):
			<input id='newPaper' class='invisiblelike' name='newPaper' accept='$allowedUplTypes' 
                                     onchange='changecolor("newPaper")' type='file'>
		</div>
		<div class='clear'></div>
		<div class='right'>
			<label id='helpEvDate'>event date <span class='helpIcon'>?</span>:
				<div class='legendEvDate mt'>
					<p>
					Always enter this date, when the date of the event that you want to report about
					is not the same as the date when you are reporting the action.
					</p>
					<p>
					For example: You should have splitted something 3 weeks ago,
					but you only now had the time to enter this in the DB. Put here the date of 3 weeks ago,
					so the DB will not get confused if you want to send something last week, that only has been splitted now...
					</p>
				</div>
			</label>
			<input id='eventDate' class='invisiblelike' type='text' name='eventDate' value='$eventdate' 
        	     onchange='changecolor("eventDate")' pattern='$patternDate' placeholder='$placeholderDate'>
			<button type="button" class='buttonlike flex' onclick="if(checkFileSize('newPaper')){submitIt('paper');}">Attach Paper to the Shipment</button>
		</div>
	</div>
</div>
