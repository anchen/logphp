<script type="text/javascript" src="jsNew/log/receiveequip.js"></script>      
<link rel="stylesheet" href="stylesNew/log/receiveequip.css">
<input id='itemsSelected'  name='itemsSelected'  type='hidden' value='$itemsSelected' >
<input id='itemsDates'     name='itemsDates'     type='hidden' value='$itemsDates' >
<input id='itemsToIds'     name='itemsToIds'     type='hidden' value='$itemsToIds' >
$header
<div class='bbox simple'>
	<div class='border padd6x12'>
		<div class="column wthird left receiveequipRow1">
			<label>new location / site<span class="mandatory">*</span>:</label>
			<select class="buttonlike flex" id='newLoc' name='locId'>
				$options_locId
			</select>
		</div>
		<div class="column wthird left receiveequipRow1">
			<label>new building or room:</label>
			<input id='locDesc' class='invisiblelike' name='locDesc' value='$locDesc' 
			           placeholder='building or room' onchange='changecolor("locDesc")'>
		</div>
		<div class="column wthird left receiveequipRow1 fright">
			<label>new status<span class="mandatory">*</span>:</label>
			<select class="buttonlike" id='newStat' name='statusId'>
				$options_statusId
			</select>	                          
		</div>
		<div class='clear'></div>
		<div class="column wthird left receiveequipRow3">
			<label>person receiving<span class="mandatory">*</span>:</label>
			<input id='recPerson' class='invisiblelike' name='recPerson' value='$recPerson' 
			           onchange='changecolor("recPerson")'>
		</div>
		<div class="column wthird left receiveequipRow3">
			<label>receiving date &amp; time<br />($placeholderDateTime)<span class="mandatory">*</span>:</label>
			<input id='recDate' class='invisiblelike' name='recDate' value='$recDate' 
			           pattern='$patternDateTime' placeholder='$placeholderDateTime' onchange='changecolor("recDate");'>
		</div>
		<div class="column wthird left receiveequipRow3 fright">
			<label>attach shipping papers (max. 2MB):</label>
			<input id='recPapers' class='invisiblelike' type='file' name='uplnewdoc' 
			            accept='$allowedUplTypes' onchange='changecolor("recPapers")'>
		</div>
		<div class='clear'></div>
		<div class="column whalf left receiveequipRow4">
			<label>shipping reference (URL), if arriving at CERN:</label>
			<input id='recRef' class='invisiblelike' name='recRef' value='$recRef' placeholder='http://' 
			            onchange='changecolor("recRef")'>
			<br />
			<label>shipping comment:</label>
			<input id='comment' class='invisiblelike' type='text' name='comment' value='$comment' 
			           onchange='changecolor("comment")'>
		</div>
		<div class="column wthird fright heightAuto middle">
			<button type="button" class='buttonlike flex $subRecStatClass' $subRecStatus onclick='if(checkRecEquip() && checkFileSize("uplnewdoc")){submitIt("save")}'>RECEIVE<br />EQUIPMENT</button>
		</div>
		<div class='clear'></div>
	</div>
</div>
