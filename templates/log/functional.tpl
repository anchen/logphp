<div id="button">  
	<button type="button" class="downloadbuttons log" onClick="submitIt('exportCsv')">Download table as <b>CSV</b> file</button>
	<button type="button" class="downloadbuttons log" onClick="submitIt('exportExcel')">Download table as <b>EXCEL</b> file</button>
</div>
$pageSelector
<div class='whitediv'>
	<div class="tablediv">
		<table class="tablesorter floatThead" id="t03">
			<colgroup>
				<col width="5%">
				<col width="5%">
				<col width="5%">
				<col width="40%">
				<col width="15%">
				<col width="15%">
				<col width="15%">
			</colgroup>
			<thead>
				<tr>
					<th class="vertical header"></th>
					<th class="vertical header"><b>Nb.</b></th>
					<th class="vertical header"><b>EQ Type Code</b></th>
					<th class="vertical header" colspan="2"><b>Name</b></th>
					<th class="vertical header"><b>Date</b></th>
					<th class="vertical header"><b># Left</b></th>
				</tr>
			</thead>
			<tbody>
				$rows
			</tbody>
		</table>
	</div>
</div>
<table class="invisiTable" id='invisiHeader' style="visibility: hidden">
	<thead>
		<tr>
			$invisi_head
		</tr>
	</thead>
	<tbody>
		$invisi_body
	</tbody>
</table>
