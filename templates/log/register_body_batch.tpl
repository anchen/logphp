<tr>
	<td><label>Quantity of Items in Batch<span class='mandatory'>*</span>:</label></td>
	<td><input class="invisiblelike" name='quantity' type='number' min='2' max='99999' value='$quantity'></td>
</tr>
<tr>
	<td><label>Quantity of Good Items in Batch:</label></td>
	<td><input class="invisiblelike" name='goodQuantity' type='number' min='0' max='99999' value='$goodQuantity'></td>
</tr>
