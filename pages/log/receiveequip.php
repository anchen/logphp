<?php

require_once "library/page.php";
require_once "pages/all/all.php";
require_once "pages/all/ranges.php";


// ReceiveEquip Page
// ============================================
class ReceiveEquipPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $items = NULL;

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		$this->items = array();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit(){
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		rangesLoadItems($this, 2);
		$res = false;
		if     ($this->post["do"]=="clr"      ) $this->submitClear();
		else if($this->post["do"]=="go"       ) $res = rangesSubmitGo($this);
		else if($this->post["do"]=="loadShip" ) rangesSubmitAddMode2($this, $this->post["shipIntId"]);
		else if($this->post["do"]=="loadTrans") rangesSubmitAddMode2($this, -1, $this->post["siteFromId"], $this->post["siteToId"]);
		else if($this->post["do"]=="save"     ) $res = $this->submitSave();
	
		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}


	// private members and methods
	// ======================================== 

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Building the form */

		// last shipment
		$allSites = $this->master->getOptionsSites();	
		$cs = new DbConfig($this->master, "lastshipment");
		$cs->columns = array("shipmentintid", "shippingfrom", "shippingdestination");
		$cs->order("createtime", "desc");
		$cs->limit(1);
		$shipment = $this->db->read("shippinghistory", $cs);
		$this->html->set("lastShipId"  ,           $shipment->shipmentintid       );
		$this->html->set("lastShipFrom", $allSites[$shipment->shippingfrom       ]);
		$this->html->set("lastShipTo"  , $allSites[$shipment->shippingdestination]);

		// site options
		$optsSites = $this->master->getOptionsSites("duringreceiveflag");
		$optsTo    = array(0=>"all") + $optsSites;
		$optsFrom  = array(0=>"all") + $optsSites;
		foreach($optsFrom as $key=>$site){ // FIXME: still needed? 
			if(in_array($site, array("ELTOS", "ELVIA"))) 
				unset($optsFrom[$key]);
		}

		$userLoc  = $this->master->getUserSite();
		$selected = count($this->items)>0 ? $this->items[0][0]->toId : $userLoc;

		$this->html->set("options_siteFromId", $this->html->makeOptions($optsFrom , isset($this->post["siteFromId"]) && $this->post["siteFromId"]>0 ? $this->post["siteFromId"] : 0       ));
		$this->html->set("options_siteToId"  , $this->html->makeOptions($optsTo   , isset($this->post["siteToId"  ]) && $this->post["siteToId"  ]>0 ? $this->post["siteToId"  ] : $userLoc));

		// build list of shipping dates
		$shipDates = array();
		$shipToIds = array();
		$latest    = 0;
		$toIds     = array();
		foreach($this->items as $idx=>$range){
			foreach($range as $item){
				if($item->dateRaw>$latest) $latest = $item->dateRaw;
				array_push($shipDates, sprintf("%d,%s", $idx+1, $item->dateRaw));
				array_push($shipToIds, sprintf("%d,%d", $idx+1, $item->toId   ));
				array_push($toIds    , $item->toId);
			}
		}
		$toIds = array_unique($toIds);
		$this->html->set("itemsDates", implode(";", $shipDates));
		$this->html->set("itemsToIds", implode(";", $shipToIds));

		// disable all in case no items are given
		if(count($this->items)==0){
			$this->html->set("subRecStatClass", "grey"    );
			$this->html->set("subRecStatus"   , "disabled");
		}

		// custom header
		$custom = $this->html->template("receiveequip_customHead");

		// the entire ranges part
		rangesLoadPage($this, true, false, "Access Type: Receive Equipment", true, false, 2, false, $custom);

		// location
		$keys   = array_keys($optsSites);
		$disAll = array_diff($keys, $toIds);
		$this->html->set("options_locId", $this->html->makeOptions($optsSites, $selected, $disAll));
		//$this->html->set("usersLocation", $userLoc);

		// status
		$optsStatus = $this->master->getOptionsStatus("duringreceiveflag");
		$this->html->set("options_statusId", $this->html->makeOptions($optsStatus, array_search("distributed", $optsStatus)));

		// rec date
		$this->html->set("recDate", array_key_exists("recDate", $this->post) ? $this->post["recDate"] : $this->master->eventDateHrS);

		// the remaining template
		return $this->html->template("receiveequip");
	}

	// submitClear
	// ---------------------------------------- 
	private function submitClear(){
		/* Clears the entire table */
		$this->items = array();
	}

	// submitSave
	// ---------------------------------------- 
	private function submitSave(){
		/* Receiving the equipment */

		// basic checks
		if(count($this->items)<1 ) return false;
		if($this->post["locId"]<1) return false;

		// check: at least one selected
		if(empty($this->post["subRanges"]) || count($this->post["subRanges"])<1){
			$this->vb->error("Please select a valid set of items to receive!");
			return false;
		}

		// extract shipping dates for selected entries
		$allToIds = explode(";", $this->post["itemsToIds"]);
		$toIds    = array();
		foreach($allToIds as $entry){
			$elm = explode(",", $entry);
			if(!in_array(intval($elm[0]), $this->post["subRanges"])) continue;
			array_push($toIds, intval($elm[1]));
		}
		$toIds = array_unique($toIds);
		if(count($toIds)>1){
			$this->vb->error("Cannot receive equipments shipped to different locations!");
			return false;
		}

		// extract shipping dates for selected entries
		$allDates = explode(";", $this->post["itemsDates"]);
		$dates    = array();
		foreach($allDates as $entry){
			$elm = explode(",", $entry);
			if(!in_array(intval($elm[0]), $this->post["subRanges"])) continue;
			array_push($dates, $elm[1]);
		}

		// date
		sort($dates);
		$receiveDate   = isValidDate($this->post["recDate"]) ? timestamp(2, $this->post["recDate"]) : $this->master->eventDate;
		$receiveDateDb = dbStringDate($receiveDate);
		if(isEarlier(timestamp(0, $receiveDate), $dates[count($dates)-1])){
			$this->vb->error("Receiving date is prior to the last shipping date!");
			return false;
		}

		// sanity check
		if(count($dates)==0){
			$this->vb->error("Please select a valid set of items to receive!");
			return false;
		}

		// update info for each items
		$theids = array();
		foreach($this->items as $idx=>$range){
			if(!in_array($idx+1, $this->post["subRanges"])) continue; // only selected items
			foreach($range as $item){

				// not allowed to change the location
				if($item->toId != $this->post["locId"]) {
					$this->vb->error("Cannot change the shipping destination upon receiving!");
					return false;
				}

				// update shipping table
				$c = new DbConfig($this->master, "update");
				$c->select("id_shippinghistory", $item->shipId);
				$this->db->shippinghistory->update(array("receivingdate"      =>$receiveDateDb,
				                                         "receivingperson"    =>$this->post["recPerson"],
				                                         "returningshipref"   =>$this->post["recRef"],
				                                         "websiteusered"      =>$this->globals["username"]), $c);
				$this->db->shippinghistory->push();
				if($this->db->error()){
					$this->vb->error("Could not update shipping information!", true);
					return false;
				}
				
				// insert status
				$this->db->statuslocation->append(array("eqentryid"    =>$item->eqid,
				                                        "statusid"     =>$this->post["statusId"],
				                                        "majorlocid"   =>$this->post["locId"],
				                                        "minorlocdesc" =>$this->post["locDesc"],
				                                        "websiteusered"=>$this->globals["username"],
				                                        "websiteusercr"=>$this->globals["username"],
				                                        "eventdate"    =>$receiveDateDb));
				$this->db->statuslocation->push();
				if($this->db->error()){
					$this->vb->error("Could not insert new status-location data!", true);
					return false;
				}
	
				$theids[$item->eqid] = $item->shipId;
			}
		}

		// comment
		if(!empty($this->post["comment"])){
			foreach($theids as $eqId=>$shipId) {
				if(!addComment($this, $this->post["comment"], -1, $this->post["recDate"], array($eqId), array($eqId=>$shipId), "shipping", true, false)) 
					return false;
			}
		}

		// document
		if(is_uploaded_file($_FILES['uplnewdoc']['tmp_name'])){
			foreach($theids as $eqId=>$shipId){
				if(!uploadDoc($this, "uplnewdoc", -1, $this->post["recDate"], array($eqId), array($eqId=>$shipId), "shipping", true, false))
					return false;
			}
		}

		// All good: commit and clear buffer
		$this->vb->success("Shipping information updated successfully!");

		// remove received items from the table
		$newItems = array();
		foreach($this->items as $idx=>$range){
			if(in_array($idx+1, $this->post["subRanges"])) continue; // only selected items
			array_push($newItems, $range);
		}
		$this->items = $newItems;

		// return true
		return true;
	}
}


$page = new ReceiveEquipPage($this, "receiveequip");

?>
