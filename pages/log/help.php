<?php

require_once "library/page.php";


// HelpPage
// ============================================
class HelpPage extends Page {

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		if($this->master->requireMultiEdit()){
			$this->html->set("multiedit_toc" , $this->html->template("help_multiedit_toc" ));
			$this->html->set("multiedit_body", $this->html->template("help_multiedit_body"));
		}
		return $this->html->template("help");
	}
}

$page = new HelpPage($this, "help");

?>
