<?php

require_once "library/page.php";
require_once "pages/all/all.php";


// mergeJsons
// --------------------------------------------
function mergeJsons($jsons){
	if(count($jsons)==0) return array();
	$merged  = array("cols"=>array(), "rows"=>array());
	$keys    = array_keys($jsons);
	$indices = array();
	for($i=0; $i<count($jsons); ++$i){
		array_push($indices, array());
		foreach($jsons[$keys[$i]]["cols"] as $idx=>$col){
			//if(in_array($col, array("BORDATE", "Annotation", "Certainty"))){
			//	if($i!==0) continue;
			//}
			array_push($merged["cols"], $col);
			array_push($indices[$i]   , $idx);
		}
	}
	$len = 0;
	foreach($indices as $idcs) $len += count($idcs);
	for($r=0; $r<count($jsons[$keys[0]]["rows"]); ++$r){
		$theRow = array_fill(0, $len, NULL);
		$j=0;
		for($i=0; $i<count($jsons); ++$i){
			foreach($indices[$i] as $idx){
				$theRow[$j] = $jsons[$keys[$i]]["rows"][$r][$idx];
				++$j;
			}
		}
		array_push($merged["rows"], $theRow);
	}
	return $merged;
}

// cleanCell
// --------------------------------------------
function cleanCell(&$cell){
	if($cell==="") $cell = NULL;
}


// Prod Page
// ============================================
class ProdPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public  $theDate = NULL;
	private $buffer  = array();
	private $bufferIsComb  = "";
	private $bufferCaption = "";
	private $bufferSite    = "";

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */
		$this->buffer        = array();
		$this->bufferIsComb  = "";
		$this->bufferCaption = "";
		$this->bufferSite    = "";
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		// load buffer
		//$time_start = microtime(true); 
		$this->loadBuffer();
		//$time_end = microtime(true); 
		//print "Loading buffer (s): ".($time_end-$time_start)."<br/>";

		// plotting and listing
		//$time_start = microtime(true); 
		if     ($this->post["do"]=="showPlot") $this->submitPlot($this->post["bufferIsComb"]==1 ? true : false);
		else if($this->post["do"]=="showList") $this->submitList($this->post["bufferIsComb"]==1 ? true : false);
		//$time_end = microtime(true); 
		//print "Loading plot (s): ".($time_end-$time_start)."<br/>";

		// rest is only displaying
		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// buildListComb
	// ---------------------------------------- 
	private function buildListComb($plotId){
		/* Builds the list of the associated combined time development plot for a given id */

		//FIXME: needed?
		//$poplotgroup = $this->db->readTable("poplotgroup", array("plotgroupdesc"), array("id_poplotgroup"=>$plotId));
		//$legend      = $poplotgroup->plotgroupdesc;

		$poplotgroupcont = $this->db->readTable("poplotgroupcont", array("*"), array("plotgroupid"=>$plotId), array(), array("sort"=>"desc"));

		// build the data
		$lists = array();
		foreach($poplotgroupcont as $row){
			$lists[$row->prodoverviewid] = $this->buildListData($row->prodoverviewid);
		}

		// merge lists into one csv
		$full  = array();
		$nElms = array_map("count", $lists);
		$keys  = array_keys($nElms, max($nElms));
		$iMax  = $keys[0];
		$nMax  = $nElms[$iMax];

		// first two rows are header
		$first  = array();
		$second = array();
		foreach($lists as $list){
			array_push($first , $list[0].";;");
			array_push($second, $list[1]     );
		}
		array_push($full, implode(";", $first ));
		array_push($full, implode(";", $second));

		// body
		for($i=2; $i<=$nMax; ++$i){ // first two rows are header
			$row = array();
			foreach($lists as $list){
				if(count($list)>$i) $add = $list[$i];
				else                $add = ";;";
				array_push($row, $add);
			}
			array_push($full, implode(";", $row));
		} 

		// build the file
		$this->buildListOutput($full, sprintf("prodlist_c_%s_%s", strval($plotId), $this->master->eventDateFs));
	}

	// buildListData
	// ---------------------------------------- 
	private function buildListData($taskId) { 
		/* Builds the data for the list corresponding to the time development plot 
		* for a given task Id (= id prodoverview) */

		// collect general parameters
		$prodoverview = $this->db->readTable("prodoverview", array("*"), array("id_prodoverview"=>$taskId));
		$lm           = trim(           $prodoverview->listingmethod);
		$lt           = trim(strtolower($prodoverview->listingtable ));
		$raw          = NULL;
		$table        = NULL;
		$use          = "ed";

		if($lm=="normal"){ // default listing method
			$cols  = $this->db->get($lt)->columns();
			$use   = in_array("rd", $cols) ? "rd" : (in_array("sd", $cols) ? "sd" : "ed"); // date field
			$table = $this->db->readTable($lt, array("id_equipment", "partsbatchmtfid", "evdate"), 
			                                   array("pid"=>$taskId), 
                                               array("TO_CHAR(".$use.", 'YYYY-MM-DD HH24:MI:SS')"=>"evdate"), array("evdate"=>"asc"), true);
		}

		else if(substr(strtolower($lm), 0, 5)=="where"){ // using listing method as where query
			$raw = $this->db->readSql("SELECT 
                    mid.id_equipment, 
                    mid.partsbatchmtfid, 
                    TO_CHAR(mid.ed, 'YYYY-MM-DD HH24:MI:SS') evdate 
                FROM 
                    ".$this->db->owner.".prodov_statbasics mid 
			    LEFT OUTER JOIN EQUIPMENT E ON E.ID_EQUIPMENT = MID.ID_EQUIPMENT 
                    ".$lm." 
                AND 
                    mid.pid = ".$taskId."
			    ORDER BY evdate asc;");
		}

		else { // else: listing method is subquery
			$raw = $this->db->readSql("SELECT
			        mid.id_equipment,
			        mid.partsbatchmtfid,
                    TO_CHAR(mid.ed, 'YYYY-MM-DD HH24:MI:SS') evdate
			    FROM
			        ".$this->db->owner.".prodov_statbasicsstrict mid
			    WHERE
			        mid.pid = ".$taskId."
			        AND mid.ed >= (
			            SELECT
			                MAX(midi.ed)
			            FROM
			                atlas_muon_nsw_mm_log.prodov_statbasicsstrict midi
			            WHERE
			                midi.pid IN (".$lm.")
			                AND midi.id_equipment = mid.id_equipment
			        )
			    ORDER BY evdate;");
		}

		$rows = array(sprintf("%s (%d)", $prodoverview->prodtaskdesc, $prodoverview->prodoversort), "Date;EQ ID;MTF ID");
		if(!empty($table)){
			foreach($table->slim as $row){
				array_push($rows, sprintf("%s;%d;%s", $row["evdate"], $row["id_equipment"], $row["partsbatchmtfid"]));
			}
		}
		if(!empty($raw)){
			foreach($raw as $row){
				array_push($rows, sprintf("%s;%d;%s", $row["evdate"], $row["mid_id_equipment"], $row["mid_partsbatchmtfid"]));
			}
		}
		return $rows;
	}

	// buildListSingle
	// ---------------------------------------- 
	private function buildListOutput($rows, $fileName){
		/* Write the rows to the csv file and open it */

		// write to the file
		if(!file_put_contents($this->master->downloadPath.$fileName.".csv", implode("\n", $rows))) 
			return false;

		// call the file
		header("Content-Type: application/vnd.ms-excel");
		header("Content-disposition: attachment;filename=".$fileName.".csv");
		readfile($this->master->downloadPath.$fileName.".csv");
		exit(); // otherwise readfile does not work properly

		//$this->html->set("openPopup", "");
		//$this->html->set("openWhat" , "list");
		//$this->html->set("openUrl"  , $this->master->downloadPath.$fileName.".csv");
		//$this->vb->success($this->html->template("prod_popup"));
		//return true;
	}

	// buildListSingle
	// ---------------------------------------- 
	private function buildListSingle($taskId) { 
		/* Builds the list corresponding to the time development plot for a given task Id (= id prodoverview) */

		$rows = $this->buildListData($taskId);
		$prodoverview = $this->db->prodoverview; // query done in buildListData
		$fileName     = sprintf("prodlist_s_%s_%s", str_pad($prodoverview->prodoversort, 3, "0", STR_PAD_LEFT), $this->master->eventDateFs);
		$this->buildListOutput($rows, $fileName);
	}

	// buildPlotComb
	// ---------------------------------------- 
	private function buildPlotComb($plotId){
		/* Builds a combined time development plot for a given id */

		$poplotgroup = $this->db->readTable("poplotgroup", array("plotgroupdesc"), array("id_poplotgroup"=>$plotId));
		$legend      = $poplotgroup->plotgroupdesc;

		$poplotgroupcont = $this->db->readTable("poplotgroupcont", array("*"), array("plotgroupid"=>$plotId), array(), array("sort"=>"desc"));

		$minBors = array();
		$pars    = array();
		$i       = 0;
		$mcidx   = null;
		foreach($poplotgroupcont as $row){
			$params = $this->getPlotParams($row->prodoverviewid);
			$params["col1"] = $row->maincolor;
			$params["col2"] = $row->secondcolor;
			$params["leg1"] = $row->mainlegend;
			$params["leg2"] = $row->secondlegend;
			$params["ismc"] = ($row->ismaincurve=="T");
			$params["exp" ] = ($row->withexp=="T");
			$pars[strval($row->id_poplotgroupcont)] = $params;
			array_push($minBors, $params["minbor"]);
			if($params["ismc"]) $mcidx = strval($row->id_poplotgroupcont);
			++$i;
		}

		// start with main curve to get bordate at beginning
		$pnew = array();
		if(!empty($mcidx)) $pnew[$mcidx] = $pars[$mcidx];
		foreach($pars as $key=>$params){
			if($key==$mcidx) continue;
			$pnew[$key] = $params;
		}

		// minimum BOR over all
		sort($minBors, SORT_NUMERIC);
		$minBOR = reset($minBors);
		foreach($pnew as $k=>$p) $pnew[$k]["minbor"] = $minBOR;

		// build the data
		$jsons = array();
		foreach($pnew as $taskId=>$params){
			$jsons[$taskId] = $this->buildPlotData($params["id_prodoverview"], $params, true);
		}

		// merge json
		$merged = mergeJsons($jsons);

		// build plot
		$fileName = sprintf("prodplot_c_%s_%s", strval($taskId), $this->master->eventDateFs);
		return $this->buildPlotOutput($merged, $pnew, $fileName, $legend);
	}

	// buildPlotData
	// ---------------------------------------- 
	private function buildPlotData($taskId, &$params=array(), $combined=false, $clean=true) { 
		/* Builds the time development plot for a given task Id (= id prodoverview) */

		// collect general parameters
		if(empty($params)) $params = $this->getPlotParams($taskId);

		if(!array_key_exists("col1", $params)) $params["col1"] = "339900";
		if(!array_key_exists("col2", $params)) $params["col2"] = "ffbf00";
		if(!array_key_exists("leg1", $params)) $params["leg1"] = "Number of items";
		if(!array_key_exists("leg2", $params)) $params["leg2"] = NULL;

		$addJson   = array();
		$addSel    = "";
		$addJoin   = "";

		$specialTasks = array(86, 87, 114); // FIXME: not needed if prodoverview has column withexp
		$withExp      = (in_array($taskId, $specialTasks) || (isset($params["exp"]) && $params["exp"]=="T")) ? true : false;

		// with expected contour
		if($withExp){
			//$params["leg2"] = $this->db->readTable("prodoverviewexp", array("expdescription"), array("prodtask"=>$taskId))->expdescription;
			$addSel    = ", (CASE
			         WHEN S.EXPVALUE is null and BORWEEK > 
			             (select max(WEEK) from 
			             (SELECT POVC.WEEK,
			             POVC.EXPVALUE
			             FROM ".$this->db->owner.".PRODOVERVIEWEXP POE
			             LEFT OUTER JOIN ".$this->db->owner.".PRODOVERVIEWCONT POVC
			             ON POE.ID_PRODOVERVIEWEXP = POVC.PRODEXPNO
			             WHERE POE.PRODTASK = ".$taskId.")
			             where EXPVALUE is not null)
			         
			         THEN 
			             (SELECT MAX(EXPVALUE) from (SELECT POVC.WEEK,
			             POVC.EXPVALUE
			             FROM ".$this->db->owner.".PRODOVERVIEWEXP POE
			             LEFT OUTER JOIN ".$this->db->owner.".PRODOVERVIEWCONT POVC
			             ON POE.ID_PRODOVERVIEWEXP = POVC.PRODEXPNO
			         WHERE POE.PRODTASK = ".$taskId."))
			             WHEN S.EXPVALUE > 0
			             THEN S.EXPVALUE
			             ELSE (NULL)
			         END) AS EXPVAL";
			$addJoin = "LEFT OUTER JOIN 
			                (SELECT POVC.WEEK, POVC.EXPVALUE
			                FROM ".$this->db->owner.".PRODOVERVIEWEXP POE
			                LEFT OUTER JOIN ".$this->db->owner.".PRODOVERVIEWCONT POVC
			                ON POE.ID_PRODOVERVIEWEXP = POVC.PRODEXPNO
			                WHERE POE.PRODTASK        = ".$taskId.") S
				        on S.WEEK = BORWEEK";
		}

		// build the json
		$json = array_map('utf8_encode', array());
	
		// json header	
		$json["cols"] = array();
		if(!$combined || (isset($params["ismc"]) && $params["ismc"]))
			array_push($json["cols"], "BORDATE");
		if($withExp) 
			array_push($json["cols"], $params["leg2"]);
		array_push($json["cols"], $params["leg1"]);
		if(!$combined || (isset($params["ismc"]) && $params["ismc"])){
			array_push($json["cols"], "Annotation");
			array_push($json["cols"], "Certainty");
		}

		// assemble SQL
		$sql = "SELECT TO_CHAR(BORDATE,'YYYY-MM-DD') AS BD, COUNTNO,
		        (CASE
		            WHEN ABS(BORDATE-sysdate) < 7 AND BORDATE-sysdate > 0 THEN 'upcoming report'
		         ELSE
		             (null)
		         END) AS CS,
		        (CASE  
		             WHEN BORDATE-sysdate > 0 THEN '0'
		             WHEN BORDATE-sysdate < 0 THEN '1'    
		         ELSE
		             (null)
		         END) AS CT
				".$addSel."
		        FROM TABLE(".$this->db->owner.".".$params["functiontype"]."(".$taskId."))
				".$addJoin."
		        WHERE BORWEEK BETWEEN ".$params["minbor"]." -2 AND ".$params["maxbor"]." ORDER BY BORWEEK";

		// data and json body
		$rows  = array();
		$rows2 = array(); // need it for cleaning (below)
		//$time_start = microtime(true); 
		//$result = $this->db->readSql($sql);
		//$time_end = microtime(true); 
		//print "Execution time: ".($time_end-$time_start)."<br/>";
		//foreach($result as $row){
		foreach($this->db->readSql($sql) as $row){
			$temp  = array();
			$temp2 = array();
			if(!$combined || (isset($params["ismc"]) && $params["ismc"]))
				array_push($temp, (string) $row["bd"]);
			array_push($temp2, (string) $row["bd"]);
			if($withExp)
				array_push($temp, (float) $row["expval"]);
			array_push($temp , (float) $row["countno"]);
			array_push($temp2, (float) $row["countno"]);
			if(!$combined || (isset($params["ismc"]) && $params["ismc"])){
				array_push($temp, (string) $row["cs"]);
				array_push($temp, (bool) $row["ct"]);
			}
			array_push($rows , $temp);
			array_push($rows2, $temp2);
		}

		// clean data
		array_walk_recursive($rows, 'cleanCell');

		// clean leave the last two empty values
		if($clean){
			$allowed = 2;
			$toSet   = array();
			$having  = 0;
			$revs    = array_reverse($rows2);
			$idx     = count($rows2)+1;
			foreach($revs as $row){
				$idx--;
				$bd  = $row[0];
				$val = $row[1];
				if($val!=0) continue;
				if($having<$allowed){
					$having++;
					continue;
				}
				array_push($toSet, $idx);
			}
			foreach($rows as $idx=>$row){
				if(!in_array($idx, $toSet)) continue;
				$rows[$idx] = NULL;
			}
		}

		// store and return
		$json["rows"] = $rows;
		return $json;
	}

	// buildPlotOutput
	// ---------------------------------------- 
	private function buildPlotOutput($raw, $pars, $fileName, $title=NULL){
		/* Builds the html output for a plot encoded in a json */

		// init json
		$json = array_map('utf8_encode', array());
		$json["cols"] = array();
		$json["rows"] = array();
		
		// build columns
		foreach($raw["cols"] as $col){
			if($col=="BORDATE")
				array_push($json['cols'], array('id'=>NULL, 'label'=>'BORDATE', 'type'=>'string' ));
			else if($col=="Annotation")
				array_push($json['cols'], array('id'=>NULL, 'label'=>NULL     , 'type'=>'string' , 'role'=>'annotation'));
			else if($col=="Certainty")
				array_push($json['cols'], array('id'=>NULL, 'label'=>NULL     , 'type'=>'boolean', 'role'=>'certainty' ));
			else 
				array_push($json['cols'], array('id'=>NULL, 'label'=>$col     , 'type'=>'number' ));
		}

		// build rows
		$rows = array();
		foreach($raw["rows"] as $row){
			$theRow = array();
			foreach($row as $val) $theRow[] = array("v"=>$val);//array_push($theRow, array('v' => $val));
			$rows[] = array("c"=>$theRow); 
			//array_push($json["rows"], $theRow); 
		}
		$json["rows"] = $rows;

		// output the json
		$this->html->set("json", json_encode($json));

		// build the attributes
		//$expFileName  = sprintf("graph_id%s_%s", str_pad($params["prodoversort"], 3, "0", STR_PAD_LEFT), $this->master->eventDateFs);
		$this->html->set("expFileName", $fileName                  );
		$this->html->set("plotDate"   , $this->master->eventDateHr );
		//$this->html->set("taskId"     , $taskId                    );

		// combined plots attributes
		if(count($pars)>1 && !empty($title)){
			$this->html->set("plotTitle", $title     );
			$this->html->set("plotType" , "LineChart");
		}
		// normal plot attributes
		else {
			$this->html->set("plotTitle", sprintf("%s / %s : %s", $pars[0]["moduletype"], $pars[0]["sitename"], $pars[0]["prodtaskdesc"])); 
			$this->html->set("plotType" , "AreaChart");
		}

		// build the colors
		$gcols = array();
		foreach($pars as $params){
			array_push($gcols, sprintf("'#%s'", $params["col1"]));
			if(!empty($params["leg2"])) // leg2 is null if there's no exp curve, col2 not
				array_push($gcols, sprintf("'#%s'", $params["col2"]));
		}
		$this->html->set("gcolors", implode(",", $gcols));

		// build invisible table header
		$fields = array();
		foreach($raw["cols"] as $idx=>$col){
			$name = $col;
			if($name=="BORDATE") $name = "Date";
			array_push($fields, sprintf("<th>%s</th>", $name)); // FIXME: mere process name here, but how put 'Expected #' or 'Items #' in case of single process?
		}
		$this->html->set("colspan", count($fields));
		$this->html->set("invhead", implode("", $fields));

		// build invisible table body
		$invbody = array();
		foreach($raw["rows"] as $row){
			$fields = array();
			foreach($row as $val)
				array_push($fields, sprintf("<td>%s</td>", strval($val)));
			array_push($invbody, sprintf("<tr>%s</tr>", implode("", $fields)));
		}
		$this->html->set("invbody", implode("", $invbody));

		// add the scripts
		$this->html->set("tableId"     , "invisiTable");
		$this->html->set("canvasId"    , "plotCanvas" );
		$this->html->set("png"         , "plotPng"    );
		$this->html->set("linkCsv"     , "plotLinkCsv");
		$this->html->set("linkXls"     , "plotLinkXls");
		$this->html->set("scriptData"  , $this->html->template("plots_onedim_default", array(), NULL, "all"));
		$this->html->set("scriptExport", $this->html->template("plots_export"        , array(), NULL, "all"));

		// build the page
		$outpath = $this->master->downloadPath.$fileName.".html";
		if(!file_put_contents($outpath, $this->html->template("prod_plot"))) 
			return false;

		// call the page
		$this->html->set("openPopup", "<script>window.open('".$outpath."');</script>");
		$this->html->set("openWhat", "plot"  );
		$this->html->set("openUrl" , $outpath);
		$this->vb->success($this->html->template("prod_popup"));
		return true;
	}

	// buildPlotSingle
	// ---------------------------------------- 
	private function buildPlotSingle($taskId) { 
		/* Constructs everything for a full plot */
		$params   = $this->getPlotParams($taskId);
		$json     = $this->buildPlotData($taskId, $params); 
		$fileName = sprintf("prodplot_s_%s_%s", str_pad($params["prodoversort"], 3, "0", STR_PAD_LEFT), $this->master->eventDateFs);
		return $this->buildPlotOutput($json, array($params), $fileName);
	}

	// getPlotParams
	// ---------------------------------------- 
	private function getPlotParams($taskId){
		/* Retrieve plot parameters information for a given task */

		$params = array();
		$prodoverview = $this->db->readTable("prodoverview", array("*"), array("id_prodoverview"=>$taskId), array(), array(), true);
		$params       = $prodoverview->slim[0];

		$raw    = $this->db->readSql(sprintf("SELECT MAX(BORWEEK)+2 AS MAXBOR FROM TABLE(%s.%s(%d)) WHERE BORDATE < (TRUNC(sysdate) + interval '1' DAY - interval '1' second)", $this->db->owner, $params["functiontype"], $taskId)); // "as" necessary to extract the proper column name, for the query it will be removed
		$params["maxbor"] = $raw[0]["maxbor"];

		$raw    = $this->db->readSql(sprintf("SELECT NVL(MINBORINT, %d-5) AS MINBOR from (SELECT MIN(BORWEEK) MINBORINT FROM TABLE(%s.%s(%d)) WHERE COUNTNO != '0' AND TASK = '%d')", $params["maxbor"], $this->db->owner, $params["functiontype"], $taskId, $taskId)); // "as" necessary to extract the proper column name, for the query it will be removed
		$params["minbor"] = $raw[0]["minbor"];

		return $params;
	}

	// loadBody
	// ---------------------------------------- 
	private function loadBody() {
		/* The part of the form where lines and links to the time development plots are displayed */
		
		// only when header has been submitted and stuff been selected
		if((!isset($this->post["filterType"]) && !isset($this->post["filterSite"])) || !isset($this->post["filterWeek"]))
			return;

		// reset buffer when regenerating
		if($this->post["do"]=="generate"){
			$this->buffer        = array();
			$this->bufferIsComb  = "";
			$this->bufferCaption = "";
			$this->bufferLabel   = "";
		}


		// build list of combined plots
		if($this->post["do"]=="generate" && $this->post["filterType"]=="combined"){
			$this->bufferCaption = sprintf("Task | Site: combined | Week: %s (%s)", $this->post["filterWeek"], (isset($this->post["filterIntegrated"]) && $this->post["filterIntegrated"]==1 ? "integrated up to " : "").$this->theDate);
			$this->bufferLabel  = "Module-Type";
			$this->bufferIsComb = "1"       ;
			$content = "combined";
			$class   = "lt";
			$popgrp  = $this->db->readTable("poplotgroup", array("id_poplotgroup", "plotgroupdesc"), array(), array(), array("id_poplotgroup"=>"asc"));
			foreach($popgrp as $row)
				array_push($this->buffer, array("number"  =>$row->id_poplotgroup,
				                                "sort"    =>$row->id_poplotgroup,
				                                "desc"    =>$row->plotgroupdesc, 
				                                "content" =>$content,
				                                "class"   =>$class,
				                                "count"   =>"-"));
		}

		// build list of single plots
		else if($this->post["do"]=="generate" && $this->post["filterType"]!="combined"){

			// the table
			$cs = new DbConfig($this->master, "table");
			$cs->table   = "prodoverview";
			$cs->columns = array("id_prodoverview", "prodoversort", "prodtaskdesc", "sitename", "moduletype", "functiontype");
			$cs->select("tasktype"    , array("N", "P", "E"));
			$cs->select("functiontype", "GET_PRODOV_STATRECS", "neq");
			$cs->order ("prodoversort");
			$cs->raw   ("NULLS FIRST", 11);
			$content = NULL;

			// filter by type
			if(!in_array($this->post["filterType"], array("", "none"))){
				$this->bufferCaption = sprintf("Task | Module-Type: %s | Week: %s (%s)", $this->post["filterType"], $this->post["filterWeek"], (isset($this->post["filterIntegrated"]) && $this->post["filterIntegrated"]==1 ? "integrated up to ": "").$this->theDate);
				$this->bufferLabel   = "Site";
				$cs->select("moduletype", $this->post["filterType"]);
				$content = "sitename";
				$class   = "lt";
			}
			else if(!in_array($this->post["filterSite"], array("", "none"))){
				$this->bufferCaption = sprintf("Task | Site: %s | Week: %s (%s)", $this->post["filterSite"], $this->post["filterWeek"], (isset($this->post["filterIntegrated"]) && $this->post["filterIntegrated"]==1 ? "integrated up to " : "").$this->theDate);
				$this->bufferLabel = "Module-Type";
				$cs->select("sitename", $this->post["filterSite"]);
				$content = "moduletype";
				$class   = "center";
			}

			// sanity check
			if(empty($content)) return;

			// the table content
			$this->bufferIsComb = "0"; // needs to be string
			$this->db->read("prodoverview", $cs, true);
			$prodoverview = $this->db->prodoverview;
			$rows = array();
			foreach($prodoverview as $row){

				$raw   = $this->db->readSql(sprintf("SELECT BORWEEK, COUNTNO FROM TABLE(%s.%s (task=>%s, week=>%s))", $this->db->owner, $row->functiontype, $row->id_prodoverview, $this->post["filterWeek"]));
				$count = $raw[0]["countno"];

				if(!isset($this->post["filterIntegrated"]) || $this->post["filterIntegrated"]!=1){
					//$raw2 = $this->db->readSql(sprintf("SELECT BORWEEK, COUNTNO FROM TABLE(%s.%s (task=>%s, week=>%s))", $this->db->owner, $row->functiontype, $row->id_prodoverview, addDays($this->theDate, -1, "Y-m-d")));
					$raw2 = $this->db->readSql(sprintf("SELECT BORWEEK, COUNTNO FROM TABLE(%s.%s (task=>%s, week=>%s))", $this->db->owner, $row->functiontype, $row->id_prodoverview, $this->post["filterWeek"]-1));
					$count = $raw[0]["countno"] - $raw2[0]["countno"];
				}

				$value = $content=="moduletype" ? $row->moduletype : $row->sitename;
				array_push($this->buffer, array("number"  =>$row->id_prodoverview,
				                                "sort"    =>str_pad($row->prodoversort, 3, "0", STR_PAD_LEFT),
				                                "desc"    =>$row->prodtaskdesc, 
				                                "content" =>$value,
				                                "class"   =>$class,
				                                "count"   =>strval($count)));
			}
		}

		// the content ------------
		$rows  = array();
		$brows = array();
		foreach($this->buffer as $row){
			array_push($rows , $this->html->template("prod_body_row", $row));
			$brow = array();
			foreach($row as $k=>$v)
				array_push($brow, sprintf("%s:%s", $k, $v));
			array_push($brows, implode("#", $brow));
		}

		// the template -----------
		$this->html->set("bufferIsComb" , $this->bufferIsComb );
		$this->html->set("bufferCaption", $this->bufferCaption);
		$this->html->set("bufferLabel"  , $this->bufferLabel  );
		$this->html->set("buffer"       , implode("%", $brows));
		$this->html->set("rows"         , implode("" , $rows));
		$this->html->set("body"         , $this->html->template("prod_body"));	
	}

	// loadBuffer
	// ---------------------------------------- 
	private function loadBuffer(){
		/* loads info from buffer */
		$this->buffer        = array();
		$this->bufferIsComb  = "";
		$this->bufferCaption = "";
		$this->bufferLabel   = "";
		if(!isset($this->post["buffer"])) return;
		$rows = explode("%", $this->post["buffer"]);
		foreach($rows as $row) {
			$rr = explode("#", $row);
			$r  = array();
			foreach($rr as $elm){
				$e = explode(":", $elm);
				$r[$e[0]]=$e[1];
			}
			array_push($this->buffer, $r);
		}
		$this->bufferIsComb  = $this->post["bufferIsComb" ];
		$this->bufferCaption = $this->post["bufferCaption"];
		$this->bufferLabel   = $this->post["bufferLabel"  ];
	}

	// loadHeader
	// ---------------------------------------- 
	private function loadHeader() {
		/* The part of the form where you can specify the parameters you request */

		$this->theDate = NULL;

		// integrated field
		if(isset($this->post["filterIntegrated"]) && $this->post["filterIntegrated"]==1)
			$this->html->set("checked_filterIntegrated", "checked");

		// Module Types
		$types = $this->db->readTable("prodoverview", array("distinct moduletype"), array(), array(), array("moduletype"=>"asc"));
		$opts  = array("none"=>"none", "combined"=>"combined");
		foreach($types as $row) $opts[$row->moduletype] = $row->moduletype;
		$default = isset($this->post["filterType"]) && $this->post["filterType"]!="none" ? $this->post["filterType"] : "none"   ;
		$class   = isset($this->post["filterType"]) && $this->post["filterType"]!="none" ? "emphasized"              : "general";
		$this->html->set("options_filterTypes", $this->html->makeOptions($opts, $default, array("none")));
		$this->html->set("classTypes"         , $class);

		// Sites
		$sites = $this->db->readTable("prodoverview", array("distinct sitename"), array(), array(), array("sitename"=>"asc"));
		$opts  = array("none"=>"none");
		foreach($sites as $row) $opts[$row->sitename] = $row->sitename;
		$default = isset($this->post["filterSite"]) && $this->post["filterSite"]!="none" ? $this->post["filterSite"] : "none"   ;
		$class   = isset($this->post["filterSite"]) && $this->post["filterSite"]!="none" ? "emphasized"              : "general";
		$this->html->set("options_filterSites", $this->html->makeOptions($opts, $default, array("none")));
		$this->html->set("classSites"         , $class);

		// Default Prod Date
		$default = -1;
		$dates   = $this->db->readTable("proddates", array("id_proddates"), array("label"=>ltrim(date("W / Y"), "0")));
		$default = $dates->id_proddates +1; // next week
		//if(array_key_exists("filterType", $this->post) && $this->post["filterType"]!="combined"){
		//	$dates   = $this->db->readTable("proddates", array("id_proddates"), array("label"=>ltrim(date("W / Y"), "0")));
		//	$default = $dates->id_proddates +1;
		//}

		// Weeks
		$weeks = $this->db->readTable("proddates", array("distinct proddate", "id_proddates"), array(), array(), array("proddate"=>"asc"));
		$opts  = array(0=>"none");
		foreach($weeks as $row) $opts[$row->id_proddates] = timestamp(1, $row->proddate);
		if(isset($this->post["filterWeek"]) && $this->post["filterWeek"]>0) $this->theDate = $opts[$this->post["filterWeek"]];
		$default = isset($this->post["filterWeek"]) && $this->post["filterWeek"]>=0 ? $this->post["filterWeek"] : $default;
		$class   = isset($this->post["filterWeek"]) && $this->post["filterWeek"]>=0 ? "emphasized"              : "general";
		$this->html->set("options_filterWeeks", $this->html->makeOptions($opts, $default, array(0)));
		$this->html->set("classWeeks"         , $class);

		// Hint Table
		$prod = $this->db->readView("hinttable", array("po.statuses_des = s.id_statuses"), array("distinct po.prodtaskdesc", "s.statusname"),
		                                         array("po.statuses_des"=>"not null"), array(), array("po.prodtaskdesc"=>"asc"));
		$rows = array();
		foreach($prod as $row) 
			array_push($rows, $this->html->template("prod_header_hintrow", array("taskdesc"=>$row->po_prodtaskdesc, "statusname"=>$row->s_statusname)));
		$this->html->set("hintRows", implode("", $rows));

		## the template --------------
		$this->html->set("header", $this->html->template("prod_header"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Generates the form and fills it with the requested information */
		//$time_start = microtime(true); 
		$this->loadHeader();
		$this->loadBody  ();
		//$time_end = microtime(true); 
		//print "Loading page (s): ".($time_end-$time_start)."<br/>";
		return $this->html->template("prod");
	}

	// submitList
	// ---------------------------------------- 
	private function submitList($comb=flase){
		/* Assembles the list for the requested plot and opens it */
		$plotId = $this->post["showList"];
		if(empty($plotId)) return false;
		return $comb ? $this->buildListComb($plotId) : $this->buildListSingle($plotId);
	}

	// submitPlot
	// ---------------------------------------- 
	private function submitPlot($comb=false){
		/* Assembles the requested plot and opens the corresponding popup */
		$plotId = $this->post["showPlot"];
		if(empty($plotId)) return false;
		return $comb ? $this->buildPlotComb($plotId) : $this->buildPlotSingle($plotId);
	}

}

$page = new ProdPage($this, "prod");

?>
