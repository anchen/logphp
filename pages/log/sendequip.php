<?php

require_once "library/page.php";
require_once "pages/all/all.php";
require_once "pages/all/ranges.php";


// SendEquipPage
// ============================================
class SendEquipPage extends Page {


	// public members and methods
	// ======================================== 

	// members
	// ---------------------------------------- 
	public $items  = array();
	public $faulty = 0;

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		$this->items = array();
		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit(){
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		rangesLoadItems($this);
		$res = false;
		if     ($this->post["do"]=="add"      ) rangesSubmitAddMode0($this);
		else if($this->post["do"]=="addShipId") $this->submitAddShipId();
		else if($this->post["do"]=="go"       ) $res = rangesSubmitGo($this);
		else if($this->post["do"]=="clr"      ) $this->submitClear();
		else if($this->post["do"]=="send"     ) $res = $this->submitSend();
	
		if($res) $this->db->commit();
		else     $this->db->undo  ();

		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// loadPage
	// ---------------------------------------- 
	private function loadPage(){
		/* Building the form */

		// additional input for sendequip
		$this->html->set("addheader", $this->html->template("ranges_header_addship", array(), NULL, "all"));

		// the entire ranges part
		$this->firstSite = "";
		rangesLoadPage($this, true, false, "Access Type: Send Equipment", true);

		// site from options
		$locOptsFr = $this->master->getOptionsSites("priorshipflag");
		$locOptsTo = $this->master->getOptionsSites("duringreceiveflag");
		$fsk       = array_search($this->firstSite, $locOptsFr);
		$defsite   = $fsk ? $fsk : $this->master->getUserSite();
		$disable   = array();
		if($fsk>0) { $disable = array_keys($locOptsFr); unset($disable[array_search($fsk, array_keys($locOptsFr))]); } // disable all but the first site if we have one
		$this->html->set("options_sitefrom", $this->html->makeOptions($locOptsFr, $defsite, $disable));
		// FIXME: this check not implemented: if(!($username == "tatsuya" || $username == "fyamane" || $username == "mdelgaud")) => disable option

		$selected = !empty($this->post["siteTo"]) ? $this->post["siteTo"] : 0;
		$disable  = array();
		if($fsk>0) array_push($disable, $fsk);
		$this->html->set("options_siteto", $this->html->makeOptions($locOptsTo, $selected, $disable));

		$this->html->set("dateFrom", array_key_exists("dateFrom", $this->post) ? $this->post["dateFrom"] : $this->master->eventDateHrS);
		$this->html->set("dateTo"  , array_key_exists("dateTo"  , $this->post) ? $this->post["dateTo"  ] : "");
		$this->html->set("shipProv", array_key_exists("shipProv", $this->post) ? $this->post["shipProv"] : "");
		$this->html->set("shipPers", array_key_exists("shipPers", $this->post) ? $this->post["shipPers"] : "");
		$this->html->set("shipRef" , array_key_exists("shipRef" , $this->post) ? $this->post["shipRef" ] : "");
		$this->html->set("shipCom" , array_key_exists("shipCom" , $this->post) ? $this->post["shipCom" ] : "");

		return $this->html->template("sendequip");
	}


	// submitAddShipId
	// ---------------------------------------- 
	private function submitAddShipId(){
		/* Loading the equipment from a past shipping Id to the table */

		if(empty($this->post["shipId"])) return;

		$sh = $this->db->readTable("shippinghistory", array("eqentryid"), array("shipmentintid"=>$this->post["shipId"]));
		if($sh->count()==0) return;

		$eqids = array();
		foreach($sh as $row) array_push($eqids, $row->eqentryid);
		rangesSubmitAddMode0($this, false, $eqids);
	}

	// submitClear
	// ---------------------------------------- 
	private function submitClear(){
		/* Clears the entire table */
		$this->items = array();
	}

	// submitSend
	// ---------------------------------------- 
	private function submitSend(){
		/* Sending the equipment, saving the form */

		// check dates
		$eventDate     = isValidDate($this->post["dateFrom"]) ? timestamp(3, $this->post["dateFrom"]) : $this->master->eventDate;
		$eventDateDb   = dbStringDate($eventDate);
		$dateTo        = isValidDate($this->post["dateTo"  ]) ? timestamp(3, $this->post["dateTo"  ]) : "";
		$dateToDb      = dbStringDate($dateTo);
		if(!empty($dateTo) && isEarlier($dateTo, $eventDate)){
			$this->vb->error("Expected return date is prior to the shipping date!");
			return false;
		} 

		// collect data
		//$sendIdent = md5($this->post["items"]);
		$this->db->shippinghistory->lock(1);

		$shiphist = $this->db->readTable("shippinghistory", array("maxid"), array(), array("max(shipmentintid)"=>"maxid"));
		$lastId   = $shiphist->maxid;
		$nextId   = $lastId + 1;

		$statIdTransit = array_search("in transit", $this->master->getOptionsStatus());
		$locIdTransit  = array_search("in transit", $this->master->getOptionsSites ());

		// EqIds
		$eqids   = array();
		foreach($this->items as $idx=>$range){
			if(!in_array($idx+1, $this->post["subRanges"])) continue;
			foreach($range as $item)
				array_push($eqids, $item->eqid);
		}

		// Shippinghistory and status
		$shipIds = array();
		foreach($eqids as $eqid){
			$this->db->shippinghistory->append(array("eqentryid"          =>$eqid, 
			                                         "shippingfrom"       =>$this->post["siteFrom"],
			                                         "shippingdestination"=>$this->post["siteTo"  ],
			                                         "shippingperson"     =>$this->post["shipPers"],
			                                         "shippingcompany"    =>$this->post["shipProv"],
			                                         "shippingdate"       =>$eventDateDb           ,
			                                         "expreturndate"      =>$dateToDb              ,
			                                         "leavingshipref"     =>$this->post["shipRef" ],
			                                         "shipmentintid"      =>$nextId                ,
			                                         "websiteusercr"      =>$this->globals["username"],
			                                         "websiteusered"      =>$this->globals["username"]));

			$this->db->statuslocation->append(array("eqentryid"          =>$eqid, 
			                                        "statusid"           =>$statIdTransit,
			                                        "majorlocid"         =>$locIdTransit,
			                                        "websiteusercr"      =>$this->globals["username"],
			                                        "websiteusered"      =>$this->globals["username"],
			                                        "eventdate"          =>$eventDateDb));
          
			$this->db->push();
			$shipIds[$eqid] = $this->db->shippinghistory->id_shippinghistory;
			if($this->db->error()){
				$this->vb->error("Shipment could not be registered!", true);
				return false;
			}
		}

		// Comment
		$typeId = array_search("shipping", $this->master->doctypes);
		if(!addComment($this, $this->post["shipCom"], $typeId, $this->post["dateFrom"], $eqids, $shipIds, "shipping", true, false)) 
			return false;

		// Paper
		if(!uploadDoc($this, "newPaper", $typeId, $this->post["dateFrom"], $eqids, $shipIds, "shipping", true, false))
			return false; 

		// All good: commit and clear buffer
		$this->vb->success(sprintf("Equipment sucessfully shipped (shipping Id = %d)!", $nextId));
		$this->items = array();
		return true;
	}
}

$page = new SendEquipPage($this, "sendequip");


?>
