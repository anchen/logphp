<?php

require_once "library/functions.php";


// parentingCheckDates
// -------------------------------------------- 
function parentingCheckDates($page, $evDate, $eqid, $name){
	/* Checks if prospective parenting date is beforce registration;
	* called by checkParenting */

	$regdate  = timestamp(0, $page->db->readTable("equipment"     , array("eventdate"), array("id_equipment"=>$eqid))->eventdate);
	$statdate = timestamp(0, $page->db->readTable("statuslocation", array("eventdate"), array("eqentryid"   =>$eqid, "isvalidflag"=>"T"), array(), array("eventdate"=>"desc"))->eventdate);
	if($evDate<min($regdate, $statdate)){
		$page->vb->error(sprintf("Eventdate is before the registration date for %s (EQID = %d), cannot be parented!", $name, $eqid)); 
		return false;
	}
	return true;
}


// parentingCheckIsGroupAndStatus
// -------------------------------------------- 
function parentingCheckIsGroupAndStatus($page, $eqid, $name, $omitStatus=false){
	/* checks if an equipment is batch, group, or if status
	* is incompatible with parenting; called by checkParenting */

	// is batch or group
	$isGroup   = false;
	$batchFlag = ($page->db->readView("batchflag", array("et.id_equipmenttypes = e.eqtypecodeid"), 
	                                               array("et.isbatchflag"), array("e.id_equipment"=>$eqid))->et_isbatchflag=="T");
	if(!$batchFlag)
		$isGroup = !empty($page->db->readTable("equipment", array("quantity"), 
	                                                        array("id_equipment"=>$eqid))->quantity);
	if($isGroup){
		$page->vb->error(sprintf("The equipment %s (EQID = %s) is a group, cannot be parented!", $name, $eqid));
		return false;
	}

	// status and location
	if($omitStatus) return true;
	$statattr        = $page->master->getEqStatus  ($eqid);
	$locattr         = $page->master->getEqLocation($eqid);
	$statId          = $statattr->sl_statusid;
	$locId           = $locattr ->sl_majorlocid;
	$allowedStat     = array_keys($page->master->getOptionsStatus("priorparentingflag"));
	$allowedLoc      = array_keys($page->master->getOptionsSites ("priorparentingflag"));
	if(!in_array($statId, $allowedStat) || !in_array($locId, $allowedLoc)){
		$page->vb->error(sprintf("The current status and/or location of equipment %s (EQID = %s) forbids parenting!", $name, $eqid));
		return false;
	}
	return true;
}


// parentingCheck
// ---------------------------------------- 
function parentingCheck($page, $container){
	/* Performs all checks deciding if parenting for page combination 
	* is actually allowed or not; called by submitParenting */

	// event date
	$eventDate   = isValidDate($container["parEventDate"]) ? $container["parEventDate"] : $page->master->eventDate;
	$eventDateDb = dbStringDate($eventDate);

	// check parenting date to be in the past or today
	if(isFuture($eventDate)){
		$page->vb->error("Please specify a parenting date that is not in the future!");
		return false;
	}

	// check if parent and child exist
	$has = ($page->db->readTable("equipment", array("id_equipment"), array("id_equipment"=>$container["idChild"]))->count()==1);
	if(!$has){
		$page->vb->error(sprintf("Child (EQID = %s) does not exist, entries cannot be parented!", $container["idChild"]));
		return false;
	}
	$has = ($page->db->readTable("equipment", array("id_equipment"), array("id_equipment"=>$container["idParent"]))->count()==1);
	if(!$has){
		$page->vb->error(sprintf("Parent (EQID = %s) does not exist, entries cannot be parented!", $container["idParent"]));
		return false;
	}

	// check if parent or child are group
	if(!parentingCheckIsGroupAndStatus($page, $container["idChild" ], "child"       )) return false;
	if(!parentingCheckIsGroupAndStatus($page, $container["idParent"], "parent", true)) return false;

	// check if registration date is after current (=parenting) date
	if(!parentingCheckDates($page, $eventDate, $container["idChild" ], "child" )) return false;
	if(!parentingCheckDates($page, $eventDate, $container["idParent"], "parent")) return false;

	// check number of links per child
	if(!parentingCheckLimit($page, $container))
		return false;

	return true;
}


// parentingCheckLimit
// -------------------------------------------- 
function parentingCheckLimit($page, $container){
	/* check the attempted number of links for the child */

	// check if parenting between these equipments actually is allowed
	$ca1 = new DbConfig($page->master, "allowed");
	$ca1->column = "ap.expectedno";
	$ca1->joinon = "ap.eqtypecode    = et1.eqtypecode";
	$ca1->joinon = "ap.allowedparent = et2.eqtypecode";
	$ca1->joinon = "et1.id_equipmenttypes = e1.eqtypecodeid";
	$ca1->joinon = "et2.id_equipmenttypes = e2.eqtypecodeid";
	$ca1->select("e1.id_equipment", $container["idChild" ]);
	$ca1->select("e2.id_equipment", $container["idParent"]);
	$page->db->read("allowed", $ca1);

	if($page->db->allowed->count()==0) {
		$page->vb->error(sprintf("Parenting is not allowed between child (EQID = %s) and parent (EQID = %s)", $container["idChild"], $container["idParent"]));
		return false;
	}

	// check if parent is full already
	$maxParentLinks = $page->db->allowed->ap_expectedno;
	$eqtypecodeid   = $page->db->readTable("equipment", array("eqtypecodeid"), array("id_equipment"=>$container["idChild"]))->eqtypecodeid;
	$view           = $page->db->readView("eqentryids", array("et.id_equipmenttypes = e.eqtypecodeid"), 
	                                                    array("e.id_equipment"), array("et.id_equipmenttypes"=>$eqtypecodeid));
	$eqentryids = array();
	foreach($view as $row) array_push($eqentryids, $row->e_id_equipment);
	$parLinks   = $page->db->readTable("parenting", array("sumlinks"), 
	                                                array("parenteqentryid"=>$container["idParent"], 
	                                                      "eqentryid"      =>$eqentryids, 
	                                                      "isactiveflag"   =>"T"), 
	                                                array("sum(numberoflinks)"=>"sumlinks"))->sumlinks;
	$fullParent = ($maxParentLinks>0 && (($parLinks + $container["numberOfLinks"]) > $maxParentLinks));
	if($fullParent && $maxParentLinks>0){
		$page->vb->error(sprintf("Reached maximum number of equipment pieces (%d/%d) for the parent (EQID = %d). Cannot do another %d parenting(s)!", $parLinks, $maxParentLinks, $container["idParent"], $container["numberOfLinks"]));
		return false;
	}

	// additional check for item
	$batchFlag = ($page->db->readView("batchflag", array("et.id_equipmenttypes = e.eqtypecodeid"), 
	                                               array("et.isbatchflag"), array("e.id_equipment"=>$container["idChild"]))->et_isbatchflag=="T");

	// note page selection will be useful later for doParenting
	$c = new DbConfig($page->master, "numoflinks");
	$c->column = "sumlinks";
	$c->reformat("sum(numberoflinks)", "sumlinks");
	$c->select("eqentryid"      , $container["idChild"]);
	$c->select("parenteqentryid", $page->master->config->eqIdOrph->value, "neq");
	$c->select("isactiveflag"   , "T");
	$parenting = $page->db->parenting;
	$parenting->read($c, true);
	$nEntries = $parenting->count();

	if(!$batchFlag) {
		$doSingleInsert = (($nEntries==0 || $parenting->sumlinks==0) && $maxParentLinks==0);
		if(!$doSingleInsert && $maxParentLinks==0){
//print "singleInsert: ".$doSingleInsert.", maxlinks: ".$maxParentLinks."<br />";
			$page->vb->error(sprintf("The item (EQID = %d) can only be parented once to parent (EQID = %d)!", $container["idChild"], $container["idParent"]));
			return false;
		}
	}

	// compute number of links
	$numberOfLinksDone = $parenting->sumlinks + (array_key_exists("prevLinks", $container) ? $container["prevLinks"] : 0); // when parenting in a loop

	if($batchFlag){
		$numberOfPieces = $page->db->readTable("equipment", array("quantity"), array("id_equipment"=>$container["idChild"]))->quantity;
	}
	else {
		$eqTypeCodeId   = $page->db->readTable("equipment", array("eqtypecodeid"),
		                                                    array("id_equipment"=>$container["idChild"]))->eqtypecodeid;
		$numberOfPieces = $page->db->readTable("equipmenttypes", array("cutpieces"),
		                                                    array("id_equipmenttypes"=>$eqTypeCodeId))->cutpieces;
		$numberOfPieces = !empty($numberOfPieces) ? $numberOfPieces : 1;
	}

	if(($numberOfLinksDone + $container["numberOfLinks"]) > $numberOfPieces){
		$page->vb->error(sprintf("Reached maximum number of equipment pieces (%d/%d) for the child (EQID = %d). Cannot do another %d parenting(s)!", $numberOfLinksDone, $numberOfPieces, $container["idChild"], $container["numberOfLinks"]));
		return false;
	}
	$page->reachedLimit = (($numberOfPieces - ($numberOfLinksDone + $container["numberOfLinks"])) == 0);
	return true;

}


// parentingDelete
// -------------------------------------------- 
function parentingDelete($page, $childId, $parentId){
	/* Removes a parenting connection for a given child and parent;
	* note we only remove a single parenting connection; multiple 
	* parentings must be deleted by separate invocations of this function
	* (also to get the date right) */


	// retrieve all parents for this child
	$parents = $page->db->readTable("parenting", array("id_parenting", "parenteqentryid", "isactiveflag", "eventdatedb"), 
	                                             array("eqentryid"=>$childId, "isactiveflag"=>"T"),
	                                             array("TO_CHAR(EVENTDATE, 'YYYY-MM-DD HH24:MI:SS')"=>"eventdatedb"));
	if($parents->count()==0) return false;
	$others = array();
	$theRow = Null;
	foreach($parents as $par) {
		if($par->parenteqentryid==$parentId) { $theRow=$par; continue; } 
		$others[$par->parenteqentryid] = $par->id_parenting;
	}

	if(empty($theRow)) return false;

	// find and remove the corresponding status
	// actually: if "assembled"+"(see parent)" exists => delete this one, independent of the date
	//           else => delete the "partially assembled" that matches the date up to 2 seconds 
	$statIdAssembPart = array_search("partially assembled", $page->master->getOptionsStatus());
	$statIdAssemb     = array_search("assembled"          , $page->master->getOptionsStatus());
	$locIdSeeParent   = array_search("(see parent)"       , $page->master->getOptionsSites ());
	$sl = $page->db->readTable("statuslocation", array("id_statuslocation", "statusid", "majorlocid", "isvalidflag"), 
	                                             array("eqentryid"=>$childId, "isvalidflag"=>"T"),
	                                             array("TO_CHAR(EVENTDATE, 'YYYY-MM-DD HH24:MI:SS')"=>"eventdatedb"));
	$rows    = array();
	$idxFull = -1; // full assembled status
	$idxPart = -1; // matched partially assembled status
	foreach($sl as $row){
		if(!($row->statusid == $statIdAssembPart) && !($row->statusid == $statIdAssemb && $row->majorlocid == $locIdSeeParent)) continue;
		array_push($rows, $row);
		if($row->statusid == $statIdAssemb && $row->majorlocid == $locIdSeeParent) 
			$idxFull = count($rows)-1; // remember status assembled
		else if($idxPart<0 && isAlmost($row->eventdatedb, $theRow->eventdatedb)) // up to 2 seconds difference
			$idxPart = count($rows)-1; // remember status partially assembled
	}
	if($idxFull<0 && $idxPart<0) return;
	$sRow = $rows[$idxFull>=0 ? $idxFull : $idxPart];
	$sRow->isvalidflag = "F"; // only remove one status since we only remove one parenting
	$sRow->push();
	$err = $page->db->error();
	if(!empty($err)) return false;

	// remove the parenting
	$theRow->isactiveflag = "F";
	$theRow->push();
	$err = $page->db->error();
	if(!empty($err)) return false;

	// check number of remaining => reactivate orphanage again if none remain
	if(count($others)==0) {
		$cp = new DbConfig($page->master, "orph");
		$cp->select("eqentryid"      , $childId                              );
		$cp->select("parenteqentryid", $page->master->config->eqIdOrph->value);
		$page->db->parenting->update(array("isactiveflag"=>"T"), $cp);
		$page->db->parenting->push();
		$err = $page->db->error();
		if(!empty($err)) return false;
	}

	return true;	
}


// parentingDoIt
// -------------------------------------------- 
function parentingDoIt($page, $container){
	/* Performs the actual parenting; called by submitParenting */

	// recompute number of entries for the child again
	if(!parentingCheckLimit($page, $container)) return false;

	// event date
	$eventDate   = isValidDate($container["parEventDate"]) ? $container["parEventDate"] : $page->master->eventDate;
	$eventDateDb = dbStringDate($eventDate);

	// equipment type
	$eqtgc = $page->db->readView("eqtgc", array("etgc.eqtypecode = et.eqtypecode",
	                                            "et.id_equipmenttypes = e.eqtypecodeid"), 
	                                      array("etgc.eqtypegroupcode"), array("e.id_equipment"=>$container["idChild"]));
	$eqtgcs = array();
	foreach($eqtgc as $row) array_push($eqtgcs, strtoupper($row->etgc_eqtypegroupcode));

	// is parent a functional position?
	$ppid  = $page->db->readView("peqt", array("et.id_equipmenttypes = e.eqtypecodeid"),
	                                     array("et.projectid"), array("e.id_equipment"=>$container["idParent"]));
	$isFunct = intval($ppid->projectid)==5;

	// do parenting entry
	$toInsert = array("eqentryid"       => $container["idChild"      ],
	                  "parenteqentryid" => $container["idParent"     ],
	                  "position"        => $container["parPosition"  ],
	                  "isactiveflag"    => $container["isActiveFlag" ],
	                  "numberoflinks"   => $container["numberOfLinks"],
	                  "websiteusercr"   => $page->globals["username"],
	                  "websiteusered"   => $page->globals["username"],
	                  "eventdate"       => $eventDateDb);
	if($isFunct)
		$toInsert["parentisabstract"] = "T";
	if(isset($container["parService"]) && in_array("CABL", $eqtgcs)) 
		$toInsert["serviceends"] = $container["parService"]=="E" ? "E" : "S";
	$page->db->parenting->append($toInsert);

	// set active flags for orphanage entries
	$parenting = $page->db->readTable("parenting", array("id_parenting", "isactiveflag"),
	                                               array("eqentryid"      =>$container["idChild"],
	                                                     "parenteqentryid"=>$page->master->config->eqIdOrph->value,
	                                                     "isactiveflag"   =>"T"));
	foreach($parenting as $row) $row->isactiveflag = "F";
	$parenting->push();


	// avoid setting status and backdating comment if parent is test or other equipment
	$eqtypecode = $page->db->readView("eqtc", array("et.id_equipmenttypes = e.eqtypecodeid"),
	                                          array("et.eqtypecode"), 
	                                          array("e.id_equipment"=>$container["idParent"]))->et_eqtypecode;
	$doStatus = (strtoupper($eqtypecode)=="TOEQ") ? false : true;


	// setting comments and status if NOT test equipment
	if($doStatus){

		// writing comments
		if(!isToday($eventDate)){
			// if the user specified a date to do the parenting in the past, not today, send a comment to the child and parent
			$com = sprintf("The user %s added a parenting link with this EQ (%s) being child and EQ (%s) as parent at %s and backdated it to %s.", $page->globals["username"], $container["idChild"], $container["idParent"], $page->master->eventDateHr, timestamp(1, $eventDate));
			$page->db->eqcomments->append(array("eqentryid"    =>$container["idChild"],
			                                    "eqcomment"    =>$com,
			                                    "typeofcomment"=>"register",
			                                    "websiteusercr"=>$page->globals["username"],
			                                    "websiteusered"=>$page->globals["username"],
			                                    "eventdate"    =>$eventDateDb));
			$page->db->eqcomments->append(array("eqentryid"    =>$container["idParent"],
			                                    "eqcomment"    =>$com,
			                                    "typeofcomment"=>"register",
			                                    "websiteusercr"=>$page->globals["username"],
			                                    "websiteusered"=>$page->globals["username"],
			                                    "eventdate"    =>$eventDateDb));
		}


		// insert status
		$statIdAssembPart = array_search("partially assembled", $page->master->getOptionsStatus());
		$statIdAssemb     = array_search("assembled"          , $page->master->getOptionsStatus());
		$locIdSeeParent   = array_search("(see parent)"       , $page->master->getOptionsSites ());
		if(!$page->reachedLimit){ // not reached limit yet => partially assembled (27)
			$page->db->statuslocation->append(array("eqentryid"    =>$container["idChild"],
			                                        "statusid"     =>$statIdAssembPart,
			                                        "websiteusercr"=>$page->globals["username"],
			                                        "websiteusered"=>$page->globals["username"],
			                                        "eventdate"    =>$eventDateDb));
		}
		else {
			$page->db->statuslocation->append(array("eqentryid"    =>$container["idChild"],
			                                        "statusid"     =>$statIdAssemb,
			                                        "majorlocid"   =>$locIdSeeParent,
			                                        "websiteusercr"=>$page->globals["username"],
			                                        "websiteusered"=>$page->globals["username"],
			                                        "eventdate"    =>$eventDateDb));
		}
	}

	$page->db->push();
	if($page->db->error()){
		$page->vb->error(sprintf("Parenting entry for child (EQID = %d) and parent (EQID = %d) could not be created!", $container["idChild"], $container["idParent"]));
		return false;
	}

	$add = !$doStatus ? "<br />(Status and comments have not been updated nor inserted since parent has EqTypeCode = 'TOEQ'.)" : "";	
	$page->vb->success(sprintf("Parenting entry for child (EQID = %d) and parent (EQID = %d) has successfully been created!".$add, $container["idChild"], $container["idParent"]));
	return true;
}

?>
