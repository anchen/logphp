<?php


// equipGetIds
// -------------------------------------------- 
function equipGetIds($page, $searchfor, $eqId, $mtfId, $subbatchId, $otherId){
	/* Obtains all other ids from the one the user has given */

	$ids       = array($eqId, $mtfId, $subbatchId, $otherId);
	$equipment = $page->db->equipment;

	if($searchfor == "mtfId") {
		$equipment = $page->db->readTable("equipment", array("id_equipment", "otherid"), 
		                                               array("partsbatchmtfid"=>$mtfId,
		                                                     "subbatchid"     =>$subbatchId));
		if($equipment->count()==1) {
			$ids[0] = $equipment->id_equipment;
			$ids[3] = $equipment->otherid;
		}
	}
	else if($searchfor == "eqId") {
		$equipment = $page->db->readTable("equipment", array("partsbatchmtfid", "subbatchid", "otherid"), 
		                                               array("id_equipment"=>$eqId));
		if($equipment->count()==1) {
			$ids[1] = $equipment->partsbatchmtfid;
			$ids[2] = $equipment->subbatchid;
			$ids[3] = $equipment->otherid;
		}
	}
	else if($searchfor == "otherId") {
		$equipment = $page->db->readTable("equipment", array("id_equipment","partsbatchmtfid", "subbatchid"), 
		                                               array("otherid"=>$otherId));
		if($equipment->count()==1) {
			$ids[0] = $equipment->id_equipment;
			$ids[1] = $equipment->partsbatchmtfid;
			$ids[2] = $equipment->subbatchid;
		}
	}
	if($equipment->count()==0) $ids = array();
	return $ids;
}


// equipLoadPage
// -------------------------------------------- 
function equipLoadPage($page, $saveName="SAVE CHANGES IN<br />INPUT FIELDS", $saveDo="submitIt('save')") {
	/* Generates the header to select the equipment */
	$page->html->set("eqId"      , $page->eqId   );
	$page->html->set("mtfId"     , $page->mtfId  );
	$page->html->set("otherId"   , $page->otherId);
	$page->html->set("newEqId"   , $page->eqId   );
	$page->html->set("newMtfId"  , $page->mtfId  );
	$page->html->set("newOtherId", $page->otherId);
	$page->html->set("saveName"  , $saveName     );
	$page->html->set("saveDo"    , $saveDo       );
	$page->html->set("eventdate" , $page->html->template("eventDate"   , array("title"=>"Event Date", "name"=>"eventDate", "addd"=>"", "addl"=>""), NULL, "all"));
	$page->html->set("header"    , $page->html->template("equip_header", array(), NULL, "all"));
}



// equipSubmitLoad
// -------------------------------------------- 
function equipSubmitLoad($page){
	/* Selection of the entry to be displayed with different options */

	$page->eqId    = NULL;
	$page->mtfId   = NULL;
	$page->otherId = NULL;

	$ids = equipGetIds($page, $page->post["load"], $page->post["newEqId"], $page->post["newMtfId"], NULL, $page->post["newOtherId"]);

	if(count($ids)==0){
		$page->vb->error("There is no equipment with the given ID!");
		return;
	}

	$page->eqId    = $ids[0];
	$page->mtfId   = $ids[1];
	$page->otherId = $ids[3];

}

?>
