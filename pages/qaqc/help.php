<?php

require_once "library/page.php";


// Help Page
// ============================================
class HelpPage extends Page {

	// load
	// ---------------------------------------- 
	public function load(){
		/* Returns the content HTML when page is invoked via the menu */
		return $this->html->template("help");
	}
}

$page = new HelpPage($this, "help");

?>
