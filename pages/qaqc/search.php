<?php

require "library/page.php";
require "pages/all/all.php";


// extendOptions
// --------------------------------------------
function extendOptions($options, $table, $key, $desc){
	/* extends an option list by a description stored in a DbTable */
	$list = array();
	foreach($table as $row)
		$list[$row->$key] = $row->$desc;
	$new = array();
	foreach($options as $key=>$val){
		$desc      = isset($list[$key]) ? sprintf("&nbsp;&nbsp;&nbsp;%s", $list[$key]) : "";
		$new[$key] = $val.$desc;
	}
	return $new;
}


// Col
// ============================================
class Col {

	// members
	// ---------------------------------------- 
	private $always           = array();
	public  $columnName       = NULL;
	public  $dbcolumnfreetext = NULL;
	public  $selectgroup      = NULL;
	public  $setasdefault     = NULL;
	public  $show             = false;

	// __construct
	// ---------------------------------------- 
	public function __construct($row, $always, $current) {
		$this->always           = $always;
		$this->columnName       = strtolower($row->dbtablecolumn);
		$this->dbcolumnfreetext = $row->dbcolumnfreetext;
		$this->selectgroup      = $row->selectgroup     ;
		$this->setasdefault     = $row->setasdefault    ;
		$this->show             = empty($current) ? true : ($this->setasdefault=="T" || in_array($this->columnName, $always) || in_array($this->columnName, $current));
	}

	// setDefault
	// ---------------------------------------- 
	public function setDefault() {
		$this->show = $this->setasdefault=="T" || in_array($this->column_name, $always);
	}
}


// Search Page
// ============================================
class SearchPage extends Page {


	// public members and methods
	// ======================================== 
	
	// members
	// ---------------------------------------- 
	private $always  = array();
	private $cols    = array();
	private $selects = array();
	private $setting = NULL;
	private $view    = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($master, $name) {
		parent::__construct($master, $name);
		$this->db->dropdownview       ->owner = $this->master->qaqcOwnerContexts;
		//$this->db->eqtypegroups       ->owner = $this->master->qaqcOwnerContexts; // still in LOG
		//$this->db->equipment          ->owner = $this->master->qaqcOwnerContexts; // still in LOG
		//$this->db->equipmenttypes     ->owner = $this->master->qaqcOwnerContexts; // still in LOG
		$this->db->fullmeasview       ->owner = $this->master->qaqcOwnerValues;
		$this->db->fullmeasviewall    ->owner = $this->master->qaqcOwnerValues;
		$this->db->measdoclink        ->owner = $this->master->qaqcOwnerValues;
		$this->db->measdocuments      ->owner = $this->master->qaqcOwnerValues;
		//$this->db->prodsitegroups     ->owner = $this->master->qaqcOwnerContexts; // still in LOG
		$this->db->usersettingswebsite->owner = $this->master->qaqcOwnerContexts;
		$this->db->webtranslations    ->owner = $this->master->qaqcOwnerContexts;
		$this->prepare();
	}

	// load
	// ---------------------------------------- 
	public function load() {
		/* Returns the content HTML when page is invoked via the menu */

		return $this->loadPage();
	}

	// submit
	// ---------------------------------------- 
	public function submit() {
		/* Returns the content HTML when the form in the page is submitted (i.e. a button is pressed) */

		if     ($this->post["do"]=="applySettings") $this->submitApplySettings();
		else if($this->post["do"]=="loadSettings" ) $this->submitLoadSettings ();
		else if($this->post["do"]=="resetSettings") $this->submitResetSettings();
		else if($this->post["do"]=="saveSettings" ) $this->submitSaveSettings ();
		else if($this->post["do"]=="exportCsv"    ) $this->submitExport("csv");
		else if($this->post["do"]=="exportExcel"  ) $this->submitExport("xls");
		else if($this->post["do"]=="exportInvCsv" ) $this->submitExport("inv");
		else if($this->post["do"]=="download"     ) $this->submitDownload();
		
		return $this->loadPage();
	}



	// private members and methods
	// ======================================== 

	// loadBody
	// ---------------------------------------- 
	private function loadBody() {
		/* Builds and returns the body */

		// only when the search button is pressed
		if(empty($this->post["do"]) || in_array($this->post["do"], array("reset"))) return;
		if(!in_array($this->post["do"], array("search", "changePage", "refresh"))) return;

		// buttons
		$this->html->set("buttons", $this->html->template("search_buttons"));

		// build the table
		$table_body = "";
		$p = array_key_exists("p", $this->globals) ? $this->globals["p"] : 0;
		$s = $this->master->config->has("pageSize") ? $this->master->config->pageSize->value : 25;

		$tableAtts = $this->loadData();
		$this->html->set("table_head", $tableAtts[0]);
		$this->html->set("table_body", $tableAtts[1]);

		// the template
		$this->html->set("body"      , $this->html->template("search_body"));
	}		

	// loadData
	// ---------------------------------------- 
	private function loadData($noPageSel=false, $exclude=array(), $useRaw=false) {
		/* Retrieves and assembles the actual data */

		// list of columns to exclude always
		$exclude = array_merge(array("md_doccontent"), $exclude);

		// prepare the selection
		$short       = $this->db->reformat[$this->view];
		$cs          = new DbConfig($this->master, "fetchall");
		$cs->joinon  = $short.".meassite_hash = mdl.meassitehash";
		$cs->joinon  = "mdl.dochash = md.measdochash";
		$cs->columns = array($short.".*", "md.id_measdocuments", "md.docname"); 
		$cs->slim    = true;

		// add search criteria
		foreach($this->selects as $key=>$val){
			if(empty($this->post[$val])) continue;
			if($val=="eqtypegroup") $key = "EQTYPEGROUP_NAME"; // yes, unfortunately
			$cs->select($short.".".$key, $this->post[$val]);
		}
		if(!isset($this->post["showDiscarded"]) || $this->post["showDiscarded"]!=1)
			$cs->select($short.".isvalid_flag", "T");

		// equipment id
		$eqId = NULL;
		if(!empty($this->post["eqId"]) || !empty($this->post["mtfId"]) || !empty($this->post["otherId"]))
			$eqId = !empty($this->post["eqId"]) ? $this->post["eqId"] : $this->master->getEqId($this->post["mtfId"], $this->post["otherId"]);

		if(!empty($eqId)){
			if(isset($this->post["showConnected"]) && $this->post["showConnected"]==1){
				$sub = new DbConfig($this->master, "sub");
				$sub->column = "eqentryid";
				$sub->raw("start with parenteqentryid='".$eqId."' connect by parentbatcheqentryid = prior eqentryid");
				$this->db->parenting->read($sub, true);
				$eqids = array($extEqId);
				foreach($this->db->parenting as $row) 
					array_push($eqids, $row->eqentryid);
				$eqids = array_filter(array_unique($eqids));
				if(count($eqids)>0) $cs->select("eq_id", $eqids);
			}
			$cs->select("eq_id", $eqId);
		}

		// timestamps
		if(!empty($this->post["msTmpFrom"]) && !empty($this->post["msTmpTo"]))
			$cs->select("measurement_time", array(array(dbStringDate(timestamp($this->post["msTmpFrom"])),
			                                            dbStringDate(timestamp($this->post["msTmpTo"  ])))), "inrange");
		else if(!empty($this->post["msTmpFrom"]))
			$cs->select("measurement_time", dbStringDate(timestamp($this->post["msTmpFrom"])), "aboveeq");
		else if(!empty($this->post["msTmpTo"]))
			$cs->select("measurement_time", dbStringDate(timestamp($this->post["msTmpTo"  ])), "beloweq");

		// measurement values
		if((!empty($this->post["msValFrom"]) && !preg_match("/[[:alpha:]]/", $this->post["msValFrom"])) &&
		   (!empty($this->post["msValTo"  ]) && !preg_match("/[[:alpha:]]/", $this->post["msValTo"  ]))){
			$cs->select("measurement_value", $this->post["msValFrom"], "aboveeq");
			$cs->select("measurement_value", $this->post["msValTo"  ], "beloweq");
		}
		else if(!empty($this->post["msValFrom"]) && preg_match("/[[:alpha:]]/", $this->post["msValFrom"]))
			$cs->select("measurement_value", $this->post["msValFrom"]);
		else if(!empty($this->post["msValTo"  ]) && preg_match("/[[:alpha:]]/", $this->post["msValTo"  ]))
			$cs->select("measurement_value", $this->post["msValTo"  ]);

		// custom sorting
		if(isset($this->post["orderBy"]) && isset($this->post["orderType"]) && !empty($this->post["orderBy"]) && !empty($this->post["orderType"])){
			if($this->post["orderBy"]!="none" && $this->post["orderType"]!="none")
				$cs->order($this->post["orderBy"], $this->post["orderType"]);
			if($this->post["orderType"]=="none"){ 
				$this->post["orderBy"  ]="none";
				$this->post["orderType"]="asc";
			}
		}

		// the page selector and the limit attributes to the config
		$start = 0;
		if(!$noPageSel) $start = pageSelector($this, $cs);

		// select the data
		$this->db->read("bigview", $cs);
		$view = $this->db->bigview;

		// build table header
		$oidx    = 0;
		$colraws = array("none");
		$columns = array("No.");
		$cols    = array();
		$colsuse = array();
		foreach($this->cols as $col) $cols[$col->columnName] = $col->show;
		foreach($view->columns() as $colname){

			$col = substr($colname, strpos($colname, "_")+1);
			if(in_array($col, array("idx", "userpasswd", "id_measdocuments", "docname"))) continue;
			if(array_key_exists($col, $cols) && !$cols[$col]) continue;

			$any = findAny($exclude, $col);
			if(!empty($any)) continue;

			$webtr = $this->db->readTable("webtranslations", array("dbcolumnfreetext"), array("dbtablename"     =>strtoupper($this->view),
			                                                                                  "dbtablecolumn"   =>strtoupper($col),
			                                                                                  "dbcolumnfreetext"=>"not null"));
			$use = $col;
			if(!$useRaw && $webtr->count()!=0) $use = $webtr->dbcolumnfreetext;

			array_push($columns, htmlentities($use, ENT_QUOTES));
			array_push($colsuse, $colname);
			array_push($colraws, replaceFirst("_", ".", $colname));
		}
		if(isset($this->post["orderBy"])) $oidx = array_search($this->post["orderBy"], $colraws);
		$type = isset($this->post["orderType"]) ? $this->post["orderType"] : "asc";
		$cols = array();
		$i    = 0;
		foreach($columns as $col){
			$add = $oidx==$i ? ($type=="asc" ? "tablesorterThDesc" : "tablesorterThAsc") : "";
			array_push($cols, sprintf("<th class=\"%s\" onclick=\"toggleSort('%s');submitIt('resort');\"><div><b>%s</b></div></th>", $add, $colraws[$i], $col));
			++$i;
		}
		$table_head = implode("",$cols);


		// build table body
		$rows = array();
		$i    = 1;
		foreach($view->slim as $row){
			$theRow = array("<td>".($start+$i)."</td>");
			foreach(array_keys($row) as $colname){
				if(!in_array($colname, $colsuse)) continue;
				$item = $row[$colname];
				$colname = substr($colname, strpos($colname, "_")+1);
				if(in_array($colname, array("mtfid", "partsbatchmtfid", "mtf_id"))){
					$pre = substr($item, 0, 5);
					$typ = "<span style=\"color:#990033; font-size:130%;\">".substr($item, 5, 4)."</span>";
					$aft = substr($item, 9, 5);
					$item = "<b>".$pre."&nbsp;".$typ."&nbsp;".$aft."</b>";
					array_push($theRow, "<td>".$item."</td>");
					continue;
				}
				if($colname=="measurement_value" && !empty($row["md_id_measdocuments"]) && !empty($item)){
					$item = $item."<br /><a href=\"#\" onclick=\"set('docId', ".$row["md_id_measdocuments"].");submitIt('download')\">" . ($row["md_docname"] != null ? htmlentities($row["md_docname"], ENT_QUOTES) : "document") . "</a>";
					array_push($theRow, "<td>".$item."</td>");
					continue;
				}
				if( $colname != "eventdate" && strpos($colname, "date")!==false  && !empty($item))
					$item = timestamp(1, $item);
				if(($colname == "eventdate" || strpos($colname, "time")!==false) && !empty($item))
					$item = timestamp(2, convertDate($item));
				if(                            strpos($colname, "_ts" )!==false  && !empty($item))
					$item = timestamp(2, convertDate($item));

				array_push($theRow, "<td>".(!empty($item) ? htmlentities($item, ENT_QUOTES) : "&nbsp;")."</td>");
			}
			array_push($rows, "<tr>".implode("", $theRow)."</tr>");
			++$i;
		}
		return array(implode("",$cols), implode("",$rows));
	}

	// loadHeader
	// ---------------------------------------- 
	private function loadHeader() {
		/* Builds and returns the header */

		// default values
		$defs = array();
		$defs["aspect"     ] = 0;
		$defs["collection" ] = 0;
		$defs["context"    ] = 0;
		$defs["eqtypegroup"] = 0;
		$defs["eqtype"     ] = 0;
		$defs["psname"     ] = 0;
		$defs["psgroup"    ] = 0;
		$defs["mtfId"      ] = "";
		$defs["eqId"       ] = 0;
		$defs["otherId"    ] = "";
		$defs["msTmpFrom"  ] = "";
		$defs["msTmpTo"    ] = "";
		$defs["msValFrom"  ] = "";
		$defs["msValTo"    ] = "";

		// reset search criteria
		if(empty($this->post["do"]) || $this->post["do"]=="reset"){
			foreach($defs as $key=>$val)
				$this->post[$key] = $val;
		}	

		// retrieve the equipment Id
		$eqId = NULL;
		if(!empty($this->post["eqId"]) || !empty($this->post["mtfId"]) || !empty($this->post["otherId"]))
			$eqId = !empty($this->post["eqId"]) ? $this->post["eqId"] : $this->master->getEqId($this->post["mtfId"], $this->post["otherId"]);

		// build the dropdown options in an intertwined big query
		$c = new DbConfig($this->master, "dropdown");
		$c->slim = true;
		foreach($this->selects as $key=>$val){
			$c->column = $key;
			if(empty($this->post[$val])) continue;
			$c->select($key, $this->post[$val]);
		}
		if(!empty($eqId)){
			$cs = new DbConfig($this->master, "subquery");
			$cs->column = "eqtypecode";
			$cs->joinon = "e.eqtypecodeid = et.id_equipmenttypes";
			$cs->select("e.id_equipment", $eqId);
			$this->db->read("eqtcs", $cs);
			$eqtcs = array();
			foreach($this->db->eqtcs as $row)
				array_push($eqtcs, $row->eqtypecode);
			$c->select("eqtype_code", $eqtcs);
		}
		$this->db->read("dropdownview", $c);
		$view = $this->db->dropdownview;

		if(count($view->slim)==0){
			$this->vb->warning("The combination of data specified in the input fields does not correspond to any data! Please change your filter criteria, you may need to use the \"Reset Fields\" button and start anew to reload the dropdowns.");
		} 

		// build the options from the result of this query
		$options = array("aspect"      => array(0 => "ALL"), 
		                 "collection"  => array(0 => "ALL"), 
		                 "context"     => array(0 => "ALL"),
		                 "eqtype"      => array(0 => "ALL"),
		                 "eqtypegroup" => array(0 => "ALL"),
		                 "psname"      => array(0 => "ALL"),
		                 "psgroup"     => array(0 => "ALL"));
		foreach($view->slim as $row){
			foreach($this->selects as $key=>$val){
				$k = strtolower($key);
				$options[$val][$row[$k]] = $row[$k];
			}
		}
		foreach($options as $key=>$val){
			$options[$key] = array_unique(array_filter($options[$key]));
			asort($options[$key]);
		}

		// extend selected options by descriptions 
		$eqtypes      = $this->db->readTable('equipmenttypes', array('eqtypecode'     , 'eqtypename'     ), array(), array(), array("eqtypecode"=>"asc"));
		$eqtypegroups = $this->db->readTable('eqtypegroups'  , array('eqtypegroupcode', 'eqtypegroupname'), array(), array(), array("eqtypegroupcode"=>"asc"));
		$psgroups     = $this->db->readTable('prodsitegroups', array('psgroupname'    , 'psgdescr'       ), array(), array(), array("psgroupname"=>"asc"));

		$options["eqtype"     ] = extendOptions($options["eqtype"     ], $eqtypes     , "eqtypecode"     , "eqtypename"     );
		$options["eqtypegroup"] = extendOptions($options["eqtypegroup"], $eqtypegroups, "eqtypegroupcode", "eqtypegroupname");
		$options["psgroup"    ] = extendOptions($options["psgroup"    ], $psgroups    , "psgroupname"    , "psgdesc"        );

		// write to template
		foreach($options as $key=>$opts){
			$this->html->set("select_".$key, $this->html->makeSelect($key, $opts, $this->post[$key], array(), array(), false, "submitIt('".$key."')", "", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"", array(0)));
		}

		// replace values		
		$this->html->set("mtfId"        , $this->post["mtfId"  ]);
		$this->html->set("eqId"         , $this->post["eqId"   ]);
		$this->html->set("otherId"      , $this->post["otherId"]);

		$this->html->set("mtfId_cls"    , $this->post["mtfId"  ]!=$defs["mtfId"  ] ? "emphasized" : "");
		$this->html->set("eqId_cls"     , $this->post["eqId"   ]!=$defs["eqId"   ] ? "emphasized" : "");
		$this->html->set("otherId_cls"  , $this->post["otherId"]!=$defs["otherId"] ? "emphasized" : "");

		$this->html->set("msTmpFrom"    , $this->post["msTmpFrom"]);
		$this->html->set("msTmpTo"      , $this->post["msTmpTo"  ]);
		$this->html->set("msValFrom"    , $this->post["msValFrom"]);
		$this->html->set("msValTo"      , $this->post["msValTo"  ]);

		$this->html->set("msTmpFrom_cls", $this->post["msTmpFrom"]!=$defs["msTmpFrom"] ? "emphasized" : "");
		$this->html->set("msTmpTo_cls"  , $this->post["msTmpTo"  ]!=$defs["msTmpTo"  ] ? "emphasized" : "");
		$this->html->set("msValFrom_cls", $this->post["msValFrom"]!=$defs["msValFrom"] ? "emphasized" : "");
		$this->html->set("msValTo_cls"  , $this->post["msValTo"  ]!=$defs["msValTo"  ] ? "emphasized" : "");

		$this->html->set("showRepeated" , isset($this->post["showRepeated" ]) && $this->post["showRepeated" ]==1 ? "checked" : "");
		$this->html->set("showDiscarded", isset($this->post["showDiscarded"]) && $this->post["showDiscarded"]==1 ? "checked" : "");
		$this->html->set("showConnected", isset($this->post["showConnected"]) && $this->post["showConnected"]==1 ? "checked" : "");

		// loadable settings
		$settings = $this->db->readTable("usersettingswebsite", array("distinct settingsname"), array("user_id" => $this->globals["userId"]));
		$options  = array("0" => "select setting");
		$add      = array("0" => "class='hidden'");
		foreach($settings as $row)
			$options[$row->settingsname] = $row->settingsname;
		$setting = !empty($this->setting) ? ($this->setting==1 ? "APPLIED USER SETTING" : $this->setting) : "DEFAULT SETTING";
		$this->html->set("setting"        , $this->setting);
		$this->html->set("settingName"    , $setting      );
		$this->html->set("select_setting" , $this->html->makeSelect("loadSetting", $options, $this->setting, array(), $add, false, "submitIt('loadSettings')", "", "class=\"buttonlike\"", "class=\"buttonlike emphasized\"", array("0")));

		// settings table
		$settings     = array();
		$descriptions = array();
		foreach($this->cols as $col){
			$vars = array("name"     => "setting_".$col->columnName, 
			              "checked"  => $col->show ? "checked" : "",
			              "disabled" => in_array($col->columnName, $this->always) ? "disabled" : "",
			              "label"    => $col->dbcolumnfreetext);
			if     ($col->selectgroup=="SETTINGS")
				array_push($settings    , $this->html->template("search_header_settings_checkbox", $vars));   
			else if($col->selectgroup=="DESCRIPTIONS")
				array_push($descriptions, $this->html->template("search_header_settings_checkbox", $vars));   
		}
		$this->html->set("settingsSett", implode("", $settings    ));
		$this->html->set("settingsDesc", implode("", $descriptions));
		$this->html->set("settingsAdd" , "<span style='padding-left:29px;'>coming soon</span>"); // FIXME

		// the template
		$this->html->set("header", $this->html->template("search_header"));
	}

	// loadPage
	// ---------------------------------------- 
	private function loadPage() {
		/* Builds and returns the page */

		$this->loadHeader();
		$this->loadBody  ();
		return $this->html->template("search");
	}

	// prepare
	// ---------------------------------------- 
	private function prepare(){

		// table setting
		$this->setting = isset($this->post["setting"]) ? $this->post["setting"] : NULL;

		// column to field associations
		$this->selects = array("ASPECT_NAME"      => "aspect"     , 
		                       "COLLECTION_NAME"  => "collection" , 
		                       "CONTEXT_NAME"     => "context"    ,
		                       "EQTYPE_CODE"      => "eqtype"     ,
		                       "EQTYPEGROUP_CODE" => "eqtypegroup",
		                       "SITE_NAME"        => "psname"     ,
		                       "PSGROUP_CODE"     => "psgroup"    );

		// define the view
		$this->view = "fullmeasview";
		if(isset($this->post["showRepeated"]) && $this->post["showRepeated"]==1)
			$this->view = "fullmeasviewall";

		// define the columns
		$this->always = array("meassite_hash", "measurement_value", "measurement_time"); // always to keep switched on
		$current      = isset($this->post["cols"]) ? explode(",", $this->post["cols"]) : array(); // currently switched on
		$this->cols   = array();
		$webtransl    = $this->db->readTable("webtranslations", array(), array("dbtablename"=>strtoupper($this->view)));
		$new          = array();
		$has          = in_array("sort", $webtransl->columns());
		foreach($webtransl as $row){
			$c = new Col($row, $this->always, $current);
			if($has) $this->cols[$row->sort] = $c;
			else     array_push($this->cols, $c);
		}
		ksort($this->cols);
	}

	// submitApplySettings
	// ---------------------------------------- 
	private function submitApplySettings($newName=NULL) {
		/* Applies the current settings */

		$new = array();
		foreach($this->cols as $col){
			$k = "setting_".$col->columnName;
			if(in_array($col->columnName, $this->always)) continue;
			if(isset($this->post[$k]) && $this->post[$k]==1) {
				$col->show = true;
				array_push($new, $col->columnName);
				continue;
			}
			$col->show = false;
		}
		$this->html->set("cols", implode(",", $new));
		$this->setting = !empty($newName) ? $newName : 1; // 1 for applied settings, newName for saving
	}

	// submitDownload
	// ---------------------------------------- 
	private function submitDownload(){
		/* Downloads a single document */
		downloadDoc($this, "measdocuments", "id_measdocuments", "doccontent", "docname");
	}

	// submitExport
	// ---------------------------------------- 
	private function submitExport($type="csv") {
		/* When requesting an export */

		// generate the full table but without start/end range
		$tableAtts = $this->loadData(true, $type=="inv" ? array('id_', 'edittime') : array(), $type=="inv");
		$this->html->set("invisi_head", $tableAtts[0]);
		$this->html->set("invisi_body", $tableAtts[1]);
		$this->html->set("onload"     , sprintf("exportTable('.invisiTable', '%s')", $type=="xls" ? "excel" : "csv"), "main");
	}

	// submitLoadSettings
	// ---------------------------------------- 
	private function submitLoadSettings() {
		/* Applies the current settings */

		$lines = $this->db->readTable("usersettingswebsite", array(),
		                                             array("user_id"     =>$this->globals["userId"],
		                                                   "settingsname"=>$this->post["loadSetting"]));
		$entries = array();
		foreach($lines as $line) $entries[strtolower($line->columnname)] = $line->issetflag=="T";

		$new = array();
		foreach($this->cols as $col){
			$col->show = isset($entries[$col->columnName]) ? $entries[$col->columnName] : false;
			if($col->show) array_push($new, $col->columnName);
		}
		$this->html->set("cols", implode(",", $new));
		$this->setting = $this->post["loadSetting"];
	}

	// submitResetSettings
	// ---------------------------------------- 
	private function submitResetSettings() {
		/* Resets the current settings */

		foreach($this->cols as $col)
			$col->setDefault();
		$this->html->set("cols", "");
		$this->setting = NULL;
	}

	// submitSaveSettings
	// ---------------------------------------- 
	private function submitSaveSettings() {
		/* Saves the current settings */

		// has to have a name
		if(empty($this->setting)) return;

		// settings that are saved are also applied
		$this->submitApplySettings($this->setting);

		// delete existing rows
		$existing = $this->db->readTable("usersettingswebsite", array(),
		                                             array("user_id"     =>$this->globals["userId"],
		                                                   "settingsname"=>$this->setting));
		if($existing->count()>0){
			foreach($existing as $row)
				$row->delete = true;
			$this->db->usersettingswebsite->push();
			$this->db->usersettingswebsite->rebase();
		}

		// store new rows
		foreach($this->cols as $col){
			$new = array("user_id"       => $this->globals["userId"],
			             "settingsname"  => $this->setting,
			             "columnname"    => strtoupper($col->columnName),
			             "issetflag"     => $col->show ? "T" : "F",
			             "isdefaultflag" => $col->setasdefault,
			             "usercr"        => $this->globals["username"],
			             "usered"        => $this->globals["username"],
			             "createtime"    => $this->master->eventDateDb,
			             "edittime"      => $this->master->eventDateDb);
			$this->db->usersettingswebsite->append($new);
		}
		$this->db->usersettingswebsite->push();
	}
}

$page = new SearchPage($this, "views");
   
?>
