# !bash
sizem=$1
setup=$2
left=$3
bottom=$4
right=$5
top=$6
filename=$7
../../scripts/barcode -e 128B -o output.ps -u mm -m $sizem -t $setup+$left+$bottom-$right-$top -i $filename -p 210x297
ps2pdf -sPAPERSIZE=a4 output.ps

#source scripts/MakePDF.sh 14.25 16.75 3.3 4.8 bardat.txt 1
