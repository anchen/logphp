# !bash

top=$1
bottom=$2
left=$3
right=$4

bardat=$5
filename="./${bardat}"

stickersize=$6

if [ $stickersize = 0 ]
then
    setup="4x12"
fi

if [ $stickersize = 1 ]
then
    setup="3x9"
fi

../../scripts/barcode -e 128B -o output.ps -u mm -m 5,5 -t $setup+$left+$bottom-$right-$top -i $filename -p 210x297

/usr/bin/ps2pdf -sPAPERSIZE=a4 output.ps
#source scripts/MakePDF.sh 14.25 16.75 3.3 4.8 dir bardat.txt 1
