<?php

require_once "container.php";
//require_once "config.php";


// oracle11Limit
// -------------------------------------------- 
function oracle11Limit($sql, $config, $upper){
	// annoying Oracle < 12 compatibility
	if($config->limit->value > 0) {
		if($config->limit->offset->value == 0)
			$sql = $upper ? sprintf("SELECT * FROM (SELECT RSLTS.*, ROWNUM AS REC_NO FROM (%s) RSLTS) WHERE REC_NO <= %d", $sql, $config->limit->value) : sprintf("select * from (select rslts.*, rownum as rec_no from (%s) rslts) where rec_no <= %d", $sql, $config->limit->value);
		else
			$sql = $upper ? sprintf("SELECT * FROM (SELECT RSLTS.*, ROWNUM AS REC_NO FROM (%s) RSLTS) WHERE REC_NO > %d AND REC_NO <= %d", $sql, $config->limit->offset->value, $config->limit->offset->value+$config->limit->value) : sprintf("select * from (select rslts.*, rownum as rec_no from (%s) rslts) where rec_no > %d and rec_no <= %d", $sql, $config->limit->offset->value, $config->limit->offset->value+$config->limit->value);
	}
	return $sql;
}


// DbFile
// ============================================
class DbFile {
	/* A wrapper around every file to be uploaded to or from the data base */

	// members
	// ---------------------------------------- 
	public $db     = NULL;
	public $master = NULL;
	public $field  = NULL;
	public $name   = NULL;
	public $error  = UPLOAD_ERR_OK;
	public $size   = 0;
	public $tmp    = "";
	public $typer  = "";
	public $type   = "";
	public $isGood = false;

	// __construct
	// ---------------------------------------- 
	public function __construct($db, $name=""){
		$this->db     = $db;
		$this->master = $db->master;
		$this->field  = NULL;
		$this->name   = $name;
		$this->error  = UPLOAD_ERR_OK;
		$this->size   = 0;
		$this->tmp    = "";
		$this->typer  = ""; // raw type from $_FILES
		$this->type   = ""; // internal type name
		$this->isGood = false;
	}

	// seize
	// ---------------------------------------- 
	public function seize($container){
		/* Catch it from a $_FILES container */
		$required = array();
		if(count(array_intersect_key(array_flip($required), $container)) !== count($required)) return;
		$this->error = $container["error"   ];
		$this->name  = $container["name"    ];
		$this->size  = $container["size"    ];
		$this->tmp   = $container["tmp_name"];
		$this->typer = $container["type"    ];
		if(array_key_exists($this->typer, $this->master->docexts)) 
			$this->type = $this->master->docexts[$this->typer];
		$e = findAny($this->master->fileexts, $this->typer);
		if($e) $this->type = $e;
		$this->verify();
	}

	// download done via the fields, still leave for future
	//// download
	//// ---------------------------------------- 
	//public function download($handler){
	//	/* Downloads the file from the data base */
	//	if(!$this->isGood) return;
	//}

	// drop
	// ---------------------------------------- 
	public function drop($path){
		/* Drops the file into a given path */
		//if(!$this->isGood) return;
		if(!is_writable($path)) return;
		$this->tmp = rtrim($path, "/")."/".$this->name;
		return $this->field->value->export($this->tmp);
	}

	// upload done via the handlers, still leave for future
	//// upload
	//// ---------------------------------------- 
	//public function upload($handler){
	//	/* Uploads the file into the data base */
	//	if(!$this->isGood) return;
	//}

	// verify
	// ---------------------------------------- 
	private function verify(){
		/* Verifies a file after up- or download */
		$this->isGood = false;
		if($this->error != UPLOAD_ERR_OK         ) return;
		if($this->size  >= $this->db->maxFileSize) return;
		if($this->tmp && !file_exists($this->tmp)) return;
		$this->isGood = true;
	}
}




// DbHandle
// ============================================
class DbHandle {
	/* A wrapper any link between variables and SQL (called 'descriptor'
	* in Oracle SQL) */

	// members
	// ---------------------------------------- 
	public  $row   = NULL;
	public  $db    = NULL;
	public  $field = NULL;
	public  $short = NULL;
	public  $dtype = NULL;
	public  $link  = 0;

	// __construct
	// ---------------------------------------- 
	public function __construct($row, $field, $short, $dtype=SQLT_LNG){
		/* Constructor */
		$this->row   = $row;
		$this->db    = $row->db;
		$this->field = $field; // the link to the DbField
		$this->short = $short; // placeholer in SQL
		$this->dtype = $dtype; // Oracle data type
		$this->load();
	}

	// load
	// ---------------------------------------- 
	private function load(){
		/* Prepares the handle, i.e. sets up the descriptor */
		$this->link = 0;
		if($this->dtype == OCI_B_BLOB)
			$this->link = $this->db->descriptor(OCI_D_LOB);
	}
}




// DbColumn
// ============================================
class DbColumn extends ContainerType {
	/* The abstract entity of a column of a view, i.e. the definition of
	* a value including name etc. as it appears in a row in a database table;
	* however the point is, that a column does NOT have a value, but a name
	* and default */

	// members
	// ---------------------------------------- 
	public $key    = NULL;
	public $table  = NULL;
	public $isPK   = false;
	public $isFake = false;

	// __construct
	// ---------------------------------------- 
	public function __construct($table, $name, $key, $dtype, $default){
		/* Constructor */ 
		parent::__construct($table->master, $name, $dtype, $default);
		$this->key    = $key;
		$this->table  = $table;
		$this->isPK   = false;
		$this->isFake = false;
	}
}




// DbField
// ============================================
class DbField extends Container {
	/* The assignment of a value to a column within a row */

	// members
	// ---------------------------------------- 
	public  $row     = NULL;
	public  $column  = NULL;
	public  $updated = false;
	public  $incSeq  = NULL;

	// __construct
	// ---------------------------------------- 
	public function __construct($row, $column, $value, $key=NULL) {
		/* Constructor */
		$this->row     = $row;
		$this->column  = $column;
		$this->incSeq  = NULL;
		parent::__construct($column->master, "field_".$row->name."_".$column->name, !empty($key) ? $key : $column->name, $value, $column);
		$this->updated = false;
	}

	// setValue
	// ---------------------------------------- 
	public function setValue($value, $init=false) {
		/* Sets a new value to the field */
		if($init && gettype($value)=="string" && strpos($value, "incONE")!==false) {
			$this->incSeq=$value;
			$this->isGood=true;
			return;
		}
		parent::setValue($value, $init);
		if($init                ) return;
		if(!$this->isGood       ) return;
		$this->updated      = true;
		$this->row->updated = true;
		$table = $this->row->view;
		if(!$table->isTable     ) return;
		$table->updated = true;
		if(!$table->db->autoPush) return;
		$table->push();
	}
}




// DbRow
// ============================================
class DbRow implements Iterator {
	/* A single row as read from the data base, i.e. it consists of n 
	* field for which there are n values */

	// traits
	// ---------------------------------------- 
	//use Cache;


	// START: copy-paste from Box and Cache and CachePure (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// validate
	// ---------------------------------------- 
	protected function validate(){
		/* Validates itself and sets the status to true if all
		* is fine; to be overloaded by children */
		$this->isValid = true;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// members
	// ---------------------------------------- 
	public    $cache        = array();
	protected $iter         = 0;
	protected $keyname      = "name";
	public    $hierarchical = false;
	protected $virtualList  = array();

	// construct
	// ---------------------------------------- 
	public function constructCachePure($keyname = "name"){
		/* Constructor */
		$this->cache        = array();
		$this->iter         = 0;
		$this->hierarchical = false;
		$this->virtualList  = array();
		$this->keyname      = $keyname;
	}

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Returns the item with name $key if it exists */
		return $this->get($key);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if the cache holds an item with name $key */
		return $this->has($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Updates the item ith name $key */
		$this->set($key, $value);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes the object with name $key */
		$this->del($key);
	}

	// add
	// ---------------------------------------- 
	public function add($key, $value) {
		/* Updates the object with name $key; if the object already
		* exists, it is ignored */
		if($this->has($key)) return;
		$this->cache[$key] = $value;
	}

	// count
	// ---------------------------------------- 
	public function count() {
		/* Return the number of stored elements */
		return count($this->cache);
	}

	// find
	// ---------------------------------------- 
	public function find($value) {
		/* Checks if the cache holds an object $value */
		return in_array($value, $this->cache);
	}

	// free
	// ---------------------------------------- 
	public function free() {
		/* Clears the entire memory of the cache and destroys
		* all objects in doing so */
		$this->cache = array();
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if the cache holds an object with name $key */
		return array_key_exists($key, $this->cache);
	}

	// rebase
	// ---------------------------------------- 
	public function rebase() {
		/* Reassign key to value for all items in the cache; if key of the cache
		* is not the same as the value stored within the object, the key is updated,
		* unless it is already assigned to another object */
		if(count($this->cache)==0) return;
		foreach($this->cache as $key=>$obj){
			if($key==$obj->{$this->keyname}) continue;
			if(!array_key_exists($this->cache, $obj->{$this->keyname}))
				$this->cache[$obj->{$this->keyname}] = $obj; // new ref
			$this->del($key); // remove old ref
		}
	}

	// retrieve
	// ---------------------------------------- 
	public function retrieve($value) {
		/* Returns the key to the object $value if it is held by 
		* the cache, otherwise NULL */
		if(!$this->find($value)) return NULL;
		return array_search($value, $this->cache);
	}

	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $this->virtualList[$keys[$this->iter]];
		}
		$keys = array_keys($this->cache);
		return $this->cache[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current object */
		if($this->hierarchical) {
			$keys = array_keys($this->virtualList);
			return $keys[$this->iter];
		}
		$keys = array_keys($this->cache);
		return $keys[$this->iter];
	}
	
	// next
	// ---------------------------------------- 
	public function next() {
		/* Iteration function, iterates through objects */
		++$this->iter;
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first object */
		$this->iter = 0;
		$this->buildVirtualList();
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		return $this->iter < count($this->cache);
	}

	// buildVirtualList
	// ---------------------------------------- 
	protected function buildVirtualList(){
		/* Builds the virtual list for iteration accounting for the
		* hierarchical parent-child structure */
		$this->virtualList = array();
		foreach($this->getParents() as $parent){
			array_push($this->virtualList, $parent);
			$this->virtualList = array_merge($this->virtualList, $parent->children(false));
		}
	}

	// getChildren
	// ---------------------------------------- 
	public function getChildren($parentname, $direct=true){
		/* Returns the list of children for a given parent */
		if(!$this->has($parentname)) return array();
		$parent = $this->get($parentname);
		return $parent->children($direct);
	}

	// getParent
	// ---------------------------------------- 
	public function getParent($childname=NULL){
		/* Returns the direct parent for a given child */
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parent;
	}

	// getParents
	// ---------------------------------------- 
	public function getParents($childname=NULL){
		/* Returns the list of parents for a given child */
		if(empty($childname)){
			$result = array();
			foreach($this->cache as $item){
				if(!empty($item->parent)) continue;
				array_push($result, $item);
			}
			return $result;
		}
		if(!$this->has($childname)) return array();
		$child = $this->get($childname);
		return $child->parents();
	}

	// sub
	// ---------------------------------------- 
	public function sub($key, $value, $parent){
		/* Adds a new item as child to a parent (subordinate) */
		if(!$this->has($parent->{$this->keyname})) $this->set($parent->{$this->keyname}, $parent);
		$this->set($key, $value);
		$parent->addChild($value);
	}

	// super
	// ---------------------------------------- 
	public function super($key, $value, $child){
		/* Adds a new item as parent to a child (superordinate) */
		if(!$this->has($child->{$this->keyname})) $this->set($child->{$this->keyname}, $child);
		$this->set($key, $value);
		$value->addChild($child);
	}

	// complement
	// ---------------------------------------- 
	public function complement($other) {
		/* Returns the list of items that are in the other cache
		* but not in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// duplicate
	// ---------------------------------------- 
	public function duplicate($newname=NULL) {
		/* Returns a new instance of the cache with all items cloned
		* as well */
		$new = new Cache($this->master, !empty($newname) ? $newname : $this->master->cloneION($this->name));
		foreach($this->cache as $key=>$obj)
			$new->set($key, $obj->duplicate());
		return $new;
	}

	// intersection
	// ---------------------------------------- 
	public function intersection($other) {
		/* Returns the list of items that are both in the other cache
		* and also in this one. Note, the items are not cloned, rather
		* their references are returned. */
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry){
			if(!$this->has($entry->{$this->keyname})) continue;
			$result[$entry->{$this->keyname}] = $entry;
		}
		return $result;
	}

	// isNull
	// ---------------------------------------- 
	public function isNull(){
		/* Checks if the cache contains zero data */
		return (count($this->cache)==0);
	}

	// merge
	// ---------------------------------------- 
	public function merge($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, it is overwritten. */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $key=>$entry)
			$this->set($key, $entry->duplicate());
	}

	// sum
	// ---------------------------------------- 
	public function sum($other) {
		/* Adds the items from another cache into this one. If 
		* the key is already assigned to an item, the entry is
		* ignored */
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return;
		foreach($other->cache as $entry){
			if($this->has($entry->{$this->keyname})) continue;
			$this->set($entry->{$this->keyname}, $entry->duplicate());
		}
	}

	// union
	// ---------------------------------------- 
	public function union($other) {
		/* Returns the array of items that are in this or
		* the other cache; in conflicting cases, the items of
		* this cache are preferred; Note that the entries are
		* not copied, rather the references to them are returned. */ 
		$result = array();
		if(empty($other) || !is_object($other) || !property_exists($other, "cache")) return $result;
		foreach($other->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		foreach($this ->cache as $entry) $result[$entry->{$this->keyname}] = $entry;
		return $result;
	}

	// construct
	// ---------------------------------------- 
	public function constructCache($master, $name, $keyname="name"){
		/* Constructor */
		$this->constructBox      ($master, $name);
		$this->constructCachePure($keyname);
	}

	// END: copy-paste from Box and Cache and CachePure (5.3 compatibility)
	// ========================================
	// ========================================


	// members
	// ---------------------------------------- 
	public    $view        = NULL;
	public    $db          = NULL;
	public    $vb          = NULL;
	public    $idx         = NULL;
	public    $pk          = NULL;
	public    $container   = array();
	public    $updated     = false;
	public    $appended    = false;
	public    $delete      = false;
	public    $deleted     = false;
	private   $vars        = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($view, $name, $fields=array(), $replacements=array()) {
		/* Constructor. Here, $fields is as in the setFields method. */
		$this->constructCache($view->master, $name, "key");
		$this->view      = $view;
		$this->db        = $view->db;
		$this->vb        = $view->vb;
		$this->container = array(); // return values
		$this->idx       = NULL;  // index of the row in the view
		$this->pk        = NULL;  // link to the DbField of the PK
		$this->updated   = false; // true if at least one field was updated
		$this->appended  = false; // true if the entire row is new
		$this->delete    = false; // true if row should be deleted in the DB
		$this->deleted   = false; // true if row has been deleted in the DB
		$this->vars      = array();
		$this->load($fields, $replacements);
	}




	// individual fields
	// ========================================

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes the DbField instance to the column with name $key from the container,
		* effectively using the default value of the column. */
		unset($this->cache[$key]);
		$this->updated = true;
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Returns the value of the DbField if it exists */
		if(!$this->has($key)) return NULL;
		return $this->cache[$key]->value;
	}

	// getField
	// ---------------------------------------- 
	public function getField($key) {
		/* Returns the reference to the DbField if it exists */
		if(!$this->has($key)) return NULL;
		return $this->cache[$key];
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value, $colname=NULL, $noPush=false) {
		/* Updates the value of the field to the column with name $key, if it exists, otherwise
		* (if $key is a valid column name) it creates a new DbField instance and stores the value;
		* for some fields (when using reformats) the column name can be different, then use $colname */
		if(!$this->has($key)){
			if(!in_array($key, $this->view->columns()) && !in_array($colname, $this->view->columns())) return;
			$this->cache[$key] = new DbField($this, $this->view->columnByName(!empty($colname) ? $colname : $key), $value, $key);
		}
		else { // necessary: DbField::setValue does a push unless it is called via constructor
			$this->cache[$key]->setValue($value, $noPush); 
		}
		if(gettype($value)=="object") $value->field = $this->cache[$key];
		$this->updated = true;
	}

	//// set
	//// ---------------------------------------- 
	//public function set($key, $value, $fieldname=NULL) {
	//	/* Updates the value of the field to the column with name $key, if it exists, otherwise
	//	* (if $key is a valid column name) it creates a new DbField instance and stores the value;
	//	* note DbField and DbColumn can have different names, if so use $fieldname for the DbField
	//	* and $key for the DbColumn */
	//	if(!$this->has($fieldname ? $fieldname : $key)){
	//		if(!in_array($fieldname ? $fieldname : $key, $this->view->columns())) return;
	//		$this->cache[$fieldname ? $fieldname : $key] = new DbField($this, $this->view->columnByName($key), $value);
	//	}
	//	$this->cache[$fieldname ? $fieldname : $key]->setValue($value); 
	//	if(gettype($value)=="object") $value->field = $this->cache[$key];
	//	$this->updated = true;
	//}



	// variable treatment (FIXME: where to put?)
	// ========================================
	public function getVar($key){
		if(!$this->hasVar($key)) return NULL;
		return $this->vars[$key];
	}
	public function hasVar($key){
		return array_key_exists($key, $this->vars);
	}
	public function setVar($key, $value){
		$this->vars[$key] = $value;
	}



	// all columns and fields
	// ========================================

	// columns
	// ---------------------------------------- 
	public function columns() {
		/* Returns the columns of the row according to the implicitly applied config */
		$cols = array();
		foreach($this->cache as $field) 
			array_push($cols, $field->column->key);
		return $cols;
	}

	// fill
	// ---------------------------------------- 
	public function fill($fields=array(), $replacements=array()) {
		/* Updates the values of multiple fields at the same time. Note, the array must
		* contain both key and value in order to do the assignment of the value to the
		* field in proper ways. */
		foreach($fields as $key=>$val)
			$this->set($key, $val, array_key_exists($key, $replacements) ? $replacements[$key] : NULL);
		$this->updated = true;
	}

	// values
	// ---------------------------------------- 
	public function values($removeDuplicates = false) {
		/* Returns the values associated to the fields of the row, according to the 
		* implicitly applied config. */
		$values = array();
		foreach($this->cache as $key=>$field){
			if($removeDuplicates && in_array($field->value, $values)) continue;
			$values[$key] = $field->value;
		}
		return $values;
	}




	// general stuff
	// ========================================

	// assignPk
	// ---------------------------------------- 
	private function assignPk(){
		/* Assigns the $this->pk variable to the proper field that contains the
		* primary key, in case the view is a table, otherwise not. */
		if(!$this->view->isTable ) return false;
		if(count($this->cache)<=0) return false;
		$pk = $this->view->pk;
		if(empty($pk)) { $this->pk = NULL; return false; }
		foreach($this->cache as $key=>$field){
			if($key != $pk->key) continue;
			$this->pk = $this->field;
			return true;
		}
		$this->pk = NULL; // this will happen if insert or select does not contain the PK column!
		return false;
	}

	// getReturns
	// ---------------------------------------- 
	public function getReturns() {
		/* Returns the stored return values from the last push */
		return $this->container;
	}

	// load
	// ---------------------------------------- 
	private function load($fields=array(), $replacements=array()) {
		/* Effectively the same as fill, but putting $updated back to false. This
		* method, therefore, only must be used from the constructor, and filled by the
		* read methods of the DbHandler! On top it also assigns the PK field. */
		$this->fill($fields, $replacements);
		foreach($this->cache as $field) $field->updated = false;
		$this->updated = false;
		$this->assignPk();
	}

	// pull
	// ---------------------------------------- 
	public function pull(){
		/* Forces rereading of the data from the data base given the current selection.
		* Note, this method is intended merely for debugging, and it only works for tables.
		* Also, DbViews and DbTables have an internal pointer $lastTotal to the last total
		* DbConfig used to retrieve data --- this is NOT updated by this function! USE WITH CARE! */
		if(!$this->view->isTable) return;
		if(empty($this->pk) || empty($this->pk->value)) return;
		$cfg = $this->view->total();
		$raw = $this->db->readConfig($cfg, $this->view->name);
		if(count($raw)==0) return;
		$use = NULL;
		if(array_key_exists("the_pk"                  , $raw[0])) $use = "the_pk";
		if(array_key_exists(strtolower($this->pk->key), $raw[0])) $use = strtolower($this->pk->key);
		if(empty($use)) return; // no primary key column
		foreach($raw as $row){
			if($row[$use] != $this->pk->value) continue;
			$this->fill($row);
			return;
		}
	}

	// push
	// ---------------------------------------- 
	public function push(){
		/* Pushes the local changes upstream to the data base. */

		if(!$this->view->isTable) return;
		if(!($this->updated || $this->appended || $this->delete)) return;

		$this->container = array();

		// building the SQL
		$handlers = $this->db->setHandlers($this, $this->appended);
		if($this->delete       ) $sql = $this->db->sqlDelete($this           ); // delete
		else if($this->appended) $sql = $this->db->sqlInsert($this, $handlers); // insert
		else                     $sql = $this->db->sqlUpdate($this, $handlers); // update
		if(empty($sql)) return;

		// execute sql
		if(!$this->db->writeSql($sql, $handlers)) {
			$this->vb->error("Writing of the following SQL statement failed:<br />".$sql, true);
			$this->db->undo();
			return;
		}

		// if table was deleted, we're done
		if($this->delete){
			$this->deleted;
			return;
		}

		// upload files
		foreach($handlers as $handler){
			if(empty($handler->field) || !is_object($handler->field->value)) continue;
			if(get_class($handler->field->value)!="DbFile") continue;
			$handler->link->savefile($handler->field->value->tmp);
		}

		// for inserted row need to update the value of the PK field
		if($this->appended) {
			$this->set($this->view->pk->key, $handlers[0]->link, NULL, true);
			$this->pk = $this->cache[$this->view->pk->key];
			$this->name = "row_".strval($this->pk->value);
			$this->view->focus($this->idx);
		}

		// extract incSeq handlers (not part of the return values)
		foreach($handlers as $handler){
			if(empty($handler->field->incSeq)) continue;
			$this->set($handler->field->column->key, $handler->link, NULL, true); 
			continue; 
		}

		// fill the return values into the internal container
		if(count($this->view->showReturns())>0) {
			foreach($handlers as $handler){
				if(!empty($handler->field->value) && get_class($handler->field->value)=="DbFile") continue;
				if($this->appended && $handler->short==":pk") continue;
				array_push($this->container, $handler->link);
			}
		}

		// clean up
		$this->view->updated = false;
		$this->updated       = false;
		$this->appended      = false;
		foreach($this->cache as $field) $field->updated = false;
	}

}

/*
how to upload a file:
$sql = "insert into %s %s values %s returning %s into %s";
e.g. "insert into table (docloc) values (empty_blob()) returning docloc into :blob";
then
$handler = oci_new_descriptor($this->conn, OCI_D_LOB);
$this->parse = oci_parse($this->conn, $sql);
oci_bind_by_name($this->parse, ":blob", $handler, -1, OCI_B_BLOB);
$this->execute = oci_execute($this->parse, OCI_DEFAULT);
if($this->execute) $handler->savefile($dbfile->tmp);
*/




// DbView
// ============================================
class DbView implements Iterator {
	/* A view is a generalization of a table, meaning, it is 
	* a collection of n fields for which there exist m values,
	* i.e. an n x m matrix of information stored in the DB but
	* now buffered in this class; the SqlView has always a
	* current rowIdx which needs to be pushed forward, thus
	* one can do view.fieldName to get the corresponding value
	* in the current row. */


	// traits
	// ---------------------------------------- 
	//use Box;


	// START: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================

	// members
	// ---------------------------------------- 
	public    $master   = NULL;
	public    $name     = NULL;
	public    $isValid  = true;
	protected $subelms  = array();
	public    $parent   = NULL;

	// construct
	// ---------------------------------------- 
	public function constructBox($master, $name){
		/* Constructor */
		$this->master  = $master;
		$this->name    = $master->checkION($name) ? $name : $master->makeION($name);
		$this->master->registerION($this->name);
		$this->isValid = true;
		$this->subelms = array();
		$this->parent  = NULL;
	}

	// addChild
	// ---------------------------------------- 
	public function addChild($item){
		/* Adds another object as child to this entity */
		array_push($this->subelms, $item);
		$item->parent = $this;
	}

	// children
	// ---------------------------------------- 
	public function children($direct=true){
		/* Returns the list of children of this entity */
		if($direct) return $this->subelms;
		$result = array();
		foreach($this->subelms as $item){
			array_push($result, $item);
			$result = array_merge($result, $item->children(false));
		}
		return $result;
	}

	// delChild
	// ---------------------------------------- 
	public function delChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return;
		unset($this->subelms[array_search($item, $this->subelms)]);
	}

	// hasChild
	// ---------------------------------------- 
	public function hasChild($item){
		/* Removes another object as child to this entity */
		if(!in_array($item, $this->subelms)) return false;
		return true;
	}

	// parents
	// ---------------------------------------- 
	public function parents(){
		/* Returns the list of parents of this entity */
		if(empty($this->parent)) return array();
		$result = array();
		array_push($result, $this->parent);
		$item = $this->parent;
		while(true){
			if(empty($item->parent)) return $result;
			array_push($result, $item->parent);
			$item = $item->parent;
		}
		return $result;
	}

	// END: copy-paste from Box (5.3 compatibility)
	// ========================================
	// ========================================



	// members
	// ---------------------------------------- 
	public    $db         = NULL;
	public    $vb         = NULL;
	protected $cols       = array();
	public    $rows       = array();
	public    $slim       = array();
	protected $iter       = 0;
	protected $currentRow = NULL;
	protected $lastTotal  = NULL;
	protected $configs    = array();
	protected $stashed    = array();
	public    $isTable    = false;
	protected $isSlim     = false;
	public    $pk         = NULL;


	// __construct
	// ---------------------------------------- 
	public function __construct($db, $name, $config=NULL) {
		/* Constructor */
		$this->constructBox($db->master, $name);
		$this->db         = $db;
		$this->vb         = $db->vb;
		$this->cols       = array();
		$this->rows       = array();
		$this->slim       = array();
		$this->iter       = 0;
		$this->currentRow = NULL;
		$this->lastTotal  = NULL;
		$this->configs    = array();
		$this->stashed    = array();
		$this->isTable    = false;
		$this->isSlim     = false;
		$this->pk         = NULL;
		if($config!=NULL) $this->read($config);
	}




	// field access in current row
	// ========================================

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Retrieves the value of a field in the current row */
		return $this->get($key);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if a field exists in the current row */
		return $this->has($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Sets a new value to a field in the current row */
		$this->set($key, $value);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes a field (effectively setting default value) from the current row */
		$this->del($key);
	}

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes a field (effectively setting default value) from the current row */
		$this->currentRow->del($key);
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Retrieves the value of a field in the current row */
		if(!$this->has($key)) return NULL;
		return $this->currentRow->get($key);
	}

	// getField
	// ---------------------------------------- 
	public function getField($key) {
		/* Returns the reference to the DbField if it exists */
		if(!$this->has($key)) return NULL;
		return $this->currentRow->getField($key);
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if a field exists in the current row */
		if(count($this->rows)==0) return false;
		return $this->currentRow->has($key);
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value) {
		/* Sets a new value to a field in the current row */
		$this->currentRow->set($key, $value);
	}




	// iteration of current row
	// ========================================

	// backward
	// ---------------------------------------- 
	public function backward() {
		/* Places pointer to internal current row to the previous row and returns it */
		$this->currentRow = prev($this->rows);
		return $this->currentRow;
	}

	// first
	// ---------------------------------------- 
	public function first() {
		/* Places pointer to internal current row to the first row and returns it */
		$this->currentRow = reset($this->rows);
		return $this->currentRow;
	}

	// focus
	// ---------------------------------------- 
	public function focus($key) {
		/* Places the pointer to internal current row via the key and returns it */
		if(!array_key_exists($key, $this->rows)) return $this->currentRow;
		$this->currentRow = $this->rows[$key];
		return $this->currentRow;
	}

	// forward
	// ---------------------------------------- 
	public function forward() {
		/* Places pointer to internal current row to the next row and returns it */
		$this->currentRow = next($this->rows);
		return $this->currentRow;
	}

	// index
	// ---------------------------------------- 
	public function index() {
		/* Returns the index of the current row */
		return array_search($this->currentRow, $this->rows);
	}

	// last
	// ---------------------------------------- 
	public function last() {
		/* Places pointer to internal current row to the last row and returns it */
		$this->currentRow = end($this->rows);
		return $this->currentRow;
	}

	// present
	// ---------------------------------------- 
	public function present() {
		/* Returns the pointer to the current row */
		return $this->currentRow;
	}




	// iteration in loops
	// ========================================
	
	// current
	// ---------------------------------------- 
	public function current() {
		/* Iteration function, returns pointer to current row */
		$keys = array_keys($this->rows);
		return $this->rows[$keys[$this->iter]];
	}
	
	// key
	// ---------------------------------------- 
	public function key() {
		/* Iteration function, returns key to current row */
		$keys = array_keys($this->rows);
		return $keys[$this->iter];
	}
	
	// next
	// ---------------------------------------- 
	public function next() {
		/* Iteration function, iterates through rows */
		++$this->iter;
	}

	// rewind
	// ---------------------------------------- 
	public function rewind() {
		/* Iteration function, sets pointer back to first row */
		$this->iter = 0;
	}
	
	// valid
	// ---------------------------------------- 
	public function valid() {
		/* Iteration function, checks if current pointer is valid */
		return $this->iter < count($this->rows); 
	}




	// managing configs
	// ========================================

	// gather
	// ---------------------------------------- 
	public function gather($key) {
		/* Returns the pointer to a prior applied selection with name $name if 
		* it exists, NULL otherwise. */
		if(!array_key_exists($key, $this->configs)) return NULL;
		return $this->configs[$key];
	}

	// pop
	// ---------------------------------------- 
	public function pop($doPull=true) {
		/* Reverts the currently stashed selections and suspends all others */
		if(count($this->stashed)==0) return;
		$last = count($this->stashed)-1;
		foreach($this->configs as $key=>$config){
			if(!$config->suspended) continue;
			if(!in_array($key, $this->stashed[$last])) continue;
			$config->suspended = false;
		}
		unset($this->stashed[$last]);
		if(!$this->db->autoPull || !$doPull) return;
		$this->read(NULL, false);
	}

	// resume
	// ---------------------------------------- 
	public function resume($name=NULL, $doPull=true) {
		/* Reverts a given suspended selection with name $name or all if NULL is given. */
		foreach($this->configs as $key=>$config){
			if($name==NULL || $name==$key) 
				$config->suspended = false;
		}
		if(!$this->db->autoPull || !$doPull) return;
		$this->read(NULL, false);
	}

	// suspend
	// ---------------------------------------- 
	public function suspend($name=NULL, $doPull=true) {
		/* Suspends the currently applied selection with name $name or all if NULL is given. */
		foreach($this->configs as $key=>$config){
			if($name==NULL || $name==$key) 
				$config->suspended = true;
		}
		if(!$this->db->autoPull || !$doPull) return;
		$this->read(NULL, false);
	}

	// stash
	// ---------------------------------------- 
	public function stash($doPull=true) {
		/* Stashes all currently applied selection; in effect same as suspend but
		* remembering which ones were suspended recently */
		$index = count($this->stashed);
		$this->stashed[$index] = array();
		foreach($this->configs as $key=>$config){
			if($config->suspended) continue;
			$config->suspended = true;
			array_push($this->stashed[$index], $key);
		}
		if(!$this->db->autoPull || !$doPull) return;
		$this->read(NULL, false);
	}

	// transfer
	// ---------------------------------------- 
	public function transfer($other, $key=NULL) {
		/* Transfers a selection with name $name from another DbView $other to this one */
		$config = !empty($key) ? $other->gather($key) : $other->total;
		$this->ontop($config);
	}

	// total
	// ---------------------------------------- 
	public function total() {
		/* Builds the total config from all currently applied selections.
		* The building is done via config->merge. Note that the order is important,
		* because merge resolves conflicts by taking the newer value. In the loop, 
		* the order is such that newer configs are merged after the older ones, thus
		* newer values are preferred. */
		$total = new DbConfig($this->master, $this->name."_total_".timestamp());
		foreach($this->configs as $key=>$config){
			if($config->suspended) continue;
			$total->merge($config);
		}
		return $total;
	}




	// reading
	// ========================================

	// ontop
	// ---------------------------------------- 
	public function ontop($config) {
		/* Reads the data from the data base according to a config $config;
		* does NOT suspend any of the configs before, hence, applies the new
		* one on top */
		$this->read($config, false);
	}

	// pull
	// ---------------------------------------- 
	public function pull() {
		/* Forces rereading of the data from the data base given the current selection.
		* Unlike to the DbRow case, this method recreates also all DbRow instances. */
		$total = $this->total();
		$this->retrieve($total);
		$this->lastTotal = $config;
	}

	// read
	// ---------------------------------------- 
	public function read($config=NULL, $reset=true) {
		/* Reads the data from the data base according to a config $config; if 
		* $reset is set to true, all other configs applied earlier are suspended
		* prior to the selection of this config */
		if($this->isTable && (!empty($config) && count($config->joinon->value)>0)) return; // table fed with a view config
		if($reset         ) $this->suspend(NULL, false);
		if(!empty($config)) $this->configs[$config->key] = $config;
		if(!$this->db->autoPull) return;
		$total = $this->total();
		if($total->identical($this->lastTotal)) return;
		if($total->isNull()                   ) return;
		$this->retrieve($total);
		$this->lastTotal = $total;
	}

	// retrieve
	// ---------------------------------------- 
	private function retrieve($config) {
		/* Retrieves the data from the data base given a configuration $config. It unwraps
		* the raw info from the DbHandler method and stores it in the internal buffers (also
		* building the associated objects) of the view.
		* The logic:
		* (1) download the data from the data base
		* (2) in case of a table: build DbRow instances and give the field contents
		*     (the DbRow constructor will fill its buffers and build the DbFields)
		*     then go to step (4)
		* (3) in case of a proper view
		* (i) collect data from the config 
		*   * unwrap all tables involved in the config (esp join statement)
		*     this should give you either the table name directly or table name + abbreviation
		*   * unwrap all columns involved in the config (including also reformatted columns and use of *)
		*     the columns should be named in the format <table/abbreviation>.<column>
		* (ii) link the DbColumns from the tables the view is related to
		* (iii) now do step 2: build the DbRows and give the field contents, the
		*     DbRow instance will pick up the proper DbColumns and build the DbFields
		* (4) place the pointer to the last row */

		// retrieve the data
		$this->rows = array();
		$raw = $this->db->readConfig($config, $this->isTable ? $this->name : NULL);
		if(count($raw)==0) return false;

		// if this view is a table, columns are already linked
		if($this->isTable){

			// test if PK column is there; return if not
			$use = NULL;
			if(array_key_exists("the_pk", $raw[0])) $use = "the_pk";
			if(!empty($this->pk) && array_key_exists(strtolower($this->pk->key), $raw[0])) $use = strtolower($this->pk->key);
			//if(empty($use)) return false; // no primary key column

			// building columns for reformatted translation from <reformat> => <column name>
			$columns = $this->db->extractColumnnames($config, $this->name);
			$collist = array();
			foreach($columns as $coldef){
				if(empty($coldef[0])) continue;
				if(strpos($coldef[0], "(")!==false){
					$this->setColumn($coldef[1], "any", NULL, true);
					continue;
				}
				$collist[$coldef[1]] = $coldef[0];
			}

			// build slim version
			if($config->slim->value){
				$this->slim = array();
				foreach($raw as $row)
					array_push($this->slim, $row);
				$this->isSlim = true;
				return true;
			}

			// build DbRows
			$i = 0;
			$this->isSlim = false;
			foreach($raw as $row){
				$key = "row_".strval(!empty($use) ? $row[$use] : $i);
				//$this->rows[$key] = new DbRow($this, $key, $row, $collist);
				$r = new DbRow($this, $key, $row, $collist);
				$r->idx = $row["_idx"];
				array_push($this->rows, $r);
				++$i;
			}
			// set pointer to last row
			$this->last();
			return true;
		}


		// PROPER VIEW CASE
		// extract column name the field (probably reformatted) is matched to

		// tables (note, for views there may be multiple tables)
		$tables = array();
		$shorts = array();
		$this->db->extractTablenames($config, $shorts, $tables);

		// columns
		$columns = $this->db->extractColumnnames($config, NULL, $tables, $shorts);

		// sanity check
		if(count($tables)==0 || count($columns)==0) return false;

		// build the translation from <short>.<column> = [<tablename>, <column>]
		// or            <reformat> = <short>.<column> = [<tablename>, <column>]
		$collist = array();
		foreach($columns as $coldef){
			// functional reformatted column, need fake DbColumn
			if(!empty($coldef[0]) && strpos($coldef[0], "(")!==false){
				$collist[$coldef[1]] = NULL;
				continue; 
			}
			// non-functional reformatted columns or non-reformatted columns 
			// (proper views must have the columns called <table>.<columnname>;
			// so either "<table>.<column> as <abbr>" or "<table>.<column>")
			$exp   = explode(".", !empty($coldef[0]) ? $coldef[0] : $coldef[1]);
			if(count($exp)==0) continue;
			if(count($exp)==1) { $collist[$coldef[1]] = NULL; continue; }
			$short = trim(preg_replace('/[0-9]+/', '', $exp[0])); // can be e2
			$col   = trim($exp[1]);
			$collist[$coldef[1]] = array($tables[array_search($short, $shorts)], $col);
		}

		// sanity check
		if(count($collist)==0) return false;

		// link the DbColumn objects
		$this->cols = array();
		foreach($collist as $name=>$repl){
			if(substr($name, 0, 1)=="_") continue;
			if(!array_key_exists($name, $collist   )) continue;
			if( array_key_exists($name, $this->cols)) continue;
			if(empty($repl)){
				// creating a fake column object, simply for the thing to work; 
				// data type is "any", thus, one cannot (or should not) edit the values
				$col = new DbColumn($this, "dbcolumn_fake", $name, "any", NULL);
				$col->isFake = true;
				$this->storeColumn($name, $col);
				continue;
			}
			$this->linkColumn($name, $repl[0], $repl[1]);
		}

		// build slim version
		if($config->slim->value){
			$this->slim = array();
			foreach($raw as $row)
				array_push($this->slim, $row);
			$this->isSlim = true;
			return true;
		}

		// go through the data and build the DbFields and DbRows
		$this->isSlim = false;
		foreach($raw as $row){
			$key   = "row_".strval($row["_idx"]);
			//$this->rows[$key] = new DbRow($this, $key, $row);
			array_push($this->rows, new DbRow($this, $key, $row));
		}

		// place the internal pointer onto the last row
		$this->last();
		return true;
	}

	// revert
	// ---------------------------------------- 
	public function revert(){
		/* Second part of the inbetween mode; restores the old setup after the
		* call of separate() to suspend the current config and pop back the 
		* stashed ones */
		$this->suspend(NULL, false); // no pulling
		$this->pop();                // pull now by reverting all
	}

	// separate
	// ---------------------------------------- 
	public function separate($config) {
		/* First part of the inbetween mode; reads the new config and only 
		* this one; all other previously active configs are suspended for the 
		* time being (stashed), then the config is applied */
		$this->stash(false);                 // no pulling now
		$this->read($config, false);         // pull new config only
	}

	// sync
	// ---------------------------------------- 
	public function sync() {
		/* Synchronizes data stored locally and upstream. For a DbView
		* this is the same as pull() */
		$this->pull(); 
	}




	// reading properties
	// ========================================

	// column
	// ---------------------------------------- 
	public function column($idx) {
		/* Returns a stored DbColumn instance according to its index */
		$keys = $this->columns();
		return $this->cols[$keys[$idx]];
	}

	// columnByName
	// ---------------------------------------- 
	public function columnByName($key) {
		/* Returns the DbColumn instance by its internal name;
		* note it is crucial that the object is retrieved by its 
		* key in the internal container and NOT by its actual ION,
		* because of how they are stored for proper views! */
		if(!array_key_exists($key, $this->cols)) return NULL;
		return $this->cols[$key];
	}

	// columns
	// ---------------------------------------- 
	public function columns() {
		/* Returns a stored DbColumn instance according to its internal name (not ION) */
		return array_keys($this->cols);
	}

	// count
	// ---------------------------------------- 
	public function count() {
		/* Returns the number of stored rows */
		return count($this->rows);
	}

	// order
	// ---------------------------------------- 
	public function order($fieldname="pk", $way="asc") {
		/* Orders the stored rows according to given criteria */
		if($this->isSlim) return;
		if(!in_array($way, array("asc", "desc"))) return;
		$neworder = array();
		foreach($this->rows as $row) $neworder[$row->$fieldname] = $row;
		if($way=="asc") ksort ($neworder);
		else            krsort($neworder);
		$this->rows = array();
		//foreach($neworder as $row) $this->rows["row_".$row->pk->value] = $row;
		foreach($neworder as $row) array_push($this->rows, $row);
	}

	// rebase
	// ---------------------------------------- 
	public function rebase() {
		/* Removes deleted rows and rebuilds the $rows cache */
		if($this->isSlim) return;
		foreach($this->rows as $key=>$row){
			if(!$row->deleted) continue;
			unset($this->rows[$key]);
		}
		$this->order();
	}

	// row
	// ---------------------------------------- 
	public function row($idx) {
		/* Returns a stored DbRow instance according to its index */
		if($this->isSlim) return $this->slim[$idx];
		$keys = array_keys($this->rows);
		return $this->rows[$keys[$idx]];
	}

	// rowByName
	// ---------------------------------------- 
	public function rowByName($name) {
		/* Returns a stored DbRow instance according to its internal name (not ION) */
		$rnames = array();
		foreach($this->rows as $row) array_push($rnames, $row->name);
		if(!in_array($name, $rnames)) return NULL;
		return $this->rows[array_search($name, $rnames)];
		//if(!array_key_exists($name, $this->rows)) return NULL;
		//return $this->rows[$name];
	}

	// rows
	// ---------------------------------------- 
	public function rows() {
		/* Returns the internal names of the rows */
		return array_keys($this->rows);
	}

	// values
	// ---------------------------------------- 
	public function values($colname, $removeDuplicates = false) {
		/* Returns the values associated to the fields of the row, according to the 
		* implicitly applied config. */
		if(!in_array($colname, $this->columns())) return array();
		$values = array();
		foreach($this->rows as $row){
			if($removeDuplicates && in_array($row->$colname->value, $values)) continue;
			array_push($values, $row->$colname->value);
		}
		return $values;
	}




	// access to columns for proper views
	// ========================================

	// linkColumn
	// ---------------------------------------- 
	private function linkColumn($name, $table, $column) {
		/* Links a column with name $column from a table with name $table
		* to this view; an internal name $name is used (since the same column
		* name can appear in multiple tables) */
		if(!$this->db->has($table)) return;
		$dbt = $this->db->get($table);
		if(!$dbt->isTable) return;
		if(!$dbt->hasColumn($column)) return;
		$this->cols[str_replace(".", "_", $name)] = $dbt->getColumn($column);
	}

	// linkColumns
	// ---------------------------------------- 
	private function linkColumns($list){
		/* Links a list of columns to this view; the list format
		* is $name => array($tablename, $columnname) */
		if(empty($list) || !is_array($list) || count($list)==0) return;
		foreach($list as $name=>$bundle)
			$this->linkColumn($name, $bundle[0], $bundle[1]);
	}

	// storeColumn
	// ---------------------------------------- 
	private function storeColumn($name, $column) {
		/* Stores a column in the view; in effect, the result is the same as
		* for linkColumn (cols[.] = DbColumn(..)), but unlike for linkColumn
		* the reference to the DbColumn object is ONLY stored by the view;
		* needed for reformatted fields with functional expressions */
		$this->cols[$name] = $column;
	}

}




// DbTable
// ============================================
class DbTable extends DbView {
	/* A child of SqlView but with isTable set to true
	* need to be careful to use this really only for tables */


	// members
	// ---------------------------------------- 
	public    $owner     = NULL;
	protected $returns   = array();
	protected $container = array();

	// __construct
	// ---------------------------------------- 
	public function __construct($db, $name, $config=NULL) {
		/* Constructor */
		parent::__construct($db, $name, $config);
		$this->owner     = $db->owner;
		$this->isTable   = true;
		$this->returns   = array();
		$this->container = array();
	}




	// general stuff
	// ========================================

	// assignPk
	// ---------------------------------------- 
	public function assignPk(){
		/* Assigns the $this->pk variable to the proper column that 
		* is related to the primary key and checks if there is really
		* just one such column */
		$assigned = false;
		foreach($this->cols as $col){
			if(!$col->isPK) continue;
			if($assigned){
				$this->vb->warning(sprintf("DbRow instance %s of table %s has multiple primary key columns!", $this->name, $this->view->name));
				return;
			}
			$this->pk = $col;
			$assigned = true;
		}
	}




	// access to columns
	// ========================================

	// getColumn
	// ---------------------------------------- 
	public function getColumn($key){
		/* Returns the DbColumn instance stored under the given name */
		if(!$this->hasColumn($key)) return NULL;
		return $this->cols[$key];
	}

	// hasColumn
	// ---------------------------------------- 
	public function hasColumn($key){
		/* Returns true if there is a DbColumn instance stored under the given name */
		return array_key_exists($key, $this->cols);
	}

	// setColumn
	// ---------------------------------------- 
	public function setColumn($key, $dtype, $default, $asFake=false){
		/* Creates a DbColumn instance with the given parameters and stores it in the
		* internal buffer */
		$key = strtolower($key);
		$ion = "column_".strtolower($this->name)."_".$key;
		$this->cols[$key] = new DbColumn($this, $ion, $key, $dtype, $default);
		if($asFake) $this->cols[$key]->isFake = true;
	}

	// unsetColumn
	// ---------------------------------------- 
	public function unsetColumn($key){
		/* Removes the DbColumn instance stored under the given name */
		unset($this->cols[$key]);
	}




	// return handling
	// ========================================

	// clearReturn
	// ---------------------------------------- 
	public function clearReturn($name=NULL) {
		/* Removes a registered return name */
		if(empty($name)){ 
			$this->returns = array(); 
			return;
		}
		if(!in_array($name, $this->returns)) return;
		$key = array_search($name, $this->returns);
		if($key === false) return;
		unset($this->returns[$key]);
	}

	// getReturn
	// ---------------------------------------- 
	public function getReturn($key) {
		/* Returns the stored return value for a given key from the 
		* last push */
		$returns = $this->getReturns();
		return $returns[$key];
	}

	// getReturns
	// ---------------------------------------- 
	public function getReturns() {
		/* Returns the stored return values from the last push */
		return $this->container;
	}

	// registerReturn
	// ---------------------------------------- 
	public function registerReturn($name) {
		/* Register a column to be returned at every update or insert statement */
		if(!in_array($name, $this->columns())) return;
		array_push($this->returns, $name);
	}

	// registerReturns
	// ---------------------------------------- 
	public function registerReturns($names) {
		/* Register a column to be returned at every update or insert statement */
		if(!is_array($names)) return;
		foreach($names as $name)
			$this->registerReturn($name);
	}

	// showReturns
	// ---------------------------------------- 
	public function showReturns() {
		/* Returns the name of the columns to be returned */
		return $this->returns;
	}




	// writing
	// ========================================

	// append
	// ---------------------------------------- 
	public function append($values) {
		/* Appends a new row (DbRow instance) to the table, built from the $values array.
		* Note it must contain the keys, which refer to the column names. Only keys that
		* correspond to a valid column name are considered. */
		$row = new DbRow($this, "appended", $values);
		$row->appended = true; // i.e. only in container now, but now yet pushed
		array_push($this->rows, $row);
		$this->last();
		if(!$this->db->autoPush) return;
		$this->push();
	}

	// delete
	// ---------------------------------------- 
	public function delete($config){
		$config->columns->value = array();
		$config->column = $this->pk->key;
		$this->separate($config);
		foreach($this as $row)
			$row->delete = true;
		if(!$this->db->autoPush) return;
		$this->push();
	}

	// lock
	// ---------------------------------------- 
	public function lock($mode=0) {
		/* Applies a lock to the table */
		$this->db->lockTable($this, $mode);
	}

	// push
	// ---------------------------------------- 
	public function push() {
		/* Pushes the local changes upstream to the data base. */
		$this->container = array();
		foreach($this->rows as $row){
			$row->push();
			array_push($this->container, $row->getReturns());
		}
	}

	// remove
	// ---------------------------------------- 
	public function remove($rowIdx){
		if(!array_key_exists($rowIdx, $this->rows)) return;
		$this->rows[$rowIdx]->delete;
	}

	// sync
	// ---------------------------------------- 
	public function sync() {
		/* Synchronizes data stored locally and upstream. */
		$this->push(); // preference to local changes by calling push first
		$this->pull(); 
	}

	// update
	// ---------------------------------------- 
	public function update($values, $config) {
		/* Updates the values of all rows selected according to a given DbConfig object */
		$config->columns = array_keys($values);
		$config->column  = $this->pk->key;
		$this->read($config, true);
		foreach($this as $row){
			foreach($values as $col=>$new)
				$row->getField($col)->setValue($new);
		}
		if(!$this->db->autoPush) return;
		$this->push();
	}
}




// DbHandler
// ============================================
class DbHandler {
	/* The instance that does most of the SQL handling regardless of the
	* specific implementation of SQL (in SQLite, things are a bit different
	* than in MySQL or OracleSQL); these functions are in the dedicated 
	* reader instances which are inherited from this mother class here */


	// members
	// ---------------------------------------- 
	public  $master   = NULL;
	public  $vb       = NULL;
	private $config   = NULL;
	private $views    = array();
	private $tables   = array();
	public  $reformat = array();
	private $conn     = NULL;
	private $parse    = NULL;
	private $exec     = NULL;
	public  $owner    = NULL;
	private $upper    = false;
	public  $autoPull = true;
	public  $autoPush = false;


	// __construct
	// ---------------------------------------- 
	public function __construct($master, $config) {
		/* Constructor */
		$this->master   = $master;
		$this->vb       = $master->vb;
		$this->config   = $config;
		$this->views    = array();
		$this->tables   = array();
		$this->reformat = array();
		$this->conn     = NULL;
		$this->parse    = NULL;
		$this->exec     = NULL;
		$this->owner    = NULL;
		$this->upper    = false; // true if only upper case SQL statements
		$this->start();
	}




	// access to views and tables
	// ========================================

	// __get
	// ---------------------------------------- 
	public function __get($key) {
		/* Retrieves a given view if it exists */
		return $this->get($key);
	}

	// __set
	// ---------------------------------------- 
	public function __set($key, $value) {
		/* Saves a new view in the list of views */
		$this->set($key, $value);
	}

	// __isset
	// ---------------------------------------- 
	public function __isset($key) {
		/* Checks if a key exists, either table or view */
		return $this->has($key);
	}

	// __unset
	// ---------------------------------------- 
	public function __unset($key) {
		/* Removes a view (unless it is also a table; tables cannot be removed) */
		$this->del($key);
	}

	// del
	// ---------------------------------------- 
	public function del($key) {
		/* Removes a view (unless it is also a table; tables cannot be removed) */
		if( array_key_exists($key, $this->tables)) return;
		if(!array_key_exists($key, $this->views )) return;
		unset($this->views[$key]);
	}

	// get
	// ---------------------------------------- 
	public function get($key) {
		/* Retrieves a given view if it exists */
		if(array_key_exists($key, $this->views )) return $this->views [$key];
		if(array_key_exists($key, $this->tables)) return $this->tables[$key];
		return NULL;
	}

	// has
	// ---------------------------------------- 
	public function has($key) {
		/* Checks if a key exists, either table or view */
		if(array_key_exists($key, $this->views )) return true;
		if(array_key_exists($key, $this->tables)) return true;
		return false;
	}

	// set
	// ---------------------------------------- 
	public function set($key, $value) {
		/* Saves a new view in the list of views */
		if(array_key_exists($key, $this->tables)) return;
		if(!is_a($value, "DbView")) return;
		$this->views[$key] = $value;
	}




	// general stuff
	// ========================================

	// close
	// ---------------------------------------- 
	private function close() {
		/* Closes the data base connection. */
		$this->undo();
		oci_close($this->conn);
	}

	// descriptor
	// ---------------------------------------- 
	public function descriptor($mode = OCI_D_LOB) {
		/* Adds a new descriptor with a given mode */
		return oci_new_descriptor($this->conn, $mode);
	}

	// end
	// ---------------------------------------- 
	public function end() {
		/* The end sequence. Pushes all remaining local changes
		* to the data base and closes the connection */
		if($this->config->has("dbFinalPush") && $this->config->dbFinalPush->value) 
			$this->push();
		$this->close();
	}

	// error
	// ---------------------------------------- 
	public function error() {
		/* Fetches the latest SQL error message */
		if($this->exec) return NULL;
		$err = oci_error($this->parse);
		return sprintf("Oracle Error #%d: %s", $err["code"], $err["message"]);
	}

	// loadColumns
	// ---------------------------------------- 
	//private function loadColumns() {
	//	/* Builds all the DbColumn instances for all tables */

	//	CH: this is the actual function
	//	however, the requests take SUPER long!
	//	use the loadColumns function below instead!

	//	$raw = $this->readSql("SELECT OWNER, TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_DEFAULT FROM ALL_TAB_COLUMNS WHERE OWNER IN ('ATLAS_MUON_NSW_MM_LOG', 'ATLAS_MUON_NSW_MM_QAQC', 'ATLAS_MUON_NSW_ELEC_QAQC', 'ATLAS_MUON_NSW_STGC_QAQC')"); 

	//	foreach($raw as $col){
	//		//print $col["owner"].",".$col["table_name"].",".$col["column_name"].",".$col["data_type"].",".$col["data_default"]."<br />"; // leave for fix
	//		$tablename = strtolower($col["table_name"]);
	//		if(!$this->has($tablename)) continue;
	//		$table = $this->get($tablename);
	//		if($table->owner!=strtolower($col["owner"])) continue;
	//		$table->setColumn($col["column_name"], sqlDataType($col["data_type"]), $col["data_default"]);
	//	}

	//	$sql = "SELECT cols.column_name FROM all_constraints cons, all_cons_columns cols WHERE cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner AND cons.constraint_type = 'P' AND cols.table_name = '%s' AND cols.owner = '%s'";
	//	$sql = $this->upper ? str_replace("%S", "%s", strtoupper($sql)) : strtolower($sql); 
	//	foreach($this->tables as $table){
	//		$rpk = $this->readSql(sprintf($sql, strtoupper($table->name), strtoupper($table->owner)));
	//		if(count($rpk)!=1) continue;
	//		$col = strtolower($rpk[0]["cols_column_name"]);
	//		if(!$table->hasColumn($col)) continue;
	//		$table->getColumn($col)->isPK = true;
	//		$table->assignPk();
	//	}
	//}

	// loadColumns
	// ---------------------------------------- 
	private function loadColumns() {
		/* Builds all the DbColumn instances for all tables */

		// fix because request is very very long!

		//// step 1: reading the info
		//$pks = array();
		//$sql = "SELECT cols.column_name, cols.table_name, cols.owner FROM all_constraints cons, all_cons_columns cols WHERE cons.constraint_name = cols.constraint_name AND cons.owner = cols.owner AND cons.constraint_type = 'P'";
		//$rpk = $this->readSql($sql);
		//foreach($rpk as $row){
		//	$pks[$row["cols_owner"].".".$row["cols_table_name"]] = $row["cols_column_name"];
		//}

		//$entries = array();
		//$tables  = array();

		//$raw = $this->readSql("SELECT OWNER, TABLE_NAME, COLUMN_NAME, DATA_TYPE, DATA_DEFAULT FROM ALL_TAB_COLUMNS WHERE OWNER IN ('ATLAS_MUON_NSW_MM_LOG', 'ATLAS_MUON_NSW_MM_QAQC', 'ATLAS_MUON_NSW_ELEC_QAQC', 'ATLAS_MUON_NSW_STGC_QAQC')"); 
		//foreach($raw as $col){
		//	$ispk = (array_key_exists($col["owner"].".".$col["table_name"], $pks) && $pks[$col["owner"].".".$col["table_name"]] == $col["column_name"]) ? "true" : "false";
		//	print $col["owner"].",".$col["table_name"].",".$col["column_name"].",".$col["data_type"].",".$col["data_default"].",".$ispk."<br />";
		//}

		//return;
		//// end of step 1

		// step 2: with the CSV file prepared:
		$path = str_replace("<theme>", $this->master->name=="index" ? "log" : $this->master->name, $this->config->pathColumns->value);
		$text = file_get_contents($path);
		$rows = explode("\n", $text);
		$raw = array();
		foreach($rows as $row){
			$cols = explode(",", $row);
			if(count($cols)<4) continue;
			$n = array("owner"=>$cols[0], "table_name"=>$cols[1], "column_name"=>$cols[2], "data_type"=>$cols[3], "data_default"=>$cols[4], "isPK"=>$cols[5]=="true" ? true : false);
			array_push($raw, $n);
		}

		foreach($raw as $col){
			$tablename = strtolower($col["table_name"]);
			if(!$this->has($tablename)) continue;
			$table = $this->get($tablename);
			if($table->owner!=strtolower($col["owner"])) continue;
			$table->setColumn($col["column_name"], sqlDataType($col["data_type"]), (strpos($col["data_default"], "empty_")!==false ? NULL : $col["data_default"]));
			if($col["isPK"]) $table->getColumn(strtolower($col["column_name"]))->isPK = true;
		}
		foreach($this->tables as $table)
			$table->assignPk();
	}

	// loadTables
	// ---------------------------------------- 
	private function loadTables() {
		/* Builds all the DbTable instances for all tables */
		//$raw1 = $this->readSql("SELECT OWNER, TABLE_NAME FROM ALL_TABLES WHERE OWNER IN ('ATLAS_MUON_NSW_MM_LOG', 'ATLAS_MUON_NSW_MM_QAQC', 'ATLAS_MUON_NSW_ELEC_QAQC', 'ATLAS_MUON_NSW_STGC_QAQC')"); 
		//$raw2 = $this->readSql("SELECT OWNER, VIEW_NAME FROM ALL_VIEWS WHERE OWNER IN ('ATLAS_MUON_NSW_MM_LOG', 'ATLAS_MUON_NSW_MM_QAQC', 'ATLAS_MUON_NSW_ELEC_QAQC', 'ATLAS_MUON_NSW_STGC_QAQC')");
		//$raw  = array_merge($raw1, $raw2);
		// fix because request is very very long!
		$path = str_replace("<theme>", $this->master->name=="index" ? "log" : $this->master->name, $this->config->pathTables->value);
		$text = file_get_contents($path);
		$rows = explode("\n", $text);
		$raw = array();
		foreach($rows as $row){
			$cols = explode(",", $row);
			if(count($cols)<2) continue;
			$n = array("table_name"=>$cols[1], "owner"=>$cols[0]);
			array_push($raw, $n);
		}
		// end of fix
		foreach($raw as $row){
			$name  = strtolower(array_key_exists("table_name", $row) ? $row["table_name"] : $row["view_name"]);
			$owner = strtolower($row["owner"]);
			//print $name.",".$owner."<br/>"; // leave for fix
			if(array_key_exists($name, $this->tables)){
				if($this->config->has("dbPrefOwner") && strpos(strtolower($this->config->dbPrefOwner->value), $owner)!==false) unset($this->tables[$name]); // need to clear ION and key
				else continue;
			}
			$table = new DbTable($this, $name);
			$table->owner = $owner;
			$this->tables[$name] = $table;
		}
	}

	// lockTable
	// ---------------------------------------- 
	public function lockTable($table, $mode=0) {
		/* Applies a lock to a table */
		if(!is_object($table) || get_class($table)!="DbTable") return;
		$as = $mode==0 ? "shared" : "exclusive";
		$sql = sprintf("lock table %s.%s in %s mode", $table->owner, $table->name, $as); 
		$this->writeSql($this->upper ? strtoupper($sql) : strtolower($sql));
	}

	// open
	// ---------------------------------------- 
	private function open() {
		/* Opens the data base connection. */
		$readOnly = $this->config->has("dbUseRO") ? $this->config->dbUseRO->value : false;
		$user = $readOnly ? $this->config->dbUserRO->value : $this->config->dbUserRW->value;
		$pswd = $readOnly ? $this->config->dbPswdRO->value : $this->config->dbPswdRW->value;
		$host = $readOnly ? $this->config->dbHostRO->value : $this->config->dbHostRW->value;
		//$tunnel  = shell_exec("ssh -fNg -L 10004:atlr1-v.cern.ch:10121 temming@lxplus.cern.ch"); 
		$this->conn = oci_connect($user, $pswd, $host, 'UTF8');
		if(!$this->conn){
			if(is_callable(array($this->vb, "error")))
				$this->vb->error("Cannot connect to data base ".$host."!");
			else
				exit("Cannot connect to data base ".$host."!");
		}
		$this->writeSql("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
		$this->owner    = $this->config->dbOwner->value;
		$this->upper    = $this->config->has("dbUpper"   ) ? $this->config->dbUpper   ->value : false;
		$this->autoPush = $this->config->has("dbAutoPush") ? $this->config->dbAutoPush->value : false;
		$this->autoPull = $this->config->has("dbAutoPull") ? $this->config->dbAutoPull->value : true;
		if($this->config->has("dbAutoSync")){
			$this->autoPush = $this->config->dbAutoSync->value;
			$this->autoPull = $this->config->dbAutoSync->value;
		}
	}

	// runChecks
	// ---------------------------------------- 
	private function runChecks() {
		/* Runs basic checks */
// FIXME
	}

	// setGlobalReformat
	// ---------------------------------------- 
	public function setGlobalReformat($key, $value) {
		/* Stores the $key that is to be reformatted into $value globally for all $configs */
		$this->reformat[$key] = $value;
	}

	// start
	// ---------------------------------------- 
	private function start() {
		/* The start sequence. Opens the data base connection, does basic
		* checks, loads and builds all internal objects. */
		$this->open       ();
		$this->runChecks  ();
		$this->loadTables ();
		$this->loadColumns();
	}




	// reading
	// ========================================

	// clear
	// ---------------------------------------- 
	public function clear($key=NULL) {
		/* Removes a DbView that is stored under the given name */
		if     (empty($key)                         ) $this->views = array();
		else if(array_key_exists($key, $this->views)) unset($this->views[$key]);
	}

	// numConfig
	// ---------------------------------------- 
	public function numConfig($config, $tablename=NULL) {
		/* Reads data from the data base according to the info stored in a configuration.
		* The method is similar to readConfig but instead of the data it returns the
		* number of affected rows. */
		$sql = $this->sqlFromConfig($config, $tablename);
		return $this->numRows($sql);
	}

	// numRows
	// ---------------------------------------- 
	public function numRows($sql, $attrib=NULL) {
		/* Executes an Oracle SQL read statement but only returns the number of affected rows
		* discarding the actual selected content. This is desirable for selections that will
		* affect a large set of data which in the readSql method will cause a memory overload. 
		* Actually also works with write statements, but only recommended for proficient users! */
		if(empty($attrib )) $attrib  = OCI_ASSOC+OCI_RETURN_NULLS;
		$s   = "select count(*) as count from ";
		$sql = sprintf(($this->upper ? strtoupper($s) : $s)." (%s)", $sql);
		//print "<b>COUNTING:</b> ".$sql."<br />";
		if($this->master->globals["username"]=="temming") print "<b>COUNTING:</b> ".$sql."<br />";
		$this->parse = oci_parse  ($this->conn, $sql);
		$this->exec  = oci_execute($this->parse);
		$result = array();
		oci_fetch_all($this->parse, $result);
		return $result["COUNT"][0];
	}

	// pull
	// ---------------------------------------- 
	public function pull() {
		/* Pulls all local changes from the upstream data base. */
		foreach($this->views  as $view ) $view ->pull();
		foreach($this->tables as $table) $table->pull();
	}

	// read
	// ---------------------------------------- 
	public function read($viewname, $config=NULL, $reset=false) {
		/* Creates (if does not exist) a view and applies the config and returns the view */
		if(!$this->has($viewname)) {
			$this->views[$viewname] = new DbView($this, $viewname, $config);
			return $this->views[$viewname];
		}
		$view = $this->get($viewname);
		$view->read($config, $reset);
		return $view;	
	}

	// readConfig
	// ---------------------------------------- 
	public function readConfig($config, $tablename=NULL) {
		/* Reads data from the data base according to the info stored in a configuration.
		* Since the configs for a single table does not contain the table name, it must be
		* given separately. If it is NULL, the config better contains it, otherwise return. */
		$sql   = $this->sqlFromConfig($config, $tablename);
		$limit = oracle11Limit($sql, $config, $this->upper); // annoying Oracle < 12 compatibility
		return $this->readSql($sql, array(), NULL, $limit);
	}

	// readSql
	// ---------------------------------------- 
	public function readSql($sql, $columns=array(), $attrib=NULL, $limit=NULL) {
		/* Executes an Oracle SQL read statement and returns the result in form of a 2D array.
		* Note this method (1) always appends also the index of the row, (2) the 2D array
		* is built as key=>value: first dimension key is the row index (_not_ the PK!) and second
		* dimension key is the field name (adding 'the_pk' as an artificial field must be done 
		* beforehand in the building of the $sql statement). */
		if(empty($attrib )) $attrib  = OCI_ASSOC+OCI_RETURN_NULLS;
		if(empty($columns)) $columns = $this->getColumnnames($sql); // the ones for the return
		$rawnames = $this->getColumnnames($sql, true); // the ones in the SQL statement
		$sql = !empty($limit) ? $limit : $sql; // annoying Oracle < 12 compatibility
		$sql = str_replace($this->upper ? " AS " : " as ", " ", $sql); // feature of Oracle, but " AS " is necessary in the decoding functions
		//print "<b>READING</b> ".$sql."<br />";
		if(isset($this->master->globals["username"]) && $this->master->globals["username"]=="temming") print "<b>READING:</b> ".$sql."<br />";
		$this->parse = oci_parse  ($this->conn, $sql);
		$this->exec  = oci_execute($this->parse);
		$result = array();
		$idx    = 0;
		while($row = oci_fetch_array($this->parse, $attrib)){
			$values = array("_idx"=>$idx);
			for($i=0;$i<count($rawnames);++$i)
				$values[strtolower($columns[$i])] = $row[$this->upper ? strtoupper($rawnames[$i]) : strtolower($rawnames[$i])];
			array_push($result, $values);
			++$idx;
		}
		return $result;
	}

	// readTable
	// ---------------------------------------- 
	public function readTable($tablename, $columns=array(), $select=array(), $reformat=array(), $order=array(), $slim=false) {
		/* Method to read a table in a single line; obviously not as powerful as
		* a full fledged read statement with manual building of the cfg; returns 
		* the pointer to the table */
		$table = $this->get($tablename);
		if(empty($table)) return;
		$cfg = new DbConfig($this->master, "readTable_".timestamp());
		$cfg->columns = $columns;
		foreach($select as $key=>$val)
			$cfg->select($key, $val);
		foreach($reformat as $key=>$val)
			$cfg->reformat($key, $val);
		foreach($order as $key=>$val){
			$cfg->order($key, !empty($val) && in_array($val, array("asc", "desc")) ? $val : "asc");
		}
		if($slim) $cfg->slim = true;
		$table->read($cfg);
		//$table->separate($cfg);
		return $table;
	}

	// readView
	// ---------------------------------------- 
	public function readView($viewname, $tables, $columns=array(), $select=array(), $reformat=array(), $order=array(), $slim=false) {
		/* Method to read a view in a single line; obviously not as powerful as
		* a full fledged read statement with manual building of the cfg; returns 
		* the pointer to the view; note: this should only be used for proper views
		* (i.e. joining tables) */
		$view = $this->get($viewname);
		if(!empty($view) && $view->isTable) return NULL;
		$cfg = new DbConfig($this->master, "readView_".timestamp());
		$cfg->columns = $columns;
		foreach($tables as $val)
			$cfg->joinon = $val;
		foreach($select as $key=>$val)
			$cfg->select($key, $val);
		foreach($reformat as $key=>$val)
			$cfg->reformat($key, $val);
		foreach($order as $key=>$val)
			$cfg->order($key, !empty($val) && in_array($val, array("asc", "desc")) ? $val : "asc");
		if($slim) $cfg->slim = true;
		if(!empty($view)) {
			$view->read($cfg);
			//$view->separate($cfg);
			return $view;
		}
		$this->read($viewname, $cfg);
		return $this->get($viewname);
	}

	// separate
	// ---------------------------------------- 
	public function separate($viewname, $config=NULL) {
		/* Creates (if does not exist) a view and applies the config and returns the view */
		if(!$this->has($viewname)) {
			$this->views[$viewname] = new DbView($this, $viewname, $config);
			return $this->views[$viewname];
		}
		$view = $this->get($viewname);
		$view->separate($config);
		return $view;	
	}




	// writing
	// ========================================

	// commit
	// ---------------------------------------- 
	public function commit() {
		/* Commits all changes. */
		if($this->master->vb->state == 3) return;
		//print "commit!";
		oci_commit($this->conn);
	}

	// push
	// ---------------------------------------- 
	public function push() {
		/* Pushes all local changes upstream to the data base. */
		foreach($this->tables as $table) $table->push();
	}

	// sync
	// ---------------------------------------- 
	public function sync() {
		/* Synchronizes the local and upstream data. */
		foreach($this->views  as $view ) $view ->sync();
		foreach($this->tables as $table) $table->sync();
	}

	// undo
	// ---------------------------------------- 
	public function undo() {
		/* Undoes the last changes up to the last commit. */
		//print "rollback!";
		oci_rollback($this->conn);
	}

	// writeSql
	// ---------------------------------------- 
	public function writeSql($sql, &$handlers=array(), $attrib=OCI_DEFAULT) {
		/* Executes an Oracle SQL write statement and returns true if the execution was successful;
		* note any bindings or return values must be dealt with before and after this method! */
		//print "<b>WRITING:</b> ".$sql."<br />";
		if(isset($this->master->globals["username"]) && $this->master->globals["username"]=="temming") print "<b>WRITING:</b> ".$sql."<br />";
		$this->parse = oci_parse($this->conn, $sql);
		foreach($handlers as $handler){
			$key = $this->upper ? strtoupper($handler->short) : strtolower($handler->short);
			if($handler->dtype==OCI_B_BLOB)
				oci_bind_by_name($this->parse, $key, $handler->link, -1, $handler->dtype);
			else
				oci_bind_by_name($this->parse, $key, $handler->link, $handler->dtype);
		}
		$this->exec = oci_execute($this->parse, $attrib);
		return $this->exec;
	}




	// technology-dependent assembler functions (DbConfig/DbHandle/DbRow => full SQL)
	// ========================================

	// setHandlers
	// ---------------------------------------- 
	public  function setHandlers($row, $inclPk = true){
		/* Builds the list of DbHandle instances for a given row */

		$handlers = array();
		if(!$row->view->isTable) return $handlers;
		if(!$row->updated && !$row->appended) return $handlers;

		// primary key
		if($inclPk)
			array_push($handlers, new DbHandle($row, $row->pk, ":pk", SQLT_LNG)); 

		// files
		$i = 0;
		foreach($row as $key=>$field){
			if(!$field->updated && !$row->appended) continue; 
			if(!is_object($field->value)) continue;
			if(get_class($field->value)!="DbFile") continue;
			array_push($handlers, new DbHandle($row, $field, ":blob".strval($i), OCI_B_BLOB));
			++$i;
		}

		// other columns
		$i = 0;
		foreach($row as $key=>$field){
			if(!$field->updated && !$row->appended) continue; 
			if(!in_array($field->column->key, $row->view->showReturns()) && empty($field->incSeq)) continue;
			array_push($handlers, new DbHandle($row, $field, ":return".strval($i), SQLT_LNG)); 
			++$i;
		}

		return $handlers;
	}

	// sqlDelete
	// ---------------------------------------- 
	public function sqlDelete($row, $config=NULL) {
		/* Generate an SQL delete statement for a DbRow */
		if(!$row->view->isTable) return NULL;
		$sql = $this->upper ? "DELETE FROM %s %s" : "delete from %s %s";
		if(empty($config)){
			$pkcol = $row->view->pk->key;
			$pk    = $row->get($pkcol);
			$pkcol = $this->upper ? strtoupper($pkcol) : strtolower($pkcol);
			$where = sprintf(($this->upper ? "WHERE" : "where")." %s=%d", $pkcol, $pk);
		}
		else {
			$where = $this->decodeWhereStatement($config);
		}
		$table = $this->upper ? strtoupper($this->owner.".".$row->view->name) : strtolower($this->owner.".".$row->view->name);
		return sprintf($sql, $table, $where);
	}

	// sqlFromConfig
	// ---------------------------------------- 
	private function sqlFromConfig($config, $tablename=NULL) {
		/* Reads data from the data base according to the info stored in a configuration.
		* Since the configs for a single table does not contain the table name, it must be
		* given separately. If it is NULL, the config better contains it, otherwise return. */

		// add the primary key for tables
		if(count($config->joinon->value)==0 && !(count($config->columns->value)==0 || (count($config->columns->value)==1 && $config->columns->value[0]=="*"))){
			$dont = false;
			foreach($config->columns->value as $col) {
				$use = $config->getReformat(strtolower($col));
				if(empty($use)) $use = $col; 
				if(strpos($use, "(")===false) continue;
				$dont=true; 
				break;
			}
			if(!$dont) {
				$table = $this->get($tablename ? $tablename : $config->table->value);
				if(!empty($table) && !empty($table->pk)){
					$pk = $table->pk->key;
					if(!empty($pk) && !in_array($pk, $config->columns->value)) {
						$config->column = "the_pk";
						$config->reformat($pk, "the_pk");
					}
				} 
			} 
		}
		// build the sql
		return $this->sqlSelect($config, $tablename);
	}

	// sqlHandlers
	// ---------------------------------------- 
	private function sqlHandlers($handlers){
		/* Generate the part of the SQL insert or update statement
		* where the handlers are linked to the data base */
		if(count($handlers)==0) return "";
		$sql   = $this->upper ? " RETURNING %s INTO %s" : " returning %s into %s";
		$names = array();
		$keys  = array();
		foreach($handlers as $handler){
			$col = empty($handler->field) ? $handler->row->view->pk->key : $handler->field->column->key;
			array_push($names, $this->upper ? strtoupper($col           ) : strtolower($col           ));
			array_push($keys , $this->upper ? strtoupper($handler->short) : strtolower($handler->short));
		}
		return sprintf($sql, implode(", ", $names), implode(", ", $keys));
	}

	// sqlInsert
	// ---------------------------------------- 
	public function sqlInsert($row, $handlers=array()) {
		/* Generate an SQL insert statement for a DbRow */
		if(!$row->view->isTable) return NULL;
		$sql    = $this->upper ? "INSERT INTO %s (%s) VALUES (%s)" : "insert into %s (%s) values (%s)";
		$names  = array();
		$values = array();
		$pkcol  = $row->view->pk->key;
		$keys   = array_keys($row->cache);
		if(!in_array($pkcol, $keys)){
			array_push($names , $this->upper ? strtoupper($pkcol) : strtolower($pkcol));
			array_push($values, $this->owner.".incONE_".strtoupper($row->view->name).".nextval");
		} 
		foreach($row as $key=>$field){
			if(!$field->isValid) continue; 
			if(!empty($field->incSeq)){
				array_push($names , $this->upper ? strtoupper($key) : strtolower($key));
				array_push($values, $this->owner.".".$field->incSeq);
				continue;
			}
			$use = (is_object($field->value) && get_class($field->value)=="DbFile") ? "empty_blob()" : $this->decodeTicks($field->value);
			array_push($names , $this->upper ? strtoupper($key) : strtolower($key));
			array_push($values, $use);
		}
		$table = $this->upper ? strtoupper($this->owner.".".$row->view->name) : strtolower($this->owner.".".$row->view->name);
		$sql   = sprintf($sql, $table, implode(", ", $names), implode(", ", $values));
		return $sql.$this->sqlHandlers($handlers);
	}

	// sqlSelect
	// ---------------------------------------- 
	private function sqlSelect($config, $tablename=NULL, $slim=false) {
		/* Generate an SQL select statement using a DbConfig object */

		if(empty($config) || get_class($config)!="DbConfig") return "";

		// apply global reformat first
		foreach($this->reformat as $key=>$val){
			if($config->has($key)) continue;
			$config->reformat($key, $val);
		}

		// proper version
		$sql = $this->upper ? "%s SELECT %s %s %s FROM %s %s %s %s %s %s %s %s %s %s %s" : "%s select %s %s %s from %s %s %s %s %s %s %s %s %s %s %s";

		$raw1    = $this->decodeRawStatement  ($config, 1);
		$raw2    = $this->decodeRawStatement  ($config, 2);
		$columns = $this->decodeColumnnames   ($config, $tablename);
		$raw3    = $this->decodeRawStatement  ($config, 3);
		$raw4    = $this->decodeRawStatement  ($config, 4);
		$tables  = $this->decodeTablenames    ($config, $tablename);
		$raw5    = $this->decodeRawStatement  ($config, 5);
		$where   = $this->decodeWhereStatement($config);
		$raw7    = $this->decodeRawStatement  ($config, 7);
		$group   = $this->decodeGroupStatement($config);
		$raw9    = $this->decodeRawStatement  ($config, 9);
		$order   = $this->decodeOrderStatement($config);
		$raw11   = $this->decodeRawStatement  ($config, 11);
		$limit   = "";//$this->decodeLimitStatement($config); // annoying Oracle < 12 compatibility
		$raw0    = $this->decodeRawStatement  ($config, 0);

		// returning sql
		$sql = sprintf($sql, $raw1, $raw2, $columns, $raw3, $raw4, $tables, $raw5, $where, 
		                     $raw7, $group, $raw9, $order, $raw11, $limit, $raw0);
		$sql = trim(preg_replace('!\s+!', ' ', $sql));
		return $sql;
	}

	// sqlUpdate
	// ---------------------------------------- 
	public function sqlUpdate($row, $handlers=array()) {
		/* Generate an SQL update statement for a DbRow */
		if(!$row->view->isTable) return NULL;
		$sql       = $this->upper ? "UPDATE %s SET %s WHERE %s" : "update %s set %s where %s";
		$toWrite   = array();
		$available = array();
		foreach($row as $key=>$field){
			array_push($available, $field->column->key);
			if(!$field->updated      ) continue; 
			if(!$field->isValid      ) continue; 
			if($field->column->isFake) continue; 
			$use = (is_object($field->value) && get_class($field->value)=="DbFile") ? "empty_blob()" : $this->decodeTicks($field->value);
			array_push($toWrite, ($this->upper ? strtoupper($field->column->key) : strtolower($field->column->key))."=".$use);
		}
		// setting missing/deleted fields to default
		$missing = array_diff($row->columns(), $available);
		foreach($missing as $colname)
			array_push($toWrite, $colname."=".$this->decodeTicks($row->view->columnByName($colname)->default));
		if(count($toWrite)<1) return NULL;
		// returning sql
		$sql = sprintf($sql, $this->upper ? strtoupper($this->owner.".".$row->view->name) : strtolower($this->owner.".".$row->view->name), implode(", ", $toWrite), ($this->upper ? strtoupper($row->view->pk->key) : strtolower($row->view->pk->key))."=".$row->get($row->view->pk->key));
		return $sql.$this->sqlHandlers($handlers);
	}




	// technology-dependent retrieval functions (full SQL => plain php)
	// ========================================

	// getColumnnames
	// ---------------------------------------- 
	private function getColumnnames($sql, $getRawSql=false, $tables=array(), $shorts=array()){
		/* Extracts the column names from a full SQL statement. This function
		* is intended _after_ the decodeColumnnames method, or at least 
		* to be run only for properly parsed SQL statements. The names returned
		* here are the proper format that is required to extend the field name to 
		* an ION. */
		$sqlu   = strtoupper($sql);
		$isJoin = (strpos($sql, " JOIN ")!==false); 
		$list   = extractColumnsSelect($sqlu);
		if(count($list)==0) return NULL;
		if(count($tables)==0 || count($shorts)==0){ 
			$tables = array();
			$shorts = array();
			$this->getTablenames($sql, $tables, $shorts);
		}
		$raw = array();
		foreach($list as $coldef){
			// plug in all columns from the table if necessary
			if(strpos($coldef, "*")!==false){
				$short = strtok($coldef, ".");
				$table = $this->get(strtolower($tables[array_search($short, $shorts)]));
				foreach($table->columns() as $colname)
					array_push($raw, $coldef=="*" || !$isJoin ? $colname : strtolower($short).".".$colname);
				continue;
			}
			// only use the abbreviation
			if(strpos($coldef, " AS ")!==false){
				array_push($raw, substr($coldef, strpos($coldef, " AS ")+4));
				continue;
			}
			// nothing to do
			array_push($raw, $coldef);
		}
		$columns = array();
		foreach($raw as $col){
			$proper = trim(strtolower($col));
			if($getRawSql){
				if(strpos($proper, ".")!==false) {
					$elms   = explode(".", $proper);
					$proper = trim($elms[1]);
				}
			}
			else {
				$proper = str_replace("(", "_", $proper);
				$proper = str_replace(")", "_", $proper);
				$proper = str_replace(".", "_", $proper);
			}
			$proper = stripLeft($proper, $this->config->has("dbIgnore") ? array_map("strtolower", $this->config->dbIgnore->value) : array());
			$proper = trim($proper, "_");
			array_push($columns, $proper);
		}
		return $columns;
	}
	
	// getTablenames
	// ---------------------------------------- 
	private function getTablenames($sql, &$tables, &$shorts){
		/* Retrieves the table names and abbreviations in plain 
		* php format from an full SQL statement */
		$sqlwords = array("LIMIT", "ORDER", "WHERE", "ON", "LEFT", "RIGHT", "FULL", "INNER", "OUTER");
		$sqlu = strtoupper($sql);
		// only a single table
		if(strpos($sqlu, " JOIN ")===false){
			$words = explode(" ", $sqlu);
			$from  = in_array("FROM", $words) ? array_search("FROM", $words) : -1; // first element is the table already in absence of "from"
			$table = $words[$from+1];
			if(strpos($table, ".")!==false) {
				$elms  = explode(".", $table);
				$table = $elms[1];
			}
			array_push($tables, $table);
			$short = count($words)>$from+2 && !in_array($words[$from+2], $sqlwords) ? $words[$from+2] : NULL;
			array_push($shorts, $short ? $short : $table);
			return;
		} 
		// join statement
		$sqlu  = substr($sqlu, strpos($sqlu, " FROM ")+6); // only trailing part
		$parts = explode(" JOIN ", $sqlu);
		foreach($parts as $part){
			$this->getTablenames($part, $tables, $shorts);
		}
	}




	// technology-dependent decoder functions (DbConfig => parts of SQL)
	// ========================================

	// decodeColumnnames
	// ---------------------------------------- 
	private function decodeColumnnames($config, $tablename=NULL) {
		/* Generate the list of fields to be selected with a
		* DbConfig object; this also accounts for joins (i.e. columns
		* from multiple tables), the use of * (to select all columns
		* of a table), as well as it adds the columns that appear in
		* the reformat lists of the config object unless they are 
		* already given in the list of columns. */
		$columns     = $config->columns->value;
		$columnnames = array(); // proper columns
		$rawnames    = array(); // raw as given by the user
		$tables      = array(); // proper tables
		$shorts      = array(); // raw as given by the user
		$useAll      = array(); // name of every table where we want all columns
		// single table
		if(count($config->joinon->value)==0){
			if     ($tablename                   ) array_push($tables, $tablename           );
			else if(!empty($config->table->value)) array_push($tables, $config->table->value);
			$tableToUse = $tablename ? $tablename : $config->table->value;
			// all columns
			if(count($columns)==0 || (count($columns)==1 && $columns[0]=="*")){
				array_push($columnnames, $tableToUse.".*");
				array_push($useAll     , $tableToUse     );
			}	
			// several columns
			else{
				foreach($columns as $col){
					$clist = explode(" ", $col);
					$cname = end($clist); // accounting for keywords like 'distinct <col name>'
					array_push($rawnames   , $cname);
					array_push($columnnames, trim(implode(" ", array_slice($clist, 0, -1)) ." ". ($cname=="*" ? $tableToUse.".*" : $this->decodeReformatColumn($config, $cname))));
				}
			}
		}
		// multiple tables, several columns
		else {
			// extracting tables
			$this->extractTablenames($config, $shorts, $tables);
			// all columns
			if(count($columns)==0 || (count($columns)==1 && $columns[0]=="*")){ 
				foreach($shorts as $short)
					array_push($columnnames, sprintf("%s.*", $short));
				foreach($tables as $table)
					array_push($useAll     , $table);
			}
			// several columns
			else{
				foreach($columns as $col){
					$clist = explode(" ", $col);
					$cname = end($clist);
					$pure  = strpos($cname, ".")!==false ? substr($cname, strpos($cname, ".")+1) : $cname;
					array_push($rawnames   , $cname);
					array_push($columnnames, trim(implode(" ", array_slice($clist, 0, -1)) ." ". ($pure=="*" ? $cname : $this->decodeReformatColumn($config, $cname))));
				}
			}
		}
		// also appending reformatted columns (they need to be automatically included)
		// logic:
		// * in case the reformat is not a function call, append the reformat if it hasn't been appended yet
		// * in case the reformat is     a function call, append the reformat if it hasn't been appended yet
		//                                                   and the original if it hasn't been appended yet
		//print "HERE!<br />";
		//print_r($rawnames);
		//print_r($columnnames);
		//print "<br />-------------<br />";

		foreach($config->getByType("dbreformat") as $entry){
			$keys = array_keys($this->tables);
			if(in_array($entry->value, $keys)) continue;
			// only redefinition
			if(strpos($entry->value, "(") === false){
				if(in_array($entry->repl, $rawnames)) continue; 
				//print "pushing ".$entry->name." ".$entry->value." ".$this->decodeReformatColumn($config, $entry->repl)."<br />";
				array_push($columnnames, $this->decodeReformatColumn($config, $entry->repl));
			}
			// function call
			else {	
				// adding the functional expression itself
				if(!in_array($entry->repl, $rawnames))
					array_push($columnnames, $this->decodeReformatColumn($config, $entry->repl));
				continue; // this we do NOT yet do cause it could contradict functional expressions..
				// adding also the columns within in the functional expression
				$raw  = $entry->value;
				$cols = extractColumns($raw, $tables, true);
				$cols = array_merge($cols, extractColumns($raw, $shorts, true));
				if(count($cols)==0) continue;
				foreach($cols as $col){
					if(in_array($col   , $rawnames   )) continue; // already used because in DbConfig::columns member
					$proper = $this->decodeReformatColumn($config, $col);
					if(in_array($proper, $columnnames)) continue; // already added because of other functional expression
					array_push($columnnames, $proper);
				}	
			}	
		}
		$sql = strtolower(implode(", ", $columnnames));
		if($this->upper) $sql = strtoupper($sql);
		return $sql;
	}

	// decodeGroupStatement
	// ---------------------------------------- 
	private function decodeGroupStatement($config){
		/* Generates the 'GROUP BY ...' statements of an SQL SELECT query
		* from a given DbConfig object */
		$groups = array();
		foreach($config->getByType("dbgroup") as $entry)
			array_push($groups, $this->upper ? strtoupper($entry->key) : strtolower($entry->key));
		if(count($groups)==0) return "";
		$raw = $this->decodeRawStatement($config, 8)." ";
		$sql = ($this->upper ? "GROUP BY " : "group by ").$raw.implode(", ", $groups);
		return $sql;
	}

	// decodeJoinEntity
	// ---------------------------------------- 
	private function decodeJoinEntity($config, $short){
		/* For a given table abbreviation $short, it returns the 
		* proper full (or long) statement in the format needed by
		* the SQL join command */
		// return null if empty
		if(empty($short)) return NULL;
		// link to a joinconfig
		if(substr($short, 0, 1)=="*"){
			$cnames = array();
			foreach($config->joinconfig->value as $cfg) array_push($cnames, $cfg->key);
			$key = substr($short, 1);
			if(!in_array($key, $cnames)) return $short;
			$idx = array_search($key, $cnames);
			return sprintf("(%s) %s", $this->sqlSelect($config->joinconfig->value[$idx], NULL, true), $key);
		}
		// link to a table
		$table = $this->decodeReformatTable($config, $short); // will be "<tablename> <short>" if reformat
		return $table;                                        // and "<short>" if no reformat
	}

	// decodeJoinStatement
	// ---------------------------------------- 
	private function decodeJoinStatement(&$tables, $config, $statement, $isFirst=false){
		/* Decodes a joinon statement from the configuration also
		* taking into account the possible joinconfigs entries of
		* said configuration */
		$sql       = $config->jointype->value." JOIN %s ON %s";
		$statement = strtolower(str_replace(" ", "", $statement));
		$halfs     = explode("=", $statement);
		$short1    = strpos($halfs[0], ".")!==false ? trim(strtok($halfs[0], ".")) : NULL;
		$short2    = strpos($halfs[1], ".")!==false ? trim(strtok($halfs[1], ".")) : NULL;
		if(empty($short1) && empty($short2)) return NULL; // one can be a variable, but not both
		if($short1 == $short2) return NULL;
		if(strpos($short1, "*")!==false && strpos($short2, "*")!==false) return NULL;
		$isJoin    = (strpos($short1, "*")!==false ? 1 : (strpos($short2, "*")!==false ? 2 : 0)); // if 1 (2) then left (right) side is joinconfig
		$long1     = $this->decodeJoinEntity($config, $short1);
		$long2     = $this->decodeJoinEntity($config, $short2);
		// start
		// within these commented lines, we may wanna use $short1/2 instead of $table1/2
		//$table1    = $isJoin==1 ? "" : (strtok($long1, " ") ? strtok($long1, " ") : $long1);
		//$table2    = $isJoin==2 ? "" : (strtok($long2, " ") ? strtok($long2, " ") : $long2);
		//if($table1 == $table2) return NULL;
		if(in_array($short1, $tables) && in_array($short2, $tables)) return NULL; 
		$new       = 0;
		if($isJoin!=1 && !empty($short1) && !in_array($short1, $tables)) { 
			$new = $new + 1;
			array_push($tables, $short1); 
		}
		if($isJoin!=2 && !empty($short2) && !in_array($short2, $tables)) { 
			$new = $new + 2; 
			if(!$isFirst || count($tables)<1) array_push($tables, $short2);
		}
		// end
		if($new==3 && $isFirst  ) $new = 1;       // prefer first table if it is the first statement
		if($new==0 && $isJoin!=0) $new = $isJoin; // use the join statement as new entity
		if($new<=0 || $new >= 3) return NULL;
		$entity = $new==1 ? $long1 : $long2;
		$sqlu   = sprintf($sql, $entity, $statement);
		if($isFirst) $sqlu = $entity; // print table alone if it is the first entry
		else if($isJoin!=0) {
			//bit more complicated for joinconfig: '(select <list of columns> from <table>) <key> on <key>.<column> = <tab>.<column>'
			$stmt = $statement;
			if($isJoin==2) $stmt = implode(" = ", array_map("trim", array_reverse(explode("=", $stmt)))); // turn around
			$sqlu = sprintf($sql, $entity, substr(trim($stmt), 1)); 
		}
		return $this->upper ? strtoupper($sqlu) : strtolower($sqlu);
	}	

	// decodeLimitStatement
	// ---------------------------------------- 
	private function decodeLimitStatement($config){
		/* Generates the 'LIMIT ...' statements of an SQL SELECT query
		* from a given DbConfig object */
		if($config->limit->value == 0) return "";
		if($config->limit->offset->value == 0)
			$sql = strtolower(sprintf("FETCH FIRST %d ROWS ONLY", $config->limit->value));
			//$raw = " ".$this->decodeRawStatement($config, 12);
			//$sql = strtolower(sprintf("LIMIT %d", $config->limit->value));
		else
			$sql = strtolower(sprintf("OFFSET %d ROWS FETCH NEXT %d ROWS ONLY", $config->limit->offset->value, $config->limit->value));
			if($this->upper) $sql = strtoupper($sql);
		return $sql;
	}

	// decodeOrderStatement
	// ---------------------------------------- 
	private function decodeOrderStatement($config){
		/* Generates the 'ORDER BY ...' statements of an SQL SELECT query
		* from a given DbConfig object */
		$dborders = $config->getByType("dborder");
		if(count($dborders)==0) return "";
		$oooorder = array(); // dedicated to J. Bercow
		foreach($dborders as $entry)
			array_push($oooorder, sprintf("%s %s", $entry->key, $entry->value));
		$keys  = array_keys($dborders);
		$first = $dborders[$keys[0]];
		$raw   = $this->decodeRawStatement($config, 10);
		$sql   = sprintf("ORDER %s BY", $first->has("add") ? $first->add->value : "");
		$sql   = $this->upper ? strtoupper($sql) : strtolower($sql);
		$sql  .= " ".$raw." ";
		$sql  .= $this->upper ? strtoupper(implode(", ", $oooorder)) : strtolower(implode(", ", $oooorder));
		return $sql;
	}

	// decodeRawStatement
	// ---------------------------------------- 
	private function decodeRawStatement($config, $position=0){
		/* Places all raw statements at the desired position */
		$dbraws = $config->getByType("dbraw");
		if(count($dbraws)==0) return "";
		$raws = array();
		foreach($dbraws as $entry){
			if($entry->pos->value!=$position) continue;
			array_push($raws, $entry->value);
		}
		return implode(" ", $raws);
	}

	// decodeReformatColumn
	// ---------------------------------------- 
	private function decodeReformatColumn($config, $short){
		/* Applies a reformat of a column name as stored in the DbConfig 
		* object $config to a value $short and returns it in the way 
		* required by SQL */
		$column = $config->getReformat(strtolower($short));
		$sql = strtolower($column ? sprintf("%s as %s", strtolower($column), $short) : $short);
		if($this->upper) $sql = strtoupper($sql);
		return $sql;
	}

	// decodeReformatTable
	// ---------------------------------------- 
	private function decodeReformatTable($config, $short){
		/* Applies a reformat of a table name as stored in the DbConfig 
		* object $config to a value $short and returns it in the way 
		* required by SQL */
		$ss    = preg_replace('/[0-9]+/', '', $short); // can be et2 if multiple et are joined
		$long  = $config->getReformat($ss);
		$table = $this->get(strtolower(!empty($long) ? $long : $short));
		if(empty($table)) return NULL;
		$res = strtoupper(sprintf("%s.%s", $table->owner, $table->name));
		return !empty($long) ? $res." ".$short : $res;
	}

	// decodeTablenames
	// ---------------------------------------- 
	private function decodeTablenames($config, $tablename=NULL){
		/* Extracts the names of the tables to be selected from in an
		* SQL query, including join statements */
		// single table
		if(count($config->joinon->value)==0 && (!empty($tablename) || !empty($config->table->value))){
			$table = NULL;
			if(!empty($tablename)) $table = $this->get(strtolower($tablename           ));
			else                   $table = $this->get(strtolower($config->table->value));
			if(empty($table)) return NULL;
			return strtoupper($table->owner.".".$table->name);
		}
		// join tables
		else if(count($config->joinon->value)>0){
			$tables = array();
			$jstmt  = array($this->decodeJoinStatement($tables, $config, $config->joinon->value[0], true));
			foreach($config->joinon->value as $join){
				$joinon = $this->decodeJoinStatement($tables, $config, $join);
				if(empty($joinon)) continue;
				array_push($jstmt, $joinon);
			}
			$sql = strtolower(implode(" ", $jstmt));
			//print "result: ".print_r($jstmt)."<br />";
			return $this->upper ? strtoupper($sql) : strtolower($sql);
		}
		// empty
		return NULL;
	}

	// decodeTicks
	// ---------------------------------------- 
	private function decodeTicks($variable){
		/* Returns the string version of this variable with ticks
		* or without ticks, depending on the original type */
		if(empty($variable) && $variable!==0 && $variable!==0.0) return "NULL";
		$type = gettype($variable);
		if(in_array($type, array("double", "integer"))) return strval($variable);
		if(substr($variable, 0, 12)=="TO_TIMESTAMP") return $variable;
		if($type=="boolean") return $variable ? "'T'" : "'F'";
		return sprintf("'%s'", strval($variable));
	}

	// decodeWhereEntity
	// ---------------------------------------- 
	private function decodeWhereEntity($entry){
		/* Builds the smallest entities of the where statement for
		* every entry in the config, i.e. <column>=<value> in case of 
		* type 'eq'; note: dbselect entries will have name=<something unique>,
		* value=<column name>, and the actual select in the option string, 
		* e.g. 'eq:=<value>' */
		if(empty($entry)) return NULL;
		if(count($entry->cache)==0) return NULL;
		// added with value NULL, thus, using eq directly
		if(empty($entry->value)){
			return $entry->has("eq") ? $entry->get("eq") : NULL;
		}
		$col = $this->upper ? strtoupper($entry->value) : strtolower($entry->value);
		$col = $col=="ROWNUM" ? "rownum" : $col;
		// selecting with another DbConfig object
		if($entry->has("config")){
			$cfg = $this->master->getObj("DbConfig", $entry->get("config")->value);
			if(!$cfg) return NULL;
			return sprintf("%s = (%s)", $col, $this->sqlSelect($cfg));
		}
		// normal equality and nonequality statements
		if($entry->has("eq") || $entry->has("neq")){
			if($entry->has("eq")) $value = $entry->get("eq" )->value;
			else                  $value = $entry->get("neq")->value;
			if(is_string($value)) $value = trim($value);
			if(empty($value) || strtolower($value)=="null") {
				if($this->has("neq")) return $col .($this->upper ? strtoupper(" is not null") : strtolower(" is not null"));
				else                  return $col .($this->upper ? strtoupper(" is null"    ) : strtolower(" is null"    ));
			}
			if(strtolower($value)=="not null") {
				if($this->has("neq")) return $col .($this->upper ? strtoupper(" is null"    ) : strtolower(" is null"    ));
				else                  return $col .($this->upper ? strtoupper(" is not null") : strtolower(" is not null"));
			}
			if(!in_array(gettype($value), array("string", "double", "integer", "boolean"))) return NULL;
			return sprintf("%s%s%s", $col, $entry->has("neq") ? "!=" : "=", $this->decodeTicks($value));
		}
		// above and below
		if($entry->has("above") || $entry->has("below")){
			if($entry->has("above")) $value = $entry->get("above")->value;
			else                     $value = $entry->get("below")->value;
			if(!$value || $value=="null" || $value=="not null") return NULL;
			if(!in_array(gettype($value), array("double", "integer"))) return NULL;
			return sprintf("%s%s%s", $col, $entry->has("above") ? ">" : "<", $this->decodeTicks($value));
		}
		// aboveeq and beloweq
		if($entry->has("aboveeq") || $entry->has("beloweq")){
			if($entry->has("aboveeq")) $value = $entry->get("aboveeq")->value;
			else                       $value = $entry->get("beloweq")->value;
			if(!$value || $value=="null" || $value=="not null") return NULL;
			if(!in_array(gettype($value), array("double", "integer"))) return NULL;
			return sprintf("%s%s%s", $col, $entry->has("aboveeq") ? ">=" : "<=", $this->decodeTicks($value));
		}
		// in or outside list
		if($entry->has("inset") || $entry->has("outset")){
			if($entry->has("inset")) $value = $entry->get("inset" )->value;
			else                     $value = $entry->get("outset")->value;
			if(gettype($value)!="array") return NULL;
			$args = array();
			foreach($value as $element)
				array_push($args, sprintf("%s%s%s", $col, $entry->has("inset") ? "=" : "!=", $this->decodeTicks($element)));
			if($entry->has("inset")) return "(".implode($this->upper ? " OR " : " or ", $args).")";
			return implode($this->upper ? " AND " : " and ", $args);
		}
		// in or outside range
		if($entry->has("inrange") || $entry->has("outrange")){
			if($entry->has("inrange")) $value = $entry->get("inrange" )->value;
			else                       $value = $entry->get("outrange")->value;
			if(gettype($value)!="array") return NULL;
			$args = array();
			foreach($value as $element){
				if(gettype($element)!="array" || count($element)!=2) continue;
				$raw = sprintf("(%s ".($this->upper ? "BETWEEN" : "between")." %s ".($this->upper ? "AND" : "and")." %s)", $col, $this->decodeTicks($element[0]), $this->decodeTicks($element[1]));
				if($entry->has("inrange")) { array_push($args, $raw); continue; }
				array_push($args, sprintf("(".($this->upper ? "NOT" : "not")." %s)", $raw));
			}
			if($entry->has("inrange")) return implode($this->upper ? " OR " : " or ", $args);
			return implode($this->upper ? " AND " : " and ", $args);
		}
		return NULL;
	}		

	// decodeWhereStatement
	// ---------------------------------------- 
	private function decodeWhereStatement($config){
		/* Builds the overall where statement from the config object;
		* The logic is roughly as follows:
		* * different types of selects (eq, neq, above, below, etc.)
		* * value can be another DbConfig object
		* * name can be null, meaning starting with _, then only value as is
		* * if value is array and eq  => or
		* * if value is array and neq => not element AND
		* * grouping and bracketing, e.g. '(col1=1 and col2=2) or col3=3' */
		// building the individual statements
		//$brackets   = array();
		$selects    = array();
		$selects[0] = array();
		$links      = array();
		$links[0]   = array();
		foreach($config->getByType("dbselect") as $entry){
			$where = $this->decodeWhereEntity($entry);
			if(empty($where)) continue;
			$group = $entry->has("group") ? $entry->get("group")->value : 0;
			$link  = $entry->has("link" ) ? $entry->get("link" )->value : "AND";
			//$close = $entry->has("close") ? $entry->get("close")->value : NULL;
			//if($group != $close) {
			//	if(!array_key_exists($group, $brackets)) $brackets[$group] = array();
			//	$brackets[$group][$close] = $link;
			//}
			if(!array_key_exists($group, $selects)) {
				$selects[$group] = array();
				$links  [$group] = array();
			}
			array_push($selects[$group], $where);
			array_push($links  [$group], $link );
		}
		// bracketing
		$keys    = array_keys($selects);
		$nGrps   = count($keys);
		$groups  = array();
		$grouped = array();
		foreach($keys as $group){
			if(count($selects[$group])==0) continue;
			$grouped[$group] = "";
			for($i=0;$i<count($selects[$group]);++$i){
				$link = " ".($this->upper ? strtoupper($links[$group][$i]) : strtolower($links[$group][$i]))." ";
				if($i==0) $link = "";
				$grouped[$group] .= $link.$selects[$group][$i];
			}
			//$grouped[$group] = implode($this->upper ? " AND " : " and ", $selects[$group]);
			if(count($selects[$group])>1 && $nGrps>1) $grouped[$group] = sprintf("(%s)", $grouped[$group]);
			array_push($groups, $group);
		}
		if(count($groups)==0) return "";
		$pattern = $config->spattern->value;
		$pattern = $this->upper ? strtoupper($pattern) : strtolower($pattern);
		$match   = array();
		preg_match_all("/(\%\d\b)/", $pattern, $match);
		$elms    = array_unique($match[0]);
		$nums    = array();
		foreach($elms as $apply) array_push($nums, intval(substr($apply, 1)));
		sort($nums); 
		sort($groups);
		$result  = ($this->upper ? "WHERE" : "where")." ";
		$result .= $this->decodeRawStatement($config, 6)." ";
		if(empty($pattern) || count($elms)!=count($groups) || $nums!=$groups)
			return $result.implode($this->upper ? " AND " : " and ", $grouped);
		foreach($elms as $apply){ // need to loop again to get order right
			$idx     = intval(substr($apply, 1));
			$pattern = str_replace($apply, $grouped[$idx], $pattern);
		}
		return $result.$pattern;
		//$where = "";
		//$prev  = -1;
		//foreach($groups as $group){
		//	if($prev==-1) {
		//		$where .= sprintf("%s", $grouped[$group]);
		//		$prev   = $group;
		//		continue;
		//	}
		//	$link  = strtolower(($prev>-1 && array_key_exists($group, $brackets) && array_key_exists($prev, $brackets[$group])) ? $brackets[$group][$prev] : "and");
		//	if($this->upper) $link = strtoupper($link);
		//	$where .= sprintf(" %s %s", $link, $grouped[$group]);
		//	$prev  = $group;
		//}	
		//return !empty($where) ? sprintf("WHERE %s", $where) : "";
	}




	// technology-independent extractor functions (DbConfig => plain php) 
	// (actually still dependent because of invoking decoder functions)
	// ========================================

	// extractColumnnames
	// ---------------------------------------- 
	public function extractColumnnames($config, $tablename=NULL, $tables=array(), $shorts=array()){
		/* Extracts the column names from the DbConfig object,
		* admittedly in the probably most ineffective way possible.. */
		$string = $this->decodeColumnnames($config, $tablename);
		$fake   = "JOIN "; // fake a join for getColumnnames
		if(!empty($tablename)) {
			//$tables = array(strtoupper($tablename                 )); 
			//$shorts = array(strtoupper($this->reformat[$tablename]));
			$fake = $tablename;
		}
		//return $this->getColumnnames("SELECT ".$string." FROM ".$fake, false, $tables, $shorts);
		$list = $this->getColumnnames("SELECT ".$string." FROM ".$fake, false, $tables, $shorts);
		$cols   = array();
		foreach($list as $coldef){
			$col = trim(strtolower($coldef));
			$col = stripLeft($col, $this->config->has("dbIgnore") ? array_map("strtolower", $this->config->dbIgnore->value) : array());
			$ref = strtolower($this->decodeReformatColumn($config, $col));
			if(strpos($ref, " as ")!==false) array_push($cols, explode(" as ", $ref));
			else                             array_push($cols, array(NULL, $col)    );
		}
		return $cols;
		// old code below, problematic with JOINS..
		$list   = extractColumnsSelect("SELECT ".$string." FROM ");
		$cols   = array();
		foreach($list as $coldef){
			$col = trim(strtolower($coldef));
			$col = stripLeft($col, $this->config->has("dbIgnore") ? array_map("strtolower", $this->config->dbIgnore->value) : array());
			if(strpos($col, " as ")!==false) array_push($cols, explode(" as ", $col));
			else                             array_push($cols, array(NULL, $col)    );
			//if(strpos($col, " as ")!==false) 
			//	$col = substr($col, strpos($col, " as ") + 4);
			//array_push($cols, $col);
		}
		return $cols;
	}

	// extractTablenames
	// ---------------------------------------- 
	public function extractTablenames($config, &$shorts, &$tables){
		/* Extracts the names and abbreviations of the tables used
		* in the joinon statements of the DbConfig object and appends
		* them to the $tables and $shorts arrays, respectively. */
		foreach($config->joinon->value as $jstmt){
			$halfs = explode("=", $jstmt);
			$halfs = array_map("trim", $halfs);
			foreach($halfs as $coldef) {
				if(strpos($coldef, ".") === false) continue;
				$short = strtok($coldef, ".");
				$table = $config->getReformat($short);
				if(in_array($table, $tables)) continue;
				array_push($shorts, $short);
				array_push($tables, $table);
			}
		}
	}
}


?>
