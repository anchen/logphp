<?php
ini_set('magic_quotes_gpc'    , 0);
ini_set('magic_quotes_runtime', 0);
ini_set('magic_quotes_sybase' , 0);
ini_set("session.gc_maxlifetime", 1800); // 30mins
ini_set('memory_limit', '1024M');
session_start();
//session_id(array_key_exists("sessId", $_GET) ? $_GET['sessId'] : (array_key_exists("sessId", $_POST) ? $_POST["sessId"] : NULL));


/*
// ----------------------------------------------------------------------------------------------------
// - Display Errors
// ----------------------------------------------------------------------------------------------------
ini_set('display_errors', 'On');
ini_set('html_errors', 0);

// ----------------------------------------------------------------------------------------------------
// - Error Reporting
// ----------------------------------------------------------------------------------------------------
error_reporting(-1);

// ----------------------------------------------------------------------------------------------------
// - Shutdown Handler
// ----------------------------------------------------------------------------------------------------
function ShutdownHandler()
{
    if(@is_array($error = @error_get_last()))
    {
        return(@call_user_func_array('ErrorHandler', $error));
    };

    return(TRUE);
};

register_shutdown_function('ShutdownHandler');

// ----------------------------------------------------------------------------------------------------
// - Error Handler
// ----------------------------------------------------------------------------------------------------
function ErrorHandler($type, $message, $file, $line)
{
    $_ERRORS = Array(
        0x0001 => 'E_ERROR',
        0x0002 => 'E_WARNING',
        0x0004 => 'E_PARSE',
        0x0008 => 'E_NOTICE',
        0x0010 => 'E_CORE_ERROR',
        0x0020 => 'E_CORE_WARNING',
        0x0040 => 'E_COMPILE_ERROR',
        0x0080 => 'E_COMPILE_WARNING',
        0x0100 => 'E_USER_ERROR',
        0x0200 => 'E_USER_WARNING',
        0x0400 => 'E_USER_NOTICE',
        0x0800 => 'E_STRICT',
        0x1000 => 'E_RECOVERABLE_ERROR',
        0x2000 => 'E_DEPRECATED',
        0x4000 => 'E_USER_DEPRECATED'
    );

    if(!@is_string($name = @array_search($type, @array_flip($_ERRORS))))
    {
        $name = 'E_UNKNOWN';
    };

    return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
};

$old_error_handler = set_error_handler("ErrorHandler");
*/




require "library/master.php";

if(!isset($_SESSION["setup"]) || !isset($_SESSION["theme"])){
	header('Location: '.rtrim(file_get_contents("configs/self"), "\n"));
	exit();
}

$setup = $_SESSION["setup"];
$chThm = (isset($_POST["theme"]) && !empty($_POST["theme"]));
$theme = $chThm ? $_POST["theme"] : $_SESSION["theme"];
if($chThm) {
	$_SESSION["theme"] = $theme;
	$_POST["theme"] = "";
}

$master = new Gate($theme, array("configs/setup_all", "configs/setup_".$theme."_".$setup, "configs/theme_".$theme));


?>
