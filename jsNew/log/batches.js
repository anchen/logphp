
// isBatchAlias
// --------------------------------------------
function isBatchAlias(alias) {
	var line1 = "NOTE: You assigned an Alias to this batch.\n\n";
	var line2 = "The sub batches need to have unique Alias IDs after splitting. Therefore, the sub batch ID will be appended to the Alias automatically. If you want to change it, go to Display Equipment and change the Aliases of the sub batches after splitting.\n\n";
	var line3 = "As an alternative approach, you could also remove the Alias completely. It is not recommended to use them for batches if not strictly necessary (i.e. because the manufacturer already named the items etc.).";
	if(alias != "") {
		alert(line1+line2+line3);
		return true;
	}
	return true;
}


// setDivUnits
// --------------------------------------------
function setDivUnits(preID, curI, count, quantity){
	var curID  = preID + (curI).toString();
	var curVal = parseInt(document.getElementById(curID).value);
	var goodUnitID  = "good"+curID;
	var goodUnitVal = parseInt(document.getElementById(goodUnitID).value);
	if (goodUnitVal > curVal) {
		document.getElementById(goodUnitID).value = (curVal).toString();
		document.getElementById(goodUnitID).onchange();
	}
	var sumVal = 0; 
	for (i = 0; i < count; i++) {
		var id = preID + (i).toString();
		var inpVal = parseInt(document.getElementById(id).value);
		sumVal = sumVal + inpVal;
	}
	if (sumVal > quantity) {
		var exVal = sumVal - quantity;
		document.getElementById(curID).value = (curVal - exVal).toString();   	
		document.getElementById("batchesDividedUnits").value = quantity.toString();   	
		document.getElementById("batchesHiddenLeftUnits").value = (0).toString();
		document.getElementById("batchesLeftU").classList.add("batchesFullysplit");      
		document.getElementById("batchesLeftU").innerHTML = "There are no Units left in the batch! GOOD!";
		return;   	
	}	
	document.getElementById("batchesDividedUnits").value = sumVal.toString();	
	if (sumVal < quantity) {
		var leftVal = quantity - sumVal;
		document.getElementById("batchesLeftU").classList.remove("batchesFullysplit");
		if (leftVal == 1) {
			document.getElementById("batchesLeftU").innerHTML = "There is still 1 Unit left in the batch to be assigned!";
		} 
		else {	
			document.getElementById("batchesLeftU").innerHTML = "There are still "+leftVal+" Units left in the batch to be assigned!";
		}
	} 
	else if(sumVal == quantity) {
		document.getElementById("batchesHiddenLeftUnits").value = (0).toString();   	
		document.getElementById("batchesLeftU").classList.add("batchesFullysplit");
		document.getElementById("batchesLeftU").innerHTML = "There are no Units left in the batch! GOOD!";
	}
	return;   	
}


// setDivGoodUnits
// --------------------------------------------
function setDivGoodUnits(preID1, preID2, curI, count, goodquantity) {
	var curID1  = preID1 + (curI).toString();
	var curID2  = preID2 + (curI).toString();	
	var curVal1 = parseInt(document.getElementById(curID1).value);
	var curVal2 = parseInt(document.getElementById(curID2).value);
	var sumVal = 0;
	if (curVal1 < curVal2) {
		curVal2 = curVal1;
		document.getElementById(curID2).value = (curVal2).toString();   	
	}					 
	for (i = 0; i < count; i++) {
		var id = preID2 + (i).toString();
		var inpVal = parseInt(document.getElementById(id).value);
		sumVal = sumVal + inpVal;
	}
	document.getElementById("batchesDividedGoodUnits").value = sumVal.toString();
	if (sumVal > goodquantity) {
		var exVal = sumVal - goodquantity;
		document.getElementById(curID2).value = (curVal2 - exVal).toString();   	
		document.getElementById("batchesDividedGoodUnits").value = goodquantity.toString();   	
		document.getElementById("batchesHiddenLeftGoodUnits").value = (0).toString();
		document.getElementById("batchesLeftGoodU").classList.add("batchesFullysplit");      
		document.getElementById("batchesLeftGoodU").innerHTML = "There are no Units left in the batch! GOOD!";
		return;   	
	}	   
	if (sumVal < goodquantity) {
		var leftVal = goodquantity - sumVal;
		document.getElementById("batchesLeftGoodU").classList.remove("batchesFullysplit");
		if (leftVal == 1) {
			document.getElementById("batchesLeftGoodU").innerHTML = "There is still 1 Good Unit left in the batch to be assigned!";
		}
		else {	
			document.getElementById("batchesLeftGoodU").innerHTML = "There are still "+leftVal+" Good Units left in the batch to be assigned!";
		}
	} 
	else if(sumVal == goodquantity) {
		document.getElementById("batchesHiddenLeftGoodUnits").value = (0).toString();   	
		document.getElementById("batchesLeftGoodU").classList.add("batchesFullysplit");
		document.getElementById("batchesLeftGoodU").innerHTML = "There are no Good Units left in the batch! GOOD!";
	}
	return;
}


// isfullysplitup
// --------------------------------------------
function isfullysplitup() {	
	var units     = document.getElementById("batchesHiddenLeftUnits").value;
	var goodunits = document.getElementById("batchesHiddenLeftGoodUnits").value;
	if(units == 0 && goodunits == 0){		
		return true;		
	} 
	else {
		alert("Units or Good Units in Batch not yet fully split up!");
		return false; 
	}
}



