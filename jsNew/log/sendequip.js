

// checkShippingDates
// --------------------------------------------
function checkShippingDates(){
	var dateFrom = document.getElementsByName("dateFrom")[0].value;  
	var from     = new Date(dateFrom);
	var dateTo   = document.getElementsByName("dateTo"  )[0].value;
	var to       = new Date(dateTo);
	if(!dateFrom || dateFrom.length===0 || isNaN(from.valueOf())){
		from = new Date(); // just use today
		//alert("Please give a valid shipping date!");
		//return false;
	}
	if(!isNaN(from.valueOf()) && !isNaN(to.valueOf()) && from.valueOf()>=to.valueOf()){
		alert("Expected return date is before shipping date!");
		return false;
	}
	return true;
}


// checkShippingSites
// --------------------------------------------
function checkShippingSites() {
	var fromID = document.getElementsByName("siteFrom")[0].value;
	var toID   = document.getElementsByName("siteTo"  )[0].value;
	if(toID == fromID) {	   		
		alert("You cannot ship equipment inside the same location. " +
		      "If you want to change the status of this selection only, " +
		      "please use the Multi-Edit-Tool, if you have access.");
        document.getElementsByName("siteTo")[0].value = 0;
        return false;
	}
    return true;	
}


// checkSendEquip
// --------------------------------------------
function checkSendEquip(fault) {
	var rangeCount = document.getElementsByName("rangeCount")[0].value;
	var fromId     = document.getElementsByName("siteFrom"  )[0].value;
	var destId     = document.getElementsByName("siteTo"    )[0].value;
	var person     = document.getElementsByName("shipPers"  )[0].value;
	if (rangeCount == 0) {
		alert("No Equipment selected!");
		return false;
	} 
	else if (fromId == 0) {
		alert("No Location selected!");
		return false;
	} 
	else if (destId == 0) {
		alert("No Destination selected!");
		return false;
	}
	else if(!person || person.length===0){
		alert("Please give a valid shipping person!");
		return false;
	} 
	else if(!checkShippingDates()){
		return false;
	} 
	else if(!checkShippingSites()){
		return false;
	} 
	else if (parseInt(fault) > 0) {
		alert("You have selected items and/or batches together that have different actual locations!");
		return false;      
	}
	return true;
}


