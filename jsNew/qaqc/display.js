
// checkUserTables
// --------------------------------------------
function checkUserTables(){
	var selection = document.getElementsByName('tablesUser')[0].value;
	var radios    = document.getElementsByName('dbchoice'  );
	var dbchoice  = "";
	for(i=0; i<radios.length; i++){
		if(radios[i].checked){
			dbchoice = radios[i].value;
			break;
		}
	}
	if(dbchoice=='elx' && selection=='centralmeasview'){
		alert("SORRY! The Central Summary Measurements Table in the Electronics DB is HUGE. As we have experienced problems displaying it here, this table has been disabled. Please use the 'Custom Search' page in order to view the measurement data.");
		return false;
	}
	return true;
}
