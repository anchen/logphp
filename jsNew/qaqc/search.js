
// askSettingName
// --------------------------------------------
function askSettingName() {
	var tabsetname = prompt("Please enter a setting name:", "setting name");
	if (tabsetname != null) {
		tabsetname = tabsetname.replace(/ /g,"_");
		document.getElementById("setting").value = tabsetname;		
		return true;
	}
	return false;
}
