
// checkRange
// --------------------------------------------
function checkRange() {
	var idType     = document.getElementsByName("typeId"    )[0].value;
	var startRange = document.getElementsByName("startRange")[0].value;
	var endRange   = document.getElementsByName("endRange"  )[0].value;          	
	
	if (idType == "mtf" && endRange != "" && (startRange.substr(0,9) != endRange.substr(0,9))) {
		alert("Different Equipment Types within one range are not allowed!");
		return false;
	} 
	else if (idType == "alias" && endRange != "") {
		var lnStart = startRange.length;
		var lnEnd   = endRange.length;   	
		if (lnStart != lnEnd) {
			alert("These aliases do not match and the range could not be built. Please make sure that you enter valid ranges!");
			return false;
		} 
		else if (endRange > 0) {
			for (i = lnStart; i >= 0; i--) {
				var charStart = startRange.substr(i,1);
				var charEnd   = endRange.substr(i,1);
				if (charStart != charEnd) {
					isDigitS =  /^\d+$/.test(charStart);
					isDigitE =  /^\d+$/.test(charEnd);
					if(!isDigitS || !isDigitE){
						alert("These aliases do not match and the range could not be built. Please make sure that you enter valid ranges!");
						return false;
					}
				}   		   
			}
		}
	}
	return true;   
}

// toggleRange
// --------------------------------------------
function toggleRange() {
	var checkbox   = document.getElementsByName("chooseRange"    )[0];
	var startrange = document.getElementsByName("startRange"     )[0];	
	var endrange   = document.getElementsByName("endRange"       )[0];	
	var startplhd1 = document.getElementsByName("startRangePlhd1")[0];
	var startplhd2 = document.getElementsByName("startRangePlhd2")[0];
	var endplhd1   = document.getElementsByName("endRangePlhd1"  )[0]; 
	var endplhd2   = document.getElementsByName("endRangePlhd2"  )[0]; 
	if(checkbox.checked == false) {
		endrange  .value       = ""; 
		endrange  .placeholder = endplhd1.value; 
		endrange  .disabled    = true; 
		startrange.placeholder = startplhd1.value;
	} 
	else {
		endrange  .placeholder = endplhd2.value;
		endrange  .disabled    = false;
		startrange.placeholder = startplhd2.value;
	}
}

